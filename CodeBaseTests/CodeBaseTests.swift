//
//  CodeBaseTests.swift
//  CodeBaseTests
//
//  Created by KurtHo on 2020/11/30.
//

import XCTest
@testable import CodeBase

class CodeBaseTests: XCTestCase {

    let vm = FirstViewModel()
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        debug(vm.resumeData.value.contents.count)
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}

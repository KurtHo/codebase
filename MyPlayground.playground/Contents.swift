//: A UIKit based Playground for presenting user interface
  
import UIKit
import PlaygroundSupport

class MyViewController : UIViewController {
    override func loadView() {
        let view = UIView()
        view.backgroundColor = .white

        let label = UILabel()
        label.frame = CGRect(x: 150, y: 200, width: 200, height: 20)
        label.text = "Hello World!"
        label.textColor = .black
        
        view.addSubview(label)
        self.view = view
    }
}

PlaygroundPage.current.liveView = FirstVC().view
//PlaygroundPage.current.liveView = MyViewController().view



//let word = "abcdefghijklmnopqrstuvwxyz"
//
//func split(str:String, by length: Int) -> [String] {
//    //startIndex = str的起始index
//    //endIndex = str的結束index
//    var startIndex = str.startIndex
//    var results = [Substring]()
//
//    while startIndex < str.endIndex {
//        //myEndIndex(取得我要的長度位置) str的起始位置，到長度的位置，limitedBy限制的總長度
//        let myEndIndex = str.index(startIndex, offsetBy: length, limitedBy: str.endIndex) ?? str.endIndex
//        //results append str的起始 到 我要的結束位置
//        var subStr = str[startIndex ..< myEndIndex]
//        results.append(str[startIndex ..< myEndIndex])
//        //把原本的起始位置設定現在結束的位置
//        startIndex = myEndIndex
//    }
//    print(results)
//    return results.map { String($0) }
//}
//
//let sep = split(str: word, by: 3)
//print(sep)
//
//struct SomeModel {
//    let kk:String
//    let num:Int
//}
//
//
//
//func makeIncremeter(forIncremet amount: Int) -> () -> Int {
//    var runningTotal = 0
//    func incrementer() -> Int {
//        runningTotal += amount
//        return runningTotal
//    }
//
//    return incrementer
//}
//
//let m1 = makeIncremeter(forIncremet: 1)
//
//print(m1())
//print(m1())
//print(m1())
//let m2 = m1
//print(m2())
//print(m1())
//print(m2())
//
//
//
//
//
//// 创建一个初始值为 2 的信号量
//let semaphore = DispatchSemaphore(value: 1)
//
//// 共享资源（一个简单的数组）
//var sharedResource = [Int]()
//
//// 模拟并发访问的函数
//func accessResource(_ threadID: Int) {
//    // 等待信号量，如果信号量计数值小于等于 0，则等待
//    semaphore.wait()
//
//    // 访问共享资源
//    sharedResource.append(threadID)
//    print("Thread \(threadID) accessed the resource. Resource: \(sharedResource)")
//
//    // 模拟一些工作
//    usleep(useconds_t(arc4random_uniform(1000000)))
//
//    // 释放信号量，增加计数值
//    semaphore.signal()
//}
//
//// 模拟多个线程访问共享资源
//for i in 0..<5 {
//    DispatchQueue.global().async {
////        accessResource(i)
////        myAccess(i)
//    }
//}
//
//// 等待所有线程执行完毕
//DispatchQueue.global().sync {
//    sleep(2) // 等待足够的时间以确保所有线程完成
//    print("All threads have finished.")
//}
//
//func myAccess(_ threadID: Int) {
//    semaphore.wait()
//    sharedResource.append(threadID)
////    debug("Thread \(threadID) accessed the resource. Resource: \(sharedResource)")
//    var type:Topic {
//        switch threadID {
//        case 0:     return .appleNews
//        case 1:     return .techNews
//        case 2:     return .teslaNews
//        case 3:     return .tw
//        default:    return .us
//        }
//    }
//
//    api.get(.news(source: type), decode: NewsList.self) { newList, result in
//        switch result {
//        case .success(let news):
//            semaphore.signal()
////            debug(news)
//            debug("type: \(type)")
//        case .failure(let err):
//            debug(err)
//        }
//    }
//}
//
//


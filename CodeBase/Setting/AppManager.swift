//
//  InitFile.swift
//  CodeBase
//
//  Created by CI-Kurt on 2020/12/8.
//

import UIKit
import RxSwift
import IQKeyboardManagerSwift

protocol OrientDelegate: AnyObject {
    func didChange(_ orientation: Orientation)
}

enum Orientation {
    case landScape, portrait
}

let appManager = AppManager.shared
class AppManager {
    private init(){}
    static let shared = AppManager()
    
    weak var orientDelegate: OrientDelegate?
    
    var orientation:Orientation = .portrait
    
    func setting() {
        observeRotation()
        rxLeakingChecker()
        settingKeyboard()
    }
    
    func observeRotation() {
        NotificationCenter.default.addObserver(self, selector: #selector(rotated), name: UIDevice.orientationDidChangeNotification, object: nil)
    }
    
    /// set enviroment arguments will auto detect rx leak checking
    func rxLeakingChecker() {
        if ProcessInfo.processInfo.environment["RxLeakChecker"] == "enable" {
            _ = Observable<Int>.interval(.seconds(1), scheduler: MainScheduler.instance)
                .subscribe(onNext: { _ in
                    print("Rx Resource: \(Resources.total)")
                })
        }
    }
    
    func settingKeyboard() {
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.toolbarManageBehaviour = .byPosition
        IQKeyboardManager.shared.enableAutoToolbar = false
    }
}

extension AppManager {
    @objc func rotated() {

        switch UIDevice.current.orientation {
        case .portrait:
            if orientation != .portrait {
                orientation = .portrait
                orientDelegate?.didChange(orientation)
            }
        case .landscapeLeft, .landscapeRight:
            orientation = .landScape
            orientDelegate?.didChange(orientation)
        default:
            ()
        }
        
    }
}

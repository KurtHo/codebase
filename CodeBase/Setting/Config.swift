//
//  Config.swift
//  CodeBase
//
//  Created by KurtHo on 2020/12/1.
//

import Foundation

public func debug(_ items: String..., filename: String = #file, function: String = #function, line: Int = #line, separator: String = " ") {
    #if DEBUG
    let pretty = "\(URL(fileURLWithPath: filename).lastPathComponent) [#\(line)] \(function)"
    let output = items.map { "\($0)" }.joined(separator: separator)
    let lines = output.split(separator: "\n")
    
    if let firstLine = lines.first {
        Swift.print(pretty + "\n\t-> \(firstLine)")
        for line in lines.dropFirst() {
            Swift.print("\t-> \(line)")
        }
    } else {
        Swift.print(pretty + "\n\t-> " + output)
    }
    #else
    Swift.print("RELEASE MODE")
    #endif
}


public func debug(_ items: Any..., filename: String = #file, function: String = #function, line: Int = #line, separator: String = " ") {
    #if DEBUG
    let pretty = "\(URL(fileURLWithPath: filename).lastPathComponent) [#\(line)] \(function)"
    let output = items.map { "\($0)" }.joined(separator: separator)
    let lines = output.split(separator: "\n")
    
    if let firstLine = lines.first {
        Swift.print(pretty + "\n\t-> \(firstLine)")
        for line in lines.dropFirst() {
            Swift.print("\t-> \(line)")
        }
    } else {
        Swift.print(pretty + "\n\t-> " + output)
    }
    #else
    Swift.print("RELEASE MODE")
    #endif
}

class Configuration {
    #if RELEASE
    public static let mode = ConfigurationMode.release
    #elseif DEBUG
    public static let mode = ConfigurationMode.debug
    #else
    public static let mode = ConfigurationMode.other
    #endif
}

public enum ConfigurationMode {
    case release, debug, other
}

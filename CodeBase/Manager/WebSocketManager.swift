//
//  WebSocketManager.swift
//  CodeBase
//
//  Created by KurtHo on 2021/12/10.
//

import Foundation
import Starscream

class WebSocketManager {
    let server = "wss://mmtg4u3c6e.execute-api.ap-northeast-1.amazonaws.com/production"
    lazy var request: URLRequest = {
        return URLRequest(url: URL(string: self.server)!)
    }()
    
    var socket: WebSocket? = nil
    var isConnected = false
    
    func setConnection(vc: WebSocketDelegate) {
        request.timeoutInterval = 5
        
        request.setValue("echo-protocol", forHTTPHeaderField: "Sec-WebSocket-Protocol")
//        request.setValue("14", forHTTPHeaderField: "Sec-WebSocket-Version")
        request.setValue("chat,superchat", forHTTPHeaderField: "Sec-WebSocket-Protocol")
        request.setValue("Everything is Awesome!", forHTTPHeaderField: "My-Awesome-Header")
        socket = WebSocket(request: request)
        
        socket?.delegate = vc
        socket?.connect()
    }
    
    func sendPubMessage(str:String, completion: ( ()-> Void)? ) {
        let content = "{\"action\": \"sendPublic\", \"message\": \"\(str)\"}"
        socket?.write(string: content, completion: completion)
    }
    
    func disconnect() {
        socket?.disconnect()
    }
    
    func setName(_ nameStr:String){
        let content = "{\"action\": \"setName\", \"name\": \"\(nameStr)\"}"
        socket?.write(string: content, completion: nil)
    }
}

extension WebSocketManager: WebSocketDelegate {
    func didReceive(event: WebSocketEvent, client: WebSocket) {
        switch event {
        case .connected(let headers):
            isConnected = true
            debug("websocket is connected: \(headers)")
        case .disconnected(let reason, let code):
            isConnected = false
            debug("websocket is disconnected: \(reason) with code: \(code)")
        case .text(let string):
            debug("Received text: \(string)")
        case .binary(let data):
            debug("Received data: \(data.count)")
        case .ping(_):
            break
        case .pong(_):
            break
        case .viabilityChanged(_):
            break
        case .reconnectSuggested(_):
            break
        case .cancelled:
            isConnected = false
        case .error(let error):
            isConnected = false
            handleError(error)
        }
    }
    
    private func handleError(_ error: Error?) {
        if let error = error as? WSError {
            debug("error...: \(error.message)")
        }else {
            debug("error...: \(String(describing: error))")
        }
    }
}

class Action:Codable {
    var action:String = "sendPublic"
    var publicMessage:String = "some public msg"
}

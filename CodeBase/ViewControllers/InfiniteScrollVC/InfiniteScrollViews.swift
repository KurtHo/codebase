//
//  TestViews.swift
//  CodeBase
//
//  Created by CI-Kurt on 2021/6/17.
//

import UIKit

class InfiniteScrollViews: NSObject {
    
    var superview: UIView
    var collectionView: UICollectionView = {
        let flowlayout = UICollectionViewFlowLayout()
        flowlayout.minimumInteritemSpacing = 0
        flowlayout.minimumLineSpacing = 0
        flowlayout.scrollDirection = .horizontal
        
        let cv = UICollectionView(frame: .zero, collectionViewLayout: flowlayout)
        cv.backgroundColor = .systemBlue
        cv.isPagingEnabled = true
        cv.showsHorizontalScrollIndicator = false
        return cv
    }()
    
    private var infiniteScrollingBehaviour: InfiniteScrollingBehaviour!
    
    init(superview: UIView) {
        self.superview = superview
        self.superview.backgroundColor = .white
        super.init()
    }
    
    func setPosition() {
        superview.addSubview(collectionView)
        collectionView.setAnchor(top: superview.safeAreaLayoutGuide.topAnchor, leading: superview.safeAreaLayoutGuide.leadingAnchor, trailing: superview.safeAreaLayoutGuide.trailingAnchor, bottom: superview.safeAreaLayoutGuide.bottomAnchor, height: nil, width: nil)
    }
    
    func setCollectionView() {
        collectionView.register(CollectionViewCell.nib, forCellWithReuseIdentifier: CollectionViewCell.identifier)
        
        DispatchQueue.main.async {
            
            let scrollingDirection = self.infiniteScrollingBehaviour.collectionConfiguration.scrollingDirection
            let configuration =  CollectionViewConfiguration(layoutType: .numberOfCellOnScreen(1), scrollingDirection: scrollingDirection)
            self.infiniteScrollingBehaviour.updateConfiguration(configuration: configuration)
        }
    }
    
    func layoutCollectionView() {
        let configuration = CollectionViewConfiguration(layoutType: .fixedSize(sizeValue: superview.bounds.width, lineSpacing: 0), scrollingDirection: .horizontal)
        infiniteScrollingBehaviour = InfiniteScrollingBehaviour(withCollectionView: collectionView, andData: Card.dummyCards, delegate: self, configuration: configuration)
        self.infiniteScrollingBehaviour.updateConfiguration(configuration: configuration)
        
    }
    

}

extension InfiniteScrollViews: InfiniteScrollingBehaviourDelegate {
    
    func configuredCell(forItemAtIndexPath indexPath: IndexPath, originalIndex: Int, andData data: InfiniteScollingData, forInfiniteScrollingBehaviour behaviour: InfiniteScrollingBehaviour) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewCell.identifier, for: indexPath)

        if let collectionCell = cell as? CollectionViewCell,
            let card = data as? Card {
            collectionCell.titleLabel.text = card.name
        }
        return cell
    }
}

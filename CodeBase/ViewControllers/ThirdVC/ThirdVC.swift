//
//  ThirdVC.swift
//  CodeBase
//
//  Created by KurtHo on 2020/12/5.
//

import UIKit

class ThirdVC: UIViewController {
    
    private var bannerView:BannerView = BannerView()
    let urls:[String] = [
        "https://images.pexels.com/photos/236047/pexels-photo-236047.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
        "https://cdn.pixabay.com/photo/2015/12/01/20/28/fall-1072821__340.jpg",
        "https://images.pexels.com/photos/257360/pexels-photo-257360.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
        "https://cdn.pixabay.com/photo/2017/04/09/09/56/avenue-2215317__340.jpg",
        "https://images.unsplash.com/photo-1500622944204-b135684e99fd?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"]
    
    let colors:[UIColor] = [.red, .orange, .yellow, .blue, .purple]
    
    var imgs = [UIImage]()

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        title = "constructing"
        
        dlImages { [weak self] in
            print("dl done...")
            self?.setUpBannerView()
        }
    }
    
    private func setUpUI() {
        self.view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        self.view.addSubview(bannerView)
        bannerView.setAnchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.leadingAnchor, trailing: view.trailingAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, height: nil, width: nil)
        view.layoutSubviews()
        
        bannerView.backgroundColor = UIColor.green
        
    }
    
    private func setUpBannerView() {
        bannerView.reloadData(configuration: nil, numberOfItems: colors.count) { (bannerView, index) -> (UIView) in
            
            let iv = self.itemView(at: index, bannerView: bannerView)

//            debug(iv)
            return iv
        }
    }
    
    private func itemView(at index:Int, bannerView:UIView)->UIImageView {
        
        let itemImageView:UIImageView = UIImageView(frame: bannerView.frame)
        itemImageView.frame.origin.x = bannerView.frame.origin.x * CGFloat(index)
        
        itemImageView.translatesAutoresizingMaskIntoConstraints = false
        itemImageView.backgroundColor = colors[index]
//        itemImageView.image = imgs[index]
        itemImageView.clipsToBounds = true
        itemImageView.contentMode = .scaleAspectFit
       
        return itemImageView
    }
    
    
    private func dlImages(finish: @escaping ()->Void) {
        let group: DispatchGroup = DispatchGroup()
        
        urls.forEach{ urlStr in
            let queue = DispatchQueue(label: urlStr, qos: .background, attributes: .concurrent)
            queue.async(group: group) {
                guard let url = URL(string: urlStr),
                        let data = try? Data(contentsOf: url),
                        let img = UIImage(data: data) else {
                            self.imgs.append(UIImage())
                    return
                }
                print("img is dl: \(urlStr)")
                
                self.imgs.append(img)
            }
        }
        
        group.notify(queue: DispatchQueue.main) {
            finish()
        }
        
    }
    
}

extension UIImageView {
    
    func setImage(path:String) {
        guard let url:URL = URL(string: path) else { return }
        DispatchQueue.global(qos: .background).async {
            guard let data:Data = try? Data(contentsOf: url) , let image:UIImage = UIImage(data: data) else { return }
            DispatchQueue.main.async {
                self.image = image
            }
        }
    }
    
}


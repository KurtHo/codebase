//
//  SMTTableViewHeaderView.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2021/12/23.
//

import UIKit

class SMTableViewHeaderView: UIView {
    weak var vm:SimpleMartVM?
    let scrollViewCarousel = ScrollViewCarousel()
    
    var pageControl:UIPageControl = {
        var pc = UIPageControl()
        pc.backgroundColor = .clear
        pc.pageIndicatorTintColor = .lightGray
        pc.currentPageIndicatorTintColor = .gray
        return pc
    }()
    let gap:CGFloat = 10
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        scrollViewCarousel.pageDelegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setContent(vm:SimpleMartVM?/*views:[UIView]*/){
        self.vm = vm
        
        addSubview(scrollViewCarousel)
        addSubview(pageControl)
        
        scrollViewCarousel.setAnchor(top: topAnchor, leading: leadingAnchor, trailing: trailingAnchor, bottom: pageControl.topAnchor, height: nil, width: nil, padding: UIEdgeInsets(top: 5, left: gap, bottom: 0, right: -gap))
        
        pageControl.setAnchor(top: scrollViewCarousel.bottomAnchor, leading: nil, trailing: nil, bottom: bottomAnchor, height: 27, width: 300)
        pageControl.setCenterAnchor(centerX: centerXAnchor, centerY: nil)
        
        pageControl.numberOfPages = vm?.colors.count ?? 1
        pageControl.subviews.forEach {
            $0.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
        }
        pageControl.size(forNumberOfPages: 5)
        
        setScrollView()
    }
    
    private func setScrollView(){
        guard let colors = vm?.colors else {return}

        let views:[SVCContentView] = {
            var vs = [SVCContentView]()
            colors.enumerated().forEach {
                let view = SVCContentView(text: String($0))
                view.backgroundColor = $1
                view.clipsToBounds = true
                view.layer.cornerRadius = 12

                vs.append(view)
            }
            return vs
        }()
        
        scrollViewCarousel.layer.borderWidth = 1
        scrollViewCarousel.layer.borderColor = UIColor.lightGray.cgColor
        scrollViewCarousel.output.setContent(views, spacing: gap)
    }
    
    func transition(){
        scrollViewCarousel.output.setConstraints(isSetting: false)
    }
    
    deinit {
        scrollViewCarousel.output.timerShutDown()
        debug("")
    }
    
}

extension SMTableViewHeaderView: ScrollViewCarouselDelegate{
    func current(page: Int) {
        guard let count = vm?.colors.count else {return}
        let currentPage = page != count + 1 ? page - 1 : 0
        
        pageControl.currentPage = currentPage
    }
}


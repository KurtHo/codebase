//
//  SimpleMartVC.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2021/12/23.
//

import UIKit

class SimpleMartVC:BaseVC {
    let vm = SimpleMartVM()
    
    let tableView = UITableView()
    var headerView: SMTableViewHeaderView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTableView()
        title = "Simple mart"
    }
    
    private func setTableView(){
        view.addSubview(tableView)
        tableView.setAnchor(top: view.topAnchor, leading: view.leadingAnchor, trailing: view.trailingAnchor, bottom: view.bottomAnchor, height: nil, width: nil)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        view.layoutSubviews()
        setHeader()
    }
    
    private func setHeader(){
        let ratio = self.tableView.bounds.size.width / 232
        let height =  self.tableView.bounds.size.width / ratio
        let rect = CGRect(x: 0, y: 0, width: self.tableView.bounds.size.width, height: height)
        
        headerView = SMTableViewHeaderView(frame:rect)
        
        tableView.tableHeaderView = headerView
        headerView.setContent(vm: vm)
    }
    
    //MARK:- Landscape
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        headerView.transition()
    }
}

//MARK:- UITableViewDataSource
extension SimpleMartVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 1 :7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = "test"
        
        if indexPath.section == 0 && indexPath.row == 0 {
            let cell = SMOperationCell()
            return cell
        }
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
}

//MARK:- UITableViewDelegate
extension SimpleMartVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch (indexPath.section, indexPath.row) {
        case (0,0):
            return 80
        default:
            return 100
        }

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
  

}


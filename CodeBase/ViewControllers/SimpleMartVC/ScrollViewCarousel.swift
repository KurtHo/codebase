//
//  File.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2021/12/23.
//

import UIKit

protocol ScrollViewCarouselDelegate: AnyObject {
    func current(page:Int)
}

protocol ScrollViewOutput {
    /// set content when initializing
    func setContent(_ views:[SVCContentView], spacing:CGFloat)
    /// call this function when parent view is about deinit
    func timerShutDown()
    /// isSetting: first setting = true, landscaping = false
    func setConstraints(isSetting:Bool)
}

class ScrollViewCarousel: UIScrollView, ScrollViewOutput {
    let stackView: UIStackView = {
        let sv = UIStackView()
        sv.axis = .horizontal
        return sv
    }()
    
    var output:ScrollViewOutput {return self}
    
    let wait:TimeInterval = 5
    var currentPage:Int = 1
    var views = [SVCContentView]()
    private var spacing:CGFloat = 0
    
    weak var timer:Timer?
    weak var pageDelegate:ScrollViewCarouselDelegate?
     
    
    func setContent(_ views:[SVCContentView], spacing:CGFloat) {
        guard let superview = superview else {return}

        self.views = views
        self.spacing = spacing
        
        stackView.spacing = spacing
        
        addSubview(stackView)
        superview.layoutSubviews()
        
        stackView.setAnchor(top: topAnchor, leading: leadingAnchor, trailing: trailingAnchor, bottom: bottomAnchor, height: bounds.height, width: nil, padding: UIEdgeInsets(top: 0, left: spacing / 2, bottom: 0, right: -spacing / 2))

        addViews()
        setConstraints(isSetting: true)
        
    }
    
    private func addViews(){
        let firstView = SVCContentView(text: String(views.count - 1))
        firstView.backgroundColor = views.last?.backgroundColor
        let lastView = SVCContentView(text: "0")
        lastView.backgroundColor = views.first?.backgroundColor
        [firstView, lastView].enumerated().forEach { index, view in
            view.clipsToBounds = true
            view.layer.cornerRadius = 12
        }
        
        self.views.insert(firstView, at: 0)
        self.views.append(lastView)
    }
    
    override init(frame:CGRect) {
        super.init(frame: frame)
        setBasic()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setBasic(){
        isPagingEnabled = true
        showsHorizontalScrollIndicator = false
        clipsToBounds = false
        delegate = self
        timerStart()
    }
    
    func setConstraints(isSetting:Bool = false) {
        let width = isSetting ? UIScreen.main.bounds.width - spacing*2 : UIScreen.main.bounds.height - spacing*2
                
        for view in self.views {
            view.removeConstraints(view.constraints)
            stackView.addArrangedSubview(view)
            view.setAnchor(top: nil, leading: nil, trailing: nil, bottom: nil, height: nil, width: width - spacing)
            view.setPosition()
        }
        
        let offset = width * CGFloat(currentPage)
        DispatchQueue.main.async {
            self.contentOffset.x = offset
        }
        
    }
    
    @objc private func autoScroll(){

        if currentPage == 0 {
            currentPage += 1
        }
        currentPage = currentPage < views.count - 1 ? currentPage + 1 : 0

        let offset = self.frame.width * CGFloat(self.currentPage)
        
        if currentPage == views.count - 1 {
            UIViewPropertyAnimator.runningPropertyAnimator(withDuration: 0.3, delay: .zero, options: .curveLinear) {
                self.contentOffset.x = offset
            } completion: {_ in
                self.contentOffset.x = self.bounds.width
                self.currentPage = 1
            
            }
            
        }else {
            setContentOffset(CGPoint(x: offset, y: 0), animated: true)
        }
        
        
        debug("offset: \(contentOffset.x)")
        pageDelegate?.current(page: currentPage)
    }
    
    
    func timerShutDown() {
        timer?.invalidate()
        timer = nil
    }
    
    private func timerStart(){
        if timer == nil {
            self.timer = Timer.scheduledTimer(timeInterval: wait, target: self, selector: #selector(autoScroll), userInfo: nil, repeats: true)
        }
    }
    
    deinit {
        debug("scroll view deinit....")
    }
}

extension ScrollViewCarousel: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        debug("offset before: \(scrollView.contentOffset)")
        
        if scrollView.contentOffset.x == 0 {
            var offset = scrollView.contentOffset
            offset.x = scrollView.bounds.width * CGFloat(views.count - 2)
            scrollView.contentOffset = offset
        }else {
            let current = (scrollView.contentOffset.x + 10) / scrollView.bounds.width
            if (views.count - 1) == Int(current) {
                var offset = scrollView.contentOffset
                offset.x = scrollView.bounds.width
                scrollView.contentOffset = offset
            }

        }
        
        timerShutDown()
        timerStart()
        currentPage = Int(scrollView.contentOffset.x / bounds.width)

        pageDelegate?.current(page: currentPage)
    }
    

    
}


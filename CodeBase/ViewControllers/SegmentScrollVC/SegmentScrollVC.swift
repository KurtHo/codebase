//
//  SegmentScrollVC.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2023/6/9.
//

import UIKit

class SegmentScrollVC: UIViewController {
    private let horizontalScrollView = HorizontalScrollView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "SegmentScrollVC"
        view.backgroundColor = .lightGray
        horizontalScrollView.frame = CGRect(x: 0, y: 100, width: view.bounds.width, height: 40)
        
        let names = [
            "John", "Emma", "Liam", "Olivia", "Noah", "Ava", "William", "Isabella", "James", "Sophia",
            "Benjamin", "Mia", "Lucas", "Charlotte", "Henry", "Amelia", "Alexander", "Harper", "Michael", "Evelyn"
        ]
        horizontalScrollView.items = names
        horizontalScrollView.itemTapped = { selectedIndex in
            print("Selected item at index: \(selectedIndex)")
        }
        
        view.addSubview(horizontalScrollView)
        horizontalScrollView.snp.makeConstraints { make in
            make.leading.trailing.top.equalToSuperview()
            make.height.equalTo(40)
        }
        
//        view.layoutSubviews()
        horizontalScrollView.layoutSubviews()
    }
    
}


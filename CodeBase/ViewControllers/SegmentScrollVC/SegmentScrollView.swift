//
//  SegmentScrollView.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2023/6/9.
//

import UIKit

class HorizontalScrollView: UIView {
    
    private let scrollView = UIScrollView()
    private let stackView = UIStackView()
    private let underlineView = UIView()
    
    var itemTapped: ((Int) -> Void)?
    
    var items: [String] = [] {
        didSet {
            setupUnderlineView()
            updateButtons()
        }
    }
    
    init() {
        super.init(frame: .zero)
        setupScrollView()
        setupStackView()
        backgroundColor = .yellow
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private func setupScrollView() {
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.isPagingEnabled = true
        addSubview(scrollView)
        
        NSLayoutConstraint.activate([
            scrollView.leadingAnchor.constraint(equalTo: leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: trailingAnchor),
            scrollView.topAnchor.constraint(equalTo: topAnchor),
            scrollView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    private func setupStackView() {
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.spacing = 10 // Adjust the spacing between buttons
        scrollView.addSubview(stackView)
        
        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            stackView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            stackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            stackView.heightAnchor.constraint(equalToConstant: 40) // Adjust the height as needed
        ])
    }
    
    private func setupUnderlineView() {
        if underlineView.superview == nil {
            scrollView.addSubview(underlineView)
        }
        underlineView.backgroundColor = .blue
        
        scrollView.superview?.layoutIfNeeded()
        underlineView.frame.origin.y = scrollView.frame.height - 2
        underlineView.frame.size.height = 2
    }
    
    private func updateButtons() {
        stackView.arrangedSubviews.forEach { $0.removeFromSuperview() }
        
        for (index, item) in items.enumerated() {
            let button = UIButton(type: .system)
            button.setTitle(item, for: .normal)
            button.titleLabel?.font = UIFont.systemFont(ofSize: 14) // Customize the font
            button.setTitleColor(.black, for: .normal) // Customize the button color
            button.addTarget(self, action: #selector(buttonTapped(_:)), for: .touchUpInside)
            button.tag = index
            stackView.addArrangedSubview(button)
            
            if index == 0 {
                button.superview?.layoutIfNeeded()
                self.underlineView.frame.size.width = button.frame.size.width
                self.underlineView.center.x = button.center.x
            }
        }
        
    }
    
    @objc private func buttonTapped(_ sender: UIButton) {
        let selectedIndex = sender.tag
        itemTapped?(selectedIndex)
        moveUnderline(to: sender)
    }
    
    private func moveUnderline(to button: UIButton) {
        UIView.animate(withDuration: 0.3) {
            self.underlineView.frame.size.width = button.frame.size.width
            self.underlineView.center.x = button.center.x
        }
    }
    
    /// call this function when all the parameter is set
    override func layoutSubviews() {
        self.superview?.layoutSubviews()
        super.layoutSubviews()
        scrollView.contentSize = stackView.frame.size
        setupUnderlineView()
        updateButtons()
    }
}


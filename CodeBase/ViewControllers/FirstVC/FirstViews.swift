//
//  FirstVCViews.swift
//  CodeBase
//
//  Created by KurtHo on 2020/11/30.
//

import UIKit

class FirstViews: NSObject {
    var superview: UIView
    
    var segmentControl: PageSegmentControl = {
        var sc = PageSegmentControl()
        sc.commaSeparatedButtonTitles = "Skills,Experience,Other"
        sc.textColor = .gray
        sc.backgroundColor = .yellow
        sc.isFill = true
        sc.accessibilityLabel = "test"
        sc.isAccessibilityElement = true
        sc.accessibilityIdentifier = "testkk"
        return sc
    }()
    
    var scrollView: PageScrollView = {
        var sv = PageScrollView()
        sv.showsVerticalScrollIndicator = false
        sv.isDirectionalLockEnabled = true
        sv.backgroundColor = .orange
        sv.isPagingEnabled = true
        sv.isDirectionalLockEnabled = true
        return sv
    }()
    
    var tableView: BaseTableView = {
        var tv = BaseTableView()
        tv.separatorColor = .lightGray
        tv.backgroundColor = .red
        tv.showsHorizontalScrollIndicator = false
        tv.showsVerticalScrollIndicator = false
//        tv.alwaysBounceHorizontal = false
        tv.isDirectionalLockEnabled = true
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    var button1: UIButton = {
        var btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("button1", for: .normal)
        btn.backgroundColor = .purple
        return btn
    }()
    
    var button2: UIButton = {
        var btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("button2", for: .normal)
        btn.backgroundColor = .brown
        return btn
    }()
    
    var naviBarButton: UIBarButtonItem = {
        var nbb = UIBarButtonItem(title: "nextVC", style: .plain, target: nil, action: nil)
        return nbb
    }()
    
    
    init(superview: UIView){
        self.superview = superview
        
        super.init()
        segmentControl.delegate = self
    }
    
    
    func setPosition() {
        [segmentControl, scrollView].forEach {
            self.superview.addSubview($0)
        }
        
//        segmentControl.setAnchor(top: superview.safeAreaLayoutGuide.topAnchor, leading: superview.leadingAnchor, trailing: superview.trailingAnchor, bottom: nil, height: 40, width: nil)
        
        segmentControl.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(40)
            make.bottom.equalTo(superview.safeAreaLayoutGuide.snp.top).offset(40)
        }
        
        scrollView.setAnchor(top: segmentControl.bottomAnchor, leading: superview.leadingAnchor, trailing: superview.trailingAnchor, bottom: superview.safeAreaLayoutGuide.bottomAnchor, height: nil, width: nil)
        
        scrollView.setContentViews([tableView, button1, button2], superview: superview)

        scrollView.pageDelegate = self
    }
    
}

extension FirstViews: PageScrollViewDelegate {
    func currentPage(_ page: Int) {
        segmentControl.selectorSegmentIndex = page
    }
    
}

extension FirstViews: PageSegmentDelegate {
    func buttonTap(toIndex: Int) {
        scrollView.setScrollToPage(toIndex)
    }
}



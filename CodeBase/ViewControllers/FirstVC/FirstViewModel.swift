//
//  FirstViewModel.swift
//  CodeBase
//
//  Created by KurtHo on 2020/12/1.
//

import UIKit

class FirstViewModel {

    var resumeData: Bind<ResumeModel> = {
        var rm = ResumeModel()
        
        rm.contents = [
            TextImgModel(text: "rx first vc", describ: "basic rx table view", img: UIImage()),
            TextImgModel(text: "sign up with validation", describ: "validate text filed logic", img: UIImage()),
            TextImgModel(text: "websocket", describ: "web socket with chat function", img: UIImage()),
            TextImgModel(text: "SimpleMart", describ: "infinite scroll view", img: UIImage()),
            TextImgModel(text: "rx react table", describ: "react when data comes in", img: UIImage()),
            TextImgModel(text: "combine vc", describ: "ios 13 combine method, like rx and bloc of flutter", img: UIImage()),
            TextImgModel(text: "navigation item", describ: "ios 14 only navigation item of selection table", img: UIImage()),
            TextImgModel(text: "navigatioin popover view", describ: "navigaiton popover view", img: UIImage()),
            TextImgModel(text: "operation queue", describ: "dummy server is unstable, sometime no data comes out", img: UIImage()),
            TextImgModel(text: "snap kit", describ: "collapse view by scrolling", img: UIImage()),
            TextImgModel(text: "complicated vc", describ: "collapse view by scrolling", img: UIImage()),
            TextImgModel(text: "animate vc", describ: "", img: UIImage()),
            TextImgModel(text: "webView", describ: "", img: UIImage()),
            TextImgModel(text: "BoxBindingVC", describ: "", img: UIImage()),
            TextImgModel(text: "展開的vc", describ: "", img: UIImage()),
            TextImgModel(text: "ExpansionVC", describ: "這塊是用chatGPT建出來的", img: UIImage()),
            TextImgModel(text: "GoogleMapAdjustVC", describ: "google map的滑動錨定", img: UIImage()),
            TextImgModel(text: "SegmentScrollView", describ: "line today的滑動", img: UIImage()),
            TextImgModel(text: "Cathy", describ: "search, dispatchGroup", img: UIImage()),
            TextImgModel(text: "CovertImageToPdfVC", describ: "get local photo and convert to pdf", img: UIImage()),

        ]
        var bind = Bind(value: rm)
        return bind
    }()
    
    
}


//
//  ViewController.swift
//  CodeBase
//
//  Created by KurtHo on 2020/11/30.
//

import UIKit

class FirstVC: BaseVC {
    var scrollY: CGFloat = 0
//    var tabBarHiding = false
    
    lazy var views = FirstViews(superview: self.view)
    private let vm = FirstViewModel()
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .all // or specify specific orientations using UIInterfaceOrientationMask constants
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setViews()
        setTableView()
        setNaviItem()
        navigationController?.navigationBar.isTranslucent = false
        
        debug("family in UIFont.familyNames")
//        for family in UIFont.familyNames.sorted() {
//            let names = UIFont.fontNames(forFamilyName: family)
//            print("Family: \(family) Font names: \(names)")
//        }
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
//            let vc = CovertImageToPdfVC()
//            self.show(vc, sender: true)
//        }
    }
     

    //// TableView datasoure and delegate
    func scrollViewShouldScrollToTop(scrollView: UIScrollView) -> Bool {
        return true
    }
    
    private func setViews() {
        views.setPosition()
        views.scrollView.delegate = self
    }
    
    private func setTableView() {
        views.tableView.delegate = self
        views.tableView.dataSource = self
    }
    
    
    private func setNaviItem() {
        views.naviBarButton.target = self
        views.naviBarButton.action = #selector(toSecondVC)
        navigationItem.rightBarButtonItem  = views.naviBarButton
    }

    @objc private func toSecondVC() {
        let vc = ThirdVC()
//        let vc = InfiniteScrollVC()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc private func toAlertView(){
        let alertVC = AlertViewVC()
        self.navigationController?.present(alertVC, animated: true, completion: nil)
    }
}


//MARK:- UITableView DataSource
extension FirstVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vm.resumeData.value.contents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = FirstTableViewCell()
        cell.setContent(data: vm.resumeData.value.contents[indexPath.row])
        
        return cell
    }

}

//MARK:- UITableView Delegate
extension FirstVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("row: \(indexPath.row)")
//        guard let vc = vm.resumeData.value.contents[indexPath.row].vc else {return}
        let vc: UIViewController
        switch indexPath.row {
        case 0:
            vc = RxTableViewVC()
        case 1:
            vc = RxSignUpValidationVC()
        case 2:
            vc = RxWebScoketVC()
        case 3:
            vc = SimpleMartVC()
        case 4:
            vc = RxReactTableVC()
        case 5:
            vc = CombineVC()
        case 6:
            if #available(iOS 14.0, *) {
                vc = NavigationItemVC()
            }else {
                vc = UIViewController()
            }
        case 7:
            if #available(iOS 14.0, *) {
                vc = NavigationPopoverVC()
            }else {
                vc = UIViewController()
            }
        case 8:
            if #available(iOS 14.0, *) {
                vc = QueueVC()
            }else {
                vc = UIViewController()
            }
        case 9:
            vc = SnapKitVC()
        case 10:
            vc = ComplicatedVC()
        case 11:
            vc = AnimateVC()
        case 12:
            vc = WebViewVC()
        case 13:
            vc = BoxBindingVC()
        case 14:
            vc = ExpandableVC()
        case 15:
            vc = ExpansionVC()
        case 16:
            vc = GoogleMapAdjustVC()
        case 17:
            vc = SegmentScrollVC()
        case 18:
//            vc = UINavigationController(rootViewController: CathyDemoVC())
            vc = CathyDemoVC()
        case 19:
            vc = CovertImageToPdfVC()
            
        default: vc = UIViewController()
        }
        self.show(vc, sender: true)
        
    }
}

//MARK: TableView Collapse
extension FirstVC {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {

        let targetView = views.segmentControl
        let height:CGFloat = 40
        targetView.snp.updateConstraints { make in
            if scrollY > height {
                make.bottom.equalTo(view.safeAreaLayoutGuide.snp.top).offset(0)
            }else if scrollY < 1 {
                make.bottom.equalTo(view.safeAreaLayoutGuide.snp.top).offset(height)
            }else {
                make.bottom.equalTo(view.safeAreaLayoutGuide.snp.top).offset(-scrollY + height)
            }
        }
        
        scrollY = scrollView.contentOffset.y
    }
}


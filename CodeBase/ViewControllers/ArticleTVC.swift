//
//  ArticleTVC.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/1/6.
//

import UIKit

class ArticleTVC: UITableViewCell {
    
    var article: ArticleModel!
    
    let imgView:ImageView = {
        let iv = ImageView()
        iv.backgroundColor = .gray
        iv.layer.cornerRadius = 5
        iv.clipsToBounds = true
        return iv
    }()
    
    let label:UILabel = {
        let lb = UILabel()
        lb.numberOfLines = 2
        return lb
    }()
    
    func setContent(article: ArticleModel){
        self.article = article
        label.text = article.title
        imgView.setImage(imgString: article.urlToImage, defaultImg: .aBei)
        
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        imgView.image = nil
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        
        contentView.addSubview(imgView)
        contentView.addSubview(label)
        
        let height: CGFloat = 60
        imgView.setCenterAnchor(centerX: nil, centerY: centerYAnchor)
        imgView.setAnchor(top: nil, leading: leadingAnchor, trailing: nil, bottom: nil, height: height, width: height, padding: UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8))
        
        label.setCenterAnchor(centerX: nil, centerY: centerYAnchor)
        label.setAnchor(top: nil, leading: imgView.trailingAnchor, trailing: trailingAnchor, bottom: nil, height: nil, width: nil, padding: UIEdgeInsets(top: 0, left: 8, bottom: 0, right: -8))
    }

    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}


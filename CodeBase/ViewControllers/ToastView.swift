//
//  ToastView.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2023/5/25.
//

import UIKit

class ToastView: UIView {
    private let messageLabel = UILabel()

    init(message: String) {
        super.init(frame: CGRect.zero)
        
        configureUI()
        configureMessageLabel(message)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func configureUI() {
        // Customize the appearance of the toast view
        self.backgroundColor = UIColor.black.withAlphaComponent(0.7)
    }
    
    private func configureMessageLabel(_ message: String) {
        messageLabel.text = message
        messageLabel.textColor = UIColor.white
        messageLabel.font = UIFont.systemFont(ofSize: 14)
        messageLabel.lineBreakMode = .byWordWrapping
        messageLabel.textAlignment = .center
        messageLabel.numberOfLines = 0
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(messageLabel)
        
        // Position the message label within the toast view
        NSLayoutConstraint.activate([
            messageLabel.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            messageLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            messageLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            messageLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8)
        ])
    }
}

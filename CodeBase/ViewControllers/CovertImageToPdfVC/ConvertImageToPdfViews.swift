//
//  ConvertImageToPdfViews.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2023/10/2.
//

import UIKit
import PDFKit
import Photos

class ConvertImageToPdfViews: BaseView<BaseViewModel> {
    let pdfView: PDFView = {
        let pv = PDFView()
        pv.translatesAutoresizingMaskIntoConstraints = false
        return pv
    }()
    
    let photoBtn: UIButton = {
        let btn = UIButton()
        btn.backgroundColor = .green
        btn.setTitleColor(.darkGray, for: .normal)
        btn.setTitle("本地相簿", for: .normal)
        return btn
    }()
    
    override func addViews() {
        [photoBtn, pdfView].forEach {
            addSubview($0)
        }
    }
    
    override func setConstraints() {
        pdfView.snp.makeConstraints { make in
            make.leading.top.trailing.equalToSuperview()
            make.bottom.equalTo(photoBtn.snp.top)
        }
        
        photoBtn.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.bottom.equalToSuperview().inset(20)
            make.height.equalTo(40)
            make.width.equalTo(80)
        }
    }
    
    
    func convertImageToPDF(image: UIImage) -> Data? {
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, CGRect(origin: .zero, size: image.size), nil)
        UIGraphicsBeginPDFPage()

        image.draw(in: CGRect(origin: .zero, size: image.size))
        
        UIGraphicsEndPDFContext()
        return pdfData as Data
    }

    
    func toPdf(img: UIImage?) {
        if
            let image = img,
            let pdfData = convertImageToPDF(image: image)
        {
                let pdfURL = FileManager.default.temporaryDirectory.appendingPathComponent("output.pdf")
            do {
                try pdfData.write(to: pdfURL)
                    if let document = PDFDocument(url: pdfURL) {
                        self.pdfView.document = document
                        self.pdfView.autoScales = true
                    }
                    
                debug("PDF saved at: \(pdfURL)")
            } catch {
                debug("Error saving PDF: \(error)")
            }

        } else {
            debug("Image not found.")
        }
    }
    
    
    func getLocalPhoto() {
        let status = PHPhotoLibrary.authorizationStatus()

        if status == .authorized {
            debug("user is already authorized to local photo")
            
        } else if status == .denied || status == .restricted {
            debug("user is denied or restricted")
            
        } else {
            PHPhotoLibrary.requestAuthorization { status in
                if status == .authorized {
                    debug("user is authorized")
                } else {
                    debug("Permission denied, handle accordingly.")
                }
            }
        }

    }
}

//
//  File.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2023/10/2.
//

import UIKit

class CovertImageToPdfVC: UIViewController {
    
    var views: ConvertImageToPdfViews!
    let vm = ConvertImageToPdfViewModel()
    
    let imagePickerController: UIImagePickerController = {
        let ipc = UIImagePickerController()
        ipc.sourceType = .photoLibrary
        return ipc
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        views = ConvertImageToPdfViews.init(vm: vm, owner: self)
        views.getLocalPhoto()
        binding()
        title = "image to pdf/tiff"
    }
    
    func binding() {
        imagePickerController.delegate = self
        views.photoBtn.addTarget(self, action: #selector(openPhotoPicker), for: .touchUpInside)
        vm.selectedImage.bindUp(listener: { _ in
            self.views.toPdf(img: self.vm.selectedImage.value)
        })
        
    }

    
    @objc func openPhotoPicker() {
        present(imagePickerController, animated: true, completion: nil)
    }
    
}

extension CovertImageToPdfVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        if let selectedImage = info[.originalImage] as? UIImage {

            vm.selectedImage.value = selectedImage
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}

//
//  ImageView.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/1/6.
//

import UIKit

class ImageView: UIImageView {
    var imgString:String?
    
    func setImage(imgString:String?, defaultImg: UIImage?){
        self.imgString = imgString
        if imgString == nil {
            self.image = defaultImg
        }else {
            
            api.downloadImage(urlStr: imgString, defaultImage: defaultImg) { [weak self] img, str in
                guard let self = self, let imgStr = self.imgString else {return}
                
                if imgStr == str {
                    DispatchQueue.main.async { [weak self] in
                        self?.image = img
                    }
                }
                
            }
        }
    }
    
}


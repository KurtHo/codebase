//
//  RxSignUpValidationViewModel.swift
//  CodeBase
//
//  Created by CI-Kurt on 2021/9/30.
//

import UIKit
import RxSwift
import RxCocoa

protocol RxSignValidationOutput {
    func tapSend(_ value:String)
}

protocol RxSignValidationType {
    var output:RxSignValidationOutput {get}
}

class RxSignUpValidationViewModel:BaseViewModel, RxSignValidationType, RxSignValidationOutput {
    
    var output: RxSignValidationOutput {return self}
    
    func tapSend(_ value:String){
        print(value + "....")
    }
    
    func isEven(num:Int) -> Observable<Int> {
        return Observable.create{ ob in
            if num % 2 == 0 {
                ob.onNext(num)
                ob.onCompleted()
            }else {
                ob.onError(NSError(domain: "不是偶數", code: -1, userInfo: nil))
            }
            
            return Disposables.create()
        }
    }
}

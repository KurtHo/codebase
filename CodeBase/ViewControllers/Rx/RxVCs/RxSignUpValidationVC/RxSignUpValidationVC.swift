//
//  RxSignUpValidationVC.swift
//  CodeBase
//
//  Created by CI-Kurt on 2021/9/30.
//

import UIKit
import RxSwift
import RxCocoa

class RxSignUpValidationVC: BaseVC {

    var views: RxSignUpValidationViews!
    let viewModel = RxSignUpValidationViewModel()
    
    override func loadView() {
        super.loadView()
        views = RxSignUpValidationViews.init(vm: viewModel, owner: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "RxSignUpValidationVC"
        
        views.tfLogic()
        binding()
    }
    
    func binding() {
        views.sendButton.rx.tap.subscribe(onNext: { [weak self] _ in
            self?.viewModel.output.tapSend("send button")
            
        }).disposed(by: disposeBag)
    }

 
}

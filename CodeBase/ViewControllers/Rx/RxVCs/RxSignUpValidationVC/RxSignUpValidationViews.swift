//
//  RxSignUpValidationViews.swift
//  CodeBase
//
//  Created by CI-Kurt on 2021/9/30.
//

import UIKit
import RxSwift
import RxCocoa

class RxSignUpValidationViews: BaseView<RxSignUpValidationViewModel> {
    let length:Int = 5
    
    let userNameTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = " user name"
        tf.layer.borderWidth = 2
        tf.layer.borderColor = UIColor.darkGray.cgColor
        
        return tf
    }()
    
    let passwordTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = " password"
        tf.layer.borderWidth = 2
        tf.layer.borderColor = UIColor.darkGray.cgColor
        return tf
    }()
    
    let passwordRepeatTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = " password again"
        tf.layer.borderWidth = 2
        tf.layer.borderColor = UIColor.darkGray.cgColor
        return tf
    }()
    
    let sendButton: UIButton = {
        let btn = UIButton()
        btn.backgroundColor = .red
        btn.setTitle("send", for: .normal)
        btn.setTitleColor(.black, for: .normal)
        return btn
    }()
    
    lazy var userNameLabel: UILabel = {
        let lb = UILabel()
        lb.text = "Username has to be at least \(self.length)"
        lb.textColor = .red
        return lb
    }()
    
    lazy var passwordLabel: UILabel = {
        let lb = UILabel()
        lb.text = "Password has to be at least \(self.length)"
        lb.textColor = .red
        return lb
    }()
    
    lazy var passwordRepeatLabel: UILabel = {
        let lb = UILabel()
        lb.text = "Password is not valid"
        lb.isHidden = true
        lb.textColor = .red
        return lb
    }()
    
    
    override func addViews() {
        [userNameTextField, passwordTextField, passwordRepeatTextField, sendButton, userNameLabel, passwordLabel, passwordRepeatLabel].forEach {
            addSubview($0)
        }
    }
    
    override func setConstraints() {
        
        let height:CGFloat = 45
        let edgeInsets = UIEdgeInsets(top: 20, left: 20, bottom: 0, right: -20)
        userNameTextField.setAnchor(top: safeAreaLayoutGuide.topAnchor, leading: leadingAnchor, trailing: trailingAnchor, bottom: nil, height: height, width: nil, padding: edgeInsets)
        
        userNameLabel.setAnchor(top: userNameTextField.bottomAnchor, leading: leadingAnchor, trailing: trailingAnchor, bottom: nil, height: height/2, width: nil, padding: UIEdgeInsets(top: 0, left: 20, bottom: 0, right: -20))
        
        passwordTextField.setAnchor(top: userNameLabel.bottomAnchor, leading: leadingAnchor, trailing: trailingAnchor, bottom: nil, height: height, width: nil, padding: edgeInsets)
        
        passwordLabel.setAnchor(top: passwordTextField.bottomAnchor, leading: leadingAnchor, trailing: trailingAnchor, bottom: nil, height: height/2, width: nil, padding: UIEdgeInsets(top: 0, left: 20, bottom: 0, right: -20))
        
        passwordRepeatTextField.setAnchor(top: passwordLabel.bottomAnchor, leading: leadingAnchor, trailing: trailingAnchor, bottom: nil, height: height, width: nil, padding: edgeInsets)

        passwordRepeatLabel.setAnchor(top: passwordRepeatTextField.bottomAnchor, leading: leadingAnchor, trailing: trailingAnchor, bottom: nil, height: height, width: nil, padding: UIEdgeInsets(top: 0, left: 20, bottom: 0, right: -20))
        
        sendButton.setAnchor(top: passwordRepeatLabel.bottomAnchor, leading: leadingAnchor, trailing: trailingAnchor, bottom: nil, height: height, width: nil, padding: edgeInsets)
        
    }
    
    func tfLogic() {
        
        let userNameValid = userNameTextField.rx.text.orEmpty.map{ [weak self] value in
            value.count >= self?.length ?? 0
        }.share(replay: 1)
        let pswdValid = passwordTextField.rx.text.orEmpty.map{[weak self] value in
            value.count >= self?.length ?? 0
        }.share(replay: 1)
        let pswdRepeatValid = passwordRepeatTextField.rx.text.orEmpty.map{[weak self] value in
            value.count >= self?.length ?? 0
        }.share(replay: 1)

        let pswdIsSame = Observable.combineLatest(passwordTextField.rx.text.orEmpty, passwordRepeatTextField.rx.text.orEmpty) {
            pswd, pswdRepeat -> Bool in
            return pswd == pswdRepeat
        }

        let allValid = Observable.combineLatest(userNameValid, pswdValid, pswdRepeatValid, pswdIsSame) { [weak self]
            userName, pswd, pswdRepeat, pswdSame -> Bool in
            let result = userName && pswd && pswdRepeat && pswdSame
            self?.sendButton.backgroundColor = result ? .green : .red
            return result
        }

        userNameValid.bind(to: passwordTextField.rx.isEnabled).disposed(by: disposeBag)
        userNameValid.bind(to: userNameLabel.rx.isHidden).disposed(by: disposeBag)
        pswdValid.bind(to: passwordLabel.rx.isHidden).disposed(by: disposeBag)
        allValid.bind(to: sendButton.rx.isEnabled).disposed(by: disposeBag)
        pswdIsSame.bind(to: passwordRepeatLabel.rx.isHidden).disposed(by: disposeBag)

    }

}

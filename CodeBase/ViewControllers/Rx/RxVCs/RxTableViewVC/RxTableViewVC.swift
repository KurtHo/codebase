//
//  SecondVC.swift
//  RxRx
//
//  Created by CI-Kurt on 2021/9/13.
//

import UIKit

class RxTableViewVC: BaseVC {
    
    var views: RxTableViewView!
    let viewModel = RxTableViewModel()
    
    override func loadView() {
        super.loadView()
        views = RxTableViewView.init(vm: viewModel, owner: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "RxTableViewVC"
        binding()
        
        // only protrait
        AppUtility.lockOrientation(.portrait)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Don't forget to reset when view is being removed
        AppUtility.lockOrientation(.all)
    }
    
    func binding() {
        viewModel.output.getData { [weak self] (data) in
            self?.views.getDataAndReload(secondModel: data)
        }

        views.tableView.rx.modelSelected(SecondModel.self).subscribe(onNext: { [weak self] value in
            let str = "tap: \(value.name.first)"
            self?.viewModel.cellTapAction(str)
            
        }).disposed(by: disposeBag)
        
        views.floattingButton.rx.tap.subscribe(onNext: { [weak self] in
            self?.viewModel.floattingBtnTap()
        }).disposed(by: disposeBag)
    }
    
}

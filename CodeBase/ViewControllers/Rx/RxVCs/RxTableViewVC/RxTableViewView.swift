//
//  SecondViews.swift
//  RxRx
//
//  Created by CI-Kurt on 2021/9/13.
//

import UIKit
import RxSwift
import RxCocoa

class RxTableViewView: BaseView<RxTableViewModel> {
        
    lazy var tableView:UITableView = {
        let tv = UITableView()
        tv.backgroundColor = .white
        return tv
    }()
    
    
    lazy var floattingButton:UIButton = {
        let btn = UIButton()
        btn.setTitle("Btn", for: .normal)
        btn.backgroundColor = .blue
        
        return btn
    }()

    override func addViews() {
        [tableView].forEach{
            addSubview($0)
        }
        tableView.addSubview(floattingButton)
    }
    
    override func setConstraints() {
        tableView.setAnchor(top: safeAreaLayoutGuide.topAnchor, leading: leadingAnchor, trailing: trailingAnchor, bottom: safeAreaLayoutGuide.bottomAnchor, height: nil, width: nil)
        
        floattingButton.setAnchor(top: nil, leading: nil, trailing: tableView.safeAreaLayoutGuide.trailingAnchor, bottom: tableView.safeAreaLayoutGuide.bottomAnchor, height: 60, width: 60, padding: UIEdgeInsets(top: 0, left: 0, bottom: -15, right: -15))
        
//        floattingButton.layoutIfNeeded()
//        floattingButton.layer.cornerRadius = floattingButton.frame.height / 2
//        floattingButton.clipsToBounds = true
        floattingButton.setRoundedShape()
    }
        
    func getDataAndReload(secondModel: [SecondModel]?){
        if let model = secondModel {
            setCellContent(data: model)
        }
    }
    
}

extension RxTableViewView {
    private func setCellContent(data: [SecondModel]){
        let items = Observable.just(data.map{$0})
        items.bind(to: tableView.rx.items) {  (tableView, row, element) in
            let cell = UITableViewCell()
            cell.backgroundColor = .white
            cell.textLabel?.text = "\(element.name.first) @ row \(row)"
            cell.textLabel?.textColor = .black
            return cell
        }
        .disposed(by: disposeBag)
    }

}





//
//  SecondViewModel.swift
//  RxRx
//
//  Created by CI-Kurt on 2021/9/14.
//

import UIKit
import RxSwift
import RxCocoa

protocol RxFirstVMOutput {
    func getData(finish: @escaping  ([SecondModel]? ) -> Void)
    func cellTapAction(_ str:String)
}
protocol RxFirstVMType {
    var output:RxFirstVMOutput {get}
}

class RxTableViewModel: BaseViewModel, RxFirstVMOutput, RxFirstVMType {
    
    var secondModels:[SecondModel]? = nil
    var output: RxFirstVMOutput {return self}
    
    override init() {
        super.init()
    }
    
    func getData(finish: @escaping ([SecondModel]? ) -> Void) {
        let str = "https://api.randomuser.me/?results=20"
        Api.get(urlStr: str, model: SecondModelResult.self) { model, err in
            guard let model = model, err == nil else {
                finish(nil)
                return
            }
            self.secondModels = model
            finish(model)
        }
    }
    
    func cellTapAction(_ str:String){
        print(str)
    }
    
    func floattingBtnTap(){
        print("floatting btn")
    }

    
    func isEven(number: Int) -> Observable<Int> {
        // 1
        return Observable.create { observer in
            if number % 2 == 0 {
                  // 2
                observer.onNext(number)
                observer.onCompleted()
            } else {
                  // 3
                observer.onError(NSError.init(domain: "不是偶數", code: 401, userInfo: nil))
            }
            // 4
            return Disposables.create()
        }
    }
    
    func someObject() {
        let subject = PublishSubject<String>()

        subject.onNext("1")
        // 1, 2
        subject.debug("A")
            .subscribe().disposed(by: disposeBag)

        subject.onNext("2")

        subject.onNext("3")

        subject.debug("B")
            .subscribe().disposed(by: disposeBag)
        // 3
        subject.onError(NSError(domain: "Test", code: -1, userInfo: nil))
    }
    
}



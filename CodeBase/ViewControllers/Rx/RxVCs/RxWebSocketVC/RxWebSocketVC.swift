//
//  RxWebSocketVC.swift
//  CodeBase
//
//  Created by KurtHo on 2021/12/10.
//

import UIKit
import RxSwift
import RxCocoa

class RxWebScoketVC: BaseVC {
    
    var views: RxWebSocketViews!
    let viewModel = RxWebSocketViewModel()
    
    override func loadView() {
        super.loadView()
        viewModel.setManager()
        views = RxWebSocketViews.init(vm: viewModel, owner: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addKeyboardObserver()
        binding()
        setNameWithAlert()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeKeyboardObserver()
    }
    
    func binding(){

        viewModel.socketModel.asObservable().subscribe(onNext: { [weak self] model in
            print("system: \(model.systemMessage)")
            self?.views.tipsLabel.text = model.systemMessage
            self?.views.msgLabel.text = model.message
            self?.views.textField.text = ""
        }).disposed(by: disposeBag)

        views.sendMessage.rx.tap.subscribe(onNext: { [weak self] _ in
            if let text = self?.views.textField.text, text != "" {
                self?.viewModel.sendMessage(text)
            }
            
        }).disposed(by: disposeBag)
        
    }
    
    func setNameWithAlert() {
        DispatchQueue.main.async {
            let alertVC = self.views.showAlertToSetName { nameStr in
                self.viewModel.setName(nameStr: nameStr)
                
            } cancel: {
                self.navigationController?.popViewController(animated: true)
            }
            self.present(alertVC, animated: true)
        }
    }

}

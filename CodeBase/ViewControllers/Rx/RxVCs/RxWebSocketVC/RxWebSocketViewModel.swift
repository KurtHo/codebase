//
//  RxWebSocketViewModel.swift
//  CodeBase
//
//  Created by KurtHo on 2021/12/10.
//

import Foundation
import Starscream
import RxSwift

class RxWebSocketViewModel: BaseViewModel {
    var isConnected = false
    var socketManager:WebSocketManager = WebSocketManager()
    var socketModel = BehaviorSubject<RxWebSocketModel>(value: RxWebSocketModel())
    
    
    func setManager() {
        socketManager.setConnection(vc: self)
    }
    
    func setName(nameStr:String) {
        socketManager.setName(nameStr)
    }
    
    func sendMessage(_ msg: String) {
        socketManager.sendPubMessage(str: msg, completion: nil)
    }

}

extension RxWebSocketViewModel:WebSocketDelegate  {
    func didReceive(event: WebSocketEvent, client: WebSocket) {
        switch event {
        case .connected(let headers):
            isConnected = true
            print("websocket is connected: \(headers)")
            
        case .disconnected(let reason, let code):
            isConnected = false
            print("websocket is disconnected: \(reason) with code: \(code)")
            
        case .text(let string):
            // 我後端沒寫好，所以不好使用codable
            print("Received text: \(string)")
    
            if let data = string.data(using: .utf8), let converted = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: String] {
                let _model = RxWebSocketModel()
                _model.systemMessage = converted["systemMessage"] ?? ""
                _model.name = converted["name"]
                _model.action = converted["action"] ?? ""
                _model.message = converted["publicMessage"] ?? ""
                self.socketModel.onNext(_model)
            }
            
        case .binary(let data):
            print("Received data: \(data.count)")
        case .ping(_):
            break
        case .pong(_):
            break
        case .viabilityChanged(_):
            break
        case .reconnectSuggested(_):
            break
        case .cancelled:
            isConnected = false
        case .error(let error):
            isConnected = false
            handleError(error)
        }
    }
    
    private func handleError(_ error: Error?) {
        if let error = error as? WSError {
            debug("error...: \(error.message)")
        }else {
            debug("error...: \(String(describing: error))")
        }
    }
    
}

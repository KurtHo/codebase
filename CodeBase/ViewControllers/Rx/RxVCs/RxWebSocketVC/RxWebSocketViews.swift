//
//  RxWebSocketViews.swift
//  CodeBase
//
//  Created by KurtHo on 2021/12/10.
//

import UIKit

class RxWebSocketViews: BaseView<RxWebSocketViewModel> {
    
    let sendMessage: UIButton = {
        let btn = UIButton()
        btn.backgroundColor = .blue
        btn.setFont(fontName: .PingFangTCRegular, size: 16, color: .white)
        btn.setTitle("send", for: .normal)
        return btn
    }()
    
    let tipsLabel: UILabel = {
        let lb = UILabel()
        lb.setFont(fontName: .PingFangTCSemibold, size: 16, color: .black)
        return lb
    }()
    
    let textField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "type something"
        return tf
    }()
    
    let msgLabel: UILabel = {
        let lb = UILabel()
        return lb
    }()
    
    
    override func addViews() {
        [sendMessage, tipsLabel, textField, msgLabel].forEach{
            addSubview($0)
        }
    }
    
    override func setConstraints() {
        let height:CGFloat = 60
        let textOffset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        textField.setAnchor(top: nil, leading: leadingAnchor, trailing: nil, bottom: safeAreaLayoutGuide.bottomAnchor, height: height , width: nil, padding: UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 0))
        
        sendMessage.setAnchor(top: nil, leading: textField.trailingAnchor, trailing: trailingAnchor, bottom: safeAreaLayoutGuide.bottomAnchor, height: height, width: height * 1.5, padding: textOffset)
        
        tipsLabel.setAnchor(top: safeAreaLayoutGuide.topAnchor, leading: leadingAnchor, trailing: trailingAnchor, bottom: nil, height: height / 2, width: nil)
        
        msgLabel.setAnchor(top: nil, leading: leadingAnchor, trailing: trailingAnchor, bottom: sendMessage.topAnchor, height: height / 2, width: nil, padding: UIEdgeInsets(top: 0, left: 10, bottom: -10, right: 0))
        
        sendMessage.setRoundedShape()
    }
    
    func showAlertToSetName(didSetName: @escaping (String) ->Void, cancel: @escaping () ->Void) -> UIAlertController {
        
        let alertVC = UIAlertController(title: "Name", message: nil, preferredStyle: .alert)
        alertVC.addTextField { (textField : UITextField!) in
            textField.placeholder = "Enter your name"
        }
        let sendAct = UIAlertAction(title: "send", style: UIAlertAction.Style.default) { _ in
            let textField = alertVC.textFields![0] as UITextField
            didSetName(textField.text ?? "")
        }
        let cancelAct = UIAlertAction(title: "cancel", style: .destructive) { _ in
            cancel()
        }
        alertVC.addAction(sendAct)
        alertVC.addAction(cancelAct)
        
        return alertVC
        
    }
    

}


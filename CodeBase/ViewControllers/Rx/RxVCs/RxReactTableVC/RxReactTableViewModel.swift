//
//  RxReactTableViewModel.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/1/6.
//

import Foundation
import RxRelay
import RealmSwift

class RxReactTableViewModel: BaseViewModel {
    var articles: Results<ArticleModel> {
        return rlm.realm.objects(ArticleModel.self)
    }
    
    lazy var relayArticles = BehaviorRelay(value: self.articles)

    
    func getNewsList() {
        print(Realm.Configuration.defaultConfiguration.fileURL ?? "")
        
        api.get(.news(source: .teslaNews), decode: NewsList.self) { [weak self] model, result in
            guard let self = self, let model = model else {return}
            rlm.add(model.rlmArticles, update: .all)
            self.relayArticles.accept(self.articles)
        }
    }
}

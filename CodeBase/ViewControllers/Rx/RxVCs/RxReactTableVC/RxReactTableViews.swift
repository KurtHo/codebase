//
//  RxReactTableViews.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/1/6.
//

import UIKit

class RxReactTableViews: BaseView<RxReactTableViewModel> {

    lazy var tableView: UITableView = {
        let tv = UITableView()
        tv.register(ArticleTVC.self, forCellReuseIdentifier: ArticleTVC.identifier)
        tv.delegate = self
        return tv
    }()
    
    var rightBtn1: UIButton = {
        let btn = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        btn.setImage(.refresh, for: .normal)
        return btn
    }()
    
    override func addViews() {
        addSubview(tableView)
    }
    
    override func setConstraints() {
        tableView.setAnchor(top: safeAreaLayoutGuide.topAnchor, leading: leadingAnchor, trailing: trailingAnchor, bottom: safeAreaLayoutGuide.bottomAnchor, height: nil, width: nil)
        
    }
    
    override func setupReferencingOutlets(owner: AnyObject?) {
        if let reactTableVC = owner as? RxReactTableVC {
            reactTableVC.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: rightBtn1)
        }
    }
    
}

extension RxReactTableViews: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}

//
//  RxReactTableVC.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/1/6.
//

import UIKit

class RxReactTableVC: ToastVC {
    var views: RxReactTableViews!
    let vm = RxReactTableViewModel()
    
    override func loadView() {
        super.loadView()
        views = RxReactTableViews.init(vm: vm, owner: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "React table with realm"
        binding()
        
        vm.getNewsList()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
            self?.showToast(message: "目前某版本的rx生成的table view會有以下warrning: 'ITableView was told to layout its visible cells and other contents without being in the view hierarchy (the table view or one of its superviews has not been added to a window)'")
        }
    }
    
    func binding() {
        views.rightBtn1.rx.tap.subscribe(onNext: { [weak self] in
            self?.vm.getNewsList()
        }).disposed(by: disposeBag)

        
        vm.relayArticles.asObservable().bind(to: views.tableView.rx.items(cellIdentifier: ArticleTVC.identifier, cellType: ArticleTVC.self)) {row, elem, cell in
            cell.setContent(article: elem)
        }.disposed(by: disposeBag)
        
        views.tableView.rx.itemSelected.subscribe(onNext: { [weak self] index in
            if let article = self?.vm.articles[index.row] {
                debug(article.desc ?? "")
            }
        }).disposed(by: disposeBag)
        
    }
    
}

import UIKit

class GoogleMapAdjustVC: UIViewController {

    var scrollView: UIScrollView!
    var svTopConstraint: NSLayoutConstraint!
    private var originPositoin: CGFloat!
    private var movingPositoin: CGFloat!
    
    let horizonView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        scrollView = UIScrollView()
        scrollView.backgroundColor = .red
        scrollView.contentSize = CGSize(width: view.bounds.width, height: view.bounds.height * 2)
        view.addSubview(scrollView)
        
        view.addSubview(horizonView)
        horizonView.backgroundColor = .darkGray
        
        let contentView = UIView()
        contentView.backgroundColor = .orange
        contentView.isUserInteractionEnabled = true
        view.addSubview(contentView)
        
        svTopConstraint = scrollView.topAnchor.constraint(equalTo: view.bottomAnchor, constant: -200)
        originPositoin = svTopConstraint.constant
        

        [scrollView, contentView, horizonView].forEach{
            $0?.translatesAutoresizingMaskIntoConstraints = false
        }
        
        NSLayoutConstraint.activate([
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            svTopConstraint,
            
            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 50),
            contentView.widthAnchor.constraint(equalToConstant: 200),
            contentView.heightAnchor.constraint(equalToConstant: 1400),
            contentView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor),
//            contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: -50),
            
            horizonView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 10),
            horizonView.widthAnchor.constraint(equalToConstant: 100),
            horizonView.heightAnchor.constraint(equalToConstant: 5),
            horizonView.centerXAnchor.constraint(equalTo: view.centerXAnchor)
            
        ])
        
        view.layoutIfNeeded()
        horizonView.layer.cornerRadius = horizonView.frame.height / 2
        horizonView.clipsToBounds = true
        
    
        let panGesture1 = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        let panGesture2 = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(_:)))
        scrollView.addGestureRecognizer(panGesture1)
        contentView.addGestureRecognizer(panGesture2)
    }
    
    @objc func handlePanGesture(_ sender: UIPanGestureRecognizer) {
        
        switch sender.state {
        case .began:
            movingPositoin = .zero
            
        case .changed:
            // Update the top constraint based on the pan gesture
            let translation = sender.translation(in: view)
            let newConstant = svTopConstraint.constant + translation.y
            
            movingPositoin += translation.y
            
            svTopConstraint.constant = newConstant
            sender.setTranslation(CGPoint.zero, in: view)
            
        case .cancelled, .ended:
            
            setMovingView()
            
            svTopConstraint.isActive = true

            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }completion: { _ in
                self.originPositoin = self.svTopConstraint.constant
            }

        default:
            ()
        }
        
    }
    
    private func setMovingView() {
        let panMoved: CGFloat = 70
                
        print("originPositoin: \(String(describing: originPositoin)), movingPositoin: \(String(describing: movingPositoin))")
        if let originPosition = originPositoin {
            if originPosition == -200 {
                if movingPositoin < 0 && abs(movingPositoin) > panMoved {
                    svTopConstraint.constant = -view.bounds.height + 100
                } else if movingPositoin > 0 && abs(movingPositoin) > panMoved {
                    svTopConstraint.constant = -60
                } else {
                    svTopConstraint.constant = -200
                }
            }else if originPosition == -60 {
                if movingPositoin < 0 && abs(movingPositoin) > panMoved {
                    svTopConstraint.constant = -200
                } else {
                    svTopConstraint.constant = -60
                }
            }else {
                if movingPositoin > 0 && abs(movingPositoin) > panMoved {
                    svTopConstraint.constant = -200
                } else {
                    svTopConstraint.constant = -view.bounds.height + 100
                }
        
            }
            
        }
        
    }
}

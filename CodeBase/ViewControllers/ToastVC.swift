//
//  ToastVC.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2023/5/25.
//

import UIKit

class ToastVC: BaseVC {
    func showToast(message: String, duration: TimeInterval = 2.5) {
        let toastView = ToastView(message: message)

        toastView.translatesAutoresizingMaskIntoConstraints = false

        view.addSubview(toastView)

        NSLayoutConstraint.activate([
            toastView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            toastView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            toastView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
//            toastView.heightAnchor.constraint(equalToConstant: toastHeight)
//            toastView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.1)
            
        ])
        
        toastView.layoutIfNeeded()
        let bottomSafeAreaHeight = (UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0) + toastView.bounds.height

        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: {
            toastView.frame.origin.y -= bottomSafeAreaHeight
        }, completion: nil)

        DispatchQueue.main.asyncAfter(deadline: .now() + duration) {
            UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: {
                toastView.frame.origin.y += bottomSafeAreaHeight
            }, completion: { _ in
                toastView.removeFromSuperview()
            })
        }
    }
}

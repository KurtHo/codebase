//
//  CombineVC.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/1/7.
//

import UIKit


@available(iOS 13.0, *)
class CombineVC: BaseVC {
    var views: CombineViews!
    let vm = CombineViewModel()
    
    override func loadView() {
        super.loadView()
        views = CombineViews.init(vm: vm, owner: self)
        title = "Combine VC"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        binding()
        vm.getData { [weak self] in
            self?.views.tableView.reloadData()
        }
    }
    
    func binding(){
        views.tableView.dataSource = self
    }
    
}

@available(iOS 13.0, *)
extension CombineVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vm.newsList?.articles.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CombineArticleTVC.identifier, for: indexPath) as? CombineArticleTVC,
              let articles = self.vm.newsList?.articles else {
                  return UITableViewCell()
              }
                
        cell.setContent(article: articles[indexPath.row])
        cell.action.sink { article in
            debug(article)
        }.store(in: &vm.observers)
        return cell
    }
    
    
}

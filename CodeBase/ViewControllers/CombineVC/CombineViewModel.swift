//
//  ComebineViewModel.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/1/7.
//

import Foundation
import Combine

@available(iOS 13.0, *)
class CombineViewModel: BaseViewModel {
    
    var observers: [AnyCancellable] = []
    var newsList: NewsList?
    
    func getData(finish: @escaping () -> Void) {
        combineApi.getData()?.sink(receiveCompletion: { completion in
            switch completion {
            case .finished:
                debug("get data success")
            case .failure(let err):
                debug("get data failure, err..: \(err)")
            }
        }, receiveValue: { [weak self] newsList in
            self?.newsList = newsList
            finish()
        }).store(in: &observers)
        
    }
}



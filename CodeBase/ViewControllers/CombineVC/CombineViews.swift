//
//  CombineViews.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/1/7.
//

import UIKit

@available(iOS 13.0, *)
class CombineViews: BaseView<CombineViewModel> {

    lazy var tableView: UITableView = {
        let tv = UITableView()
        tv.register(CombineArticleTVC.self, forCellReuseIdentifier: CombineArticleTVC.identifier)
        tv.delegate = self
        return tv
    }()
    
    override func addViews() {
        addSubview(tableView)
    }
    
    override func setConstraints() {
        tableView.setAnchor(top: safeAreaLayoutGuide.topAnchor, leading: leadingAnchor, trailing: trailingAnchor, bottom: safeAreaLayoutGuide.bottomAnchor, height: nil, width: nil)
    }
    
}


@available(iOS 13.0, *)
extension CombineViews: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}

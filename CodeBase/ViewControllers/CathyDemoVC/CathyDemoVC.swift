//
//  CathyDemoVC.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2023/9/12.
//

import UIKit

class CathyDemoVC: UIViewController {

    lazy var views = CathyViews(vm: vm, parentView: view)
    
    let vm = CathyVM()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        navigationController?.navigationBar.isHidden = true
        binding()
        loadData()
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        navigationController?.navigationBar.isHidden = true
        
        let alert = views.showAlert {
            self.vm.getFriend(type: .noFriend) {
                debug("取得無好友列表")
                DispatchQueue.main.async { [weak self] in
                    self?.views.setSearchBar(hide: true)
                    self?.views.setSegmentControl(hide: true)
                }
            } err: {
                debug("err")
            }

        } action2: {
            self.vm.getFriendsData { friends in
                debug("取得好友畫面")
                
                DispatchQueue.main.async { [weak self] in
                    self?.views.setSegmentControl(hide: false)
                    self?.views.setSegmentControlData(friends: friends)
                    self?.views.setSearchBar(hide: false)
                    self?.views.tableView.reloadData()
                }
            }
        } action3: {
            self.vm.getFriendInvitation {
                debug("取得好友邀請列表")
//                self.views.invitationModel = self.vm.invitation.value
                if let invitation = self.vm.invitation.value {
                    self.views.setInvitation(friends: invitation)
                }
                DispatchQueue.main.async { [weak self] in
                    self?.views.hideSegmentInvitationUser(false)
                }
                
            } err: {
                
            }

            self.vm.getFriendsData { friends in
                debug("取得好友畫面")
                
                DispatchQueue.main.async { [weak self] in
                    self?.views.setSegmentControl(hide: false)
                    self?.views.setSegmentControlData(friends: friends)
                    self?.views.setSearchBar(hide: false)
                    self?.views.tableView.reloadData()
                }
            }
            
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.present(alert, animated: true)
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.navigationBar.isHidden = false
        navigationItem.setHidesBackButton(true, animated: false)
    }
        
    private func binding() {
        views.firstBtn.addTarget(self, action: #selector(item1Tapped), for: .touchUpInside)
        views.secondBtn.addTarget(self, action: #selector(item2Tapped), for: .touchUpInside)
        views.thirdBtn.addTarget(self, action: #selector(item3Tapped), for: .touchUpInside)
        let userViewGest = UITapGestureRecognizer(target: self, action: #selector(userViewTapped))
        views.userIdView.addGestureRecognizer(userViewGest)
        views.userIdView.avatarBtn.addTarget(self, action: #selector(avatarTapped), for: .touchUpInside)
        views.emptyFriendView?.addFriendBtn.addTarget(self, action: #selector(addFriendTapped), for: .touchUpInside)
        views.emptyFriendView?.setUserId.addTarget(self, action: #selector(setKokoId), for: .touchUpInside)
        views.refreshControl.addTarget(self, action: #selector(loadFriendData(_:)), for: UIControl.Event.valueChanged)
        
        views.tableView.dataSource = self
        
        vm.friends.bindUp { [weak self] friends in
            guard let self = self, let friends = friends else {return}
            if friends.isEmpty {
                self.views.setEmptyView()
            }else {
                self.views.setFriendsView()
            }
        }
        
    }

    func loadData() {

        vm.getUserData { userModel in
            DispatchQueue.main.async { [weak self] in
                self?.views.userIdView.setUserModel(userModel)
            }
        }
    }
    
    @objc func item1Tapped() {
        debug("")
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func item2Tapped() {
        debug("")
    }
    
    @objc func item3Tapped() {
        views.hideSegmentInvitationUser(true)
        debug("")
    }
    
    @objc func userViewTapped() {
        debug("")
    }
    
    @objc func avatarTapped() {
        debug("avatar")
    }
    
    @objc func addFriendTapped() {
        debug("addFriendBtn")
    }
    
    @objc func setKokoId() {
        debug("setKokoId")
    }
    
    @objc func loadFriendData(_ sender: UIRefreshControl) {
        let needShowing: Bool = vm.friends.value == nil
        self.vm.getFriendsData { friends in
            DispatchQueue.main.async { [weak self] in
                self?.views.tableView.reloadData()
                if needShowing {
                    self?.views.setSegmentControl(hide: false)
                    self?.views.setSearchBar(hide: false)
                    self?.views.setSegmentControlData(friends: friends)
                }
                sender.endRefreshing()
            }
        }
        
    }
    
    deinit {
        debug("some deinit...")
    }
}

extension CathyDemoVC: UITableViewDataSource {
    func getFilterModel(_ friends: [FriendModel]) -> [FriendModel] {
        let data = friends.filter { friendName in
            friendName.name.lowercased().contains((views.searchFilterText ?? "").lowercased())
        }
        
        return data.isEmpty ? friends : data
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let friends = vm.friends.value else {return 0}
        guard !views.searchFilterText.isEmpty else {return friends.count}
 
        return getFilterModel(friends).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: FriendsCell.identifier, for: indexPath) as? FriendsCell {
            guard let friends = vm.friends.value else {return UITableViewCell() }
            let data = getFilterModel(friends)
            cell.setContent(friend: data[indexPath.row])
            return cell
        }else {
            return UITableViewCell()
        }
        
    }
    
}




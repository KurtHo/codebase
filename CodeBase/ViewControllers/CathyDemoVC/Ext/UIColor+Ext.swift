//
//  UIColor+Ext.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2023/9/12.
//

import UIKit

extension UIColor {
    static func setRGB(r:CGFloat, g:CGFloat, b: CGFloat, alpha:CGFloat = 1) -> UIColor {
        return UIColor(red: r/255, green: g/255, blue: b/255, alpha: alpha)
    }
    
    static let hotPink: UIColor = .setRGB(r: 236, g: 0, b: 140)
    
    static let greyishBrown: UIColor = .setRGB(r: 71, g: 71, b: 71)
    
    static let brownGrey: UIColor = .setRGB(r: 153, g: 153, b: 153)
}

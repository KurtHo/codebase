//
//  CathyApi.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2023/9/12.
//
import UIKit

enum ApiError: Error {
    case decodeError
    case unknowError(str:String)
}

class CathyApi {
    
    enum ModelType {
        case noFriend, friend1, friend2, invitation, user
        var type: String {
            switch self {
            case .noFriend:     return FriendsDummyData.noFriendQQ
            case .friend1:      return FriendsDummyData.friend1
            case .friend2:      return FriendsDummyData.friend2
            case .invitation:   return FriendsDummyData.invitation
            case .user:         return UserDummyData.user
            }
        }
    }

    static private let randomDelay = Double(arc4random_uniform(2000)) / 1000.0

    static private let decoder = JSONDecoder()
    
    static func get(modelType: ModelType, cb: @escaping (Result<[FriendModel], ApiError>) -> Void) {
        DispatchQueue.global().asyncAfter(deadline: .now() + randomDelay) {
            
            if let jsonData = modelType.type.data(using: .utf8) {
                do {
                    let respData = try decoder.decode(FriendsOutput.self, from: jsonData)
                    cb(.success(respData.response))
                }catch {
                    cb(.failure(.decodeError))
                }
            }else {
                cb(.failure(.unknowError(str: "encode fail")))
            }
            
        }
        
    }
    
    static func getUser(cb: @escaping (Result<[UserModel], ApiError>) -> Void) {
        DispatchQueue.global().asyncAfter(deadline: .now() + randomDelay) {
            
            if let jsonData = ModelType.user.type.data(using: .utf8) {
                do {
                    let respData = try decoder.decode(UserOutput.self, from: jsonData)
                    cb(.success(respData.response))
                }catch {
                    cb(.failure(.decodeError))
                }
            }else {
                cb(.failure(.unknowError(str: "encode fail")))
            }
            
        }
        
    }
    
}

extension CathyApi {
    static func downloadImage(urlStr: String?, defaultImage: UIImage? = nil, finish: @escaping (UIImage?, String?)->Void, errCB: ((ApiError)->Void)? = nil){
        guard let urlString = urlStr, let url = URL(string: urlString)  else {
            finish(defaultImage, urlStr)
            return
        }
        
        if let image = Caches.shared.image.object(forKey: urlString as NSString) {
            finish(image, urlString)
        }else{
            URLSession.shared.dataTask(with: url){ data, resp, err in
                if let err = err {
                    debug("dl image err: \(err.localizedDescription)")
                    errCB?(.unknowError(str: "encode fail"))
                    finish(defaultImage, urlStr)
                }
                guard let data = data, let image = UIImage(data: data) else {
                    finish(defaultImage, urlStr)
                    return
                }
                DispatchQueue.main.async {
                    Caches.shared.image.setObject(image, forKey: urlString as NSString)
                    finish(image, urlString)
                }
  
            }.resume()
        }
    }
}

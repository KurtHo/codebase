//
//  TabBarController.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2023/9/12.
//
//import UIKit
//
//class CathyTabBarController: UITabBarController {
//
//    lazy var contentView = TabBarViews(parentView: view)
//
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        setVCs()
//        binding()
//        setViews()
//        contentView.setViews()
//    }
//
//    private func setVCs() {
//        let firstVC = FirstVC()
//
//        let secondVC = UINavigationController(rootViewController: SecondVC())
//
//        let thirdVC = ThirdVC()
//
//        let fourthVC = FourthVC()
//
//        let fifthVC = FifthVC()
//
//        viewControllers = [firstVC, secondVC, thirdVC, fourthVC, fifthVC]
//    }
//
//    private func setViews() {
//        tabBar.isHidden = true
//        selectedIndex = 0
//    }
//
//    private func binding() {
//        contentView.isUserInteractionEnabled = true
//        [
//            contentView.firstBtn,
//            contentView.secondBtn,
//            contentView.thirdBtn,
//            contentView.fourthBtn,
//            contentView.fifthBtn
//        ].forEach {
//            $0.addTarget(self, action: #selector(btnTapped(_:)), for: .touchUpInside)
//        }
//    }
//
//    @objc private func btnTapped(_ sender: UIButton) {
//        if let index = contentView.subviews.firstIndex(of: sender) {
//            selectedIndex = index
//            debug("index: \(index)")
//        }
//    }
//
//}
//
//
//class TabBarViews: BaseImageViews {
//
//
//    override init(parentView: UIView) {
//        super.init(parentView: parentView)
//        image = UIImage(named: "imgTabbarBg")
//    }
//
//
//    required init?(coder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//
//    let firstBtn: UIButton = {
//        let btn = UIButton()
//        btn.setImage(UIImage(named: "tab1Unselect"), for: .normal)
//        btn.setImage(UIImage(named: "tab1select"), for: .selected)
//        return btn
//    }()
//
//    let secondBtn: UIButton = {
//        let btn = UIButton()
//        btn.setImage(UIImage(named: "tab2Select"), for: .normal)
//        btn.setImage(UIImage(named: "tab2Select"), for: .selected)
//        return btn
//    }()
//
//    let thirdBtn: UIButton = {
//        let btn = UIButton()
//        btn.setImage(UIImage(named: "tab3Unselect"), for: .normal)
//        btn.setImage(UIImage(named: "tab3Unselect"), for: .selected)
//        return btn
//    }()
//
//    let fourthBtn: UIButton = {
//        let btn = UIButton()
//        btn.setImage(UIImage(named: "tab4Unselect"), for: .normal)
//        btn.setImage(UIImage(named: "tab4select"), for: .selected)
//        return btn
//    }()
//
//    let fifthBtn: UIButton = {
//        let btn = UIButton()
//        btn.setImage(UIImage(named: "tab5Unselect"), for: .normal)
//        btn.setImage(UIImage(named: "tab5select"), for: .selected)
//        return btn
//    }()
//
//    override func setViews() {
//        parentView.addSubview(self)
//
//        let ratio:CGFloat = UIScreen.main.bounds.width / 375
//
//        [firstBtn, secondBtn, thirdBtn, fourthBtn, fifthBtn].forEach {
//            self.addSubview($0)
//        }
//
//        self.snp.makeConstraints { make in
//            make.leading.trailing.equalToSuperview()
//            make.bottom.equalTo(parentView.safeAreaLayoutGuide.snp.bottom)
//            make.height.equalTo(68)
//        }
//
//
//        firstBtn.snp.makeConstraints { make in
//            make.leading.equalTo(25 * ratio)
//            make.bottom.equalTo(-3)
//            make.height.equalTo(46)
//            make.width.equalTo(28)
//        }
//
//        secondBtn.snp.makeConstraints { make in
//            make.leading.equalTo(firstBtn.snp.trailing).offset(55)
//            make.bottom.equalTo(-3)
//            make.size.equalTo(firstBtn)
//        }
//
//        thirdBtn.snp.makeConstraints { make in
//            make.centerX.equalTo(parentView)
//            make.width.equalTo(85)
//            make.height.equalTo(68)
//        }
//
//        fourthBtn.snp.makeConstraints { make in
//            make.trailing.equalTo(fifthBtn.snp.leading).offset(-46)
//            make.bottom.equalTo(-3)
//            make.size.equalTo(firstBtn)
//        }
//
//        fifthBtn.snp.makeConstraints { make in
//            make.trailing.equalTo(parentView.snp.trailing).offset(-25 * ratio)
//            make.bottom.equalTo(-3)
//            make.size.equalTo(firstBtn)
//        }
//    }
//}
//
//
//class FourthVC: UIViewController {
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        view.backgroundColor = .yellow
//    }
//}
//
//class FifthVC: UIViewController {
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        view.backgroundColor = .green
//    }
//}
//

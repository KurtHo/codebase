//
//  FriendModel.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2023/9/12.
//

import Foundation

struct FriendsOutput: Codable {
    let response: [FriendModel]
}

struct FriendModel: Codable, Equatable {
    let name: String
    let status: Int
    let isTop: String
    let fid: String
    let updateDate: String
    let avatar: String
    
    
    
    static func ==(lhs: FriendModel, rhs: FriendModel) -> Bool {
        return lhs.name == rhs.name && lhs.fid == rhs.fid
    }
    
    var isFavorite:Bool {
        return isTop == "1"
    }

}

struct FriendsDummyData {
    static let noFriendQQ = """
    {
      "response": [
        
      ]
    }
    """
    
    static let friend1 = """
    {
      "response": [
        {
          "name": "黃靖僑",
          "status": 0,
          "isTop": "0",
          "fid": "001",
          "updateDate": "2019/08/01",
          "avatar": "https://i.pravatar.cc/150?img=1"
        },
        {
          "name": "翁勳儀",
          "status": 2,
          "isTop": "1",
          "fid": "002",
          "updateDate": "2019/08/02",
          "avatar": "https://i.pravatar.cc/150?img=2"
        },
        {
          "name": "洪佳妤",
          "status": 1,
          "isTop": "0",
          "fid": "003",
          "updateDate": "2019/08/04",
          "avatar": "https://i.pravatar.cc/150?img=3"
        },
        {
          "name": "梁立璇",
          "status": 1,
          "isTop": "0",
          "fid": "004",
          "updateDate": "2019/08/01",
          "avatar": "https://i.pravatar.cc/150?img=4"
        },
        {
          "name": "梁立璇",
          "status": 1,
          "isTop": "0",
          "fid": "005",
          "updateDate": "2019/08/04",
          "avatar": "https://i.pravatar.cc/150?img=5"
        }
      ]
    }
    """
    
    static let friend2 =  """
    {
      "response": [
        {
          "name": "黃靖僑",
          "status": 1,
          "isTop": "0",
          "fid": "001",
          "updateDate": "2019/08/02",
          "avatar": "https://i.pravatar.cc/150?img=1"
        },
        {
          "name": "翁勳儀",
          "status": 1,
          "isTop": "1",
          "fid": "002",
          "updateDate": "2019/08/01",
          "avatar": "https://i.pravatar.cc/150?img=2"
        },
        {
          "name": "林宜真",
          "status": 1,
          "isTop": "0",
          "fid": "012",
          "updateDate": "2019/08/01",
          "avatar": "https://i.pravatar.cc/150?img=8"
        }
      ]
    }
    """

    static let invitation = """
    {
      "response": [
        {
          "name": "黃靖僑",
          "status": 0,
          "isTop": "0",
          "fid": "001",
          "updateDate": "2019/08/01",
          "avatar": "https://i.pravatar.cc/150?img=1"
        },
        {
          "name": "翁勳儀",
          "status": 0,
          "isTop": "1",
          "fid": "002",
          "updateDate": "2019/08/02",
          "avatar": "https://i.pravatar.cc/150?img=2"
        },
        {
          "name": "洪佳妤",
          "status": 1,
          "isTop": "0",
          "fid": "003",
          "updateDate": "2019/08/04",
          "avatar": "https://i.pravatar.cc/150?img=3"
        },
        {
          "name": "彭安亭",
          "status": 0,
          "isTop": "0",
          "fid": "007",
          "updateDate": "2019/08/02",
          "avatar": "https://i.pravatar.cc/150?img=20"
        },
        {
          "name": "施君凌",
          "status": 2,
          "isTop": "0",
          "fid": "008",
          "updateDate": "2019/08/03",
          "avatar": "https://i.pravatar.cc/150?img=21"
        }
      ]
    }
    """

}

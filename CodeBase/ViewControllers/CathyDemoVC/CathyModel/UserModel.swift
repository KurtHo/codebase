//
//  UserModel.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2023/9/12.
//

import Foundation
struct UserOutput: Codable {
    let response: [UserModel]
}

struct UserModel: Codable {
    let name: String
    let kokoid: String
    let avatar: String
}

struct UserDummyData {
    static let user = """
    {
      "response": [
         {
              "name": "蔡國泰",
              "kokoid": "Mike",
              "avatar": "https://i.pravatar.cc/150?img=11"
            }
      ]
    }
    """
}


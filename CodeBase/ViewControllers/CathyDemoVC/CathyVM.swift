//
//  CathyVM.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2023/9/12.
//
import Foundation

class CathyVM: CathyBaseViewModel {
    var acceptedInvitationModel: [FriendModel] = []
    var declineInvitationModel: [FriendModel] = []
    
    var invitatingModel: [FriendModel]?
//    var friends: Box<[FriendModel]?> = Box(value: [])
    var friends: Bind<[FriendModel]?> = Bind(value: nil)
    var invitation: Bind<[FriendModel]?> = Bind(value: nil)
    
    /// 取得好友列表
    func getFriendsData(cb: @escaping ([FriendModel]) -> Void) {
        let group = DispatchGroup()
        
        var api1Completed = false
        var api2Completed = false
        
        group.enter()
        getFriend(type: .friend1) {
            debug("friend1")
            group.leave()
            api1Completed = true
        } err: {
            debug("err handle")
            api1Completed = true
        }

        group.enter()
        getFriend(type: .friend2) {
            debug("friend2")
            group.leave()
            api2Completed = true
        } err: {
            debug("err handle")
            api1Completed = true
        }
        
        group.notify(queue: .main) {
            if api1Completed && api2Completed {
                cb(self.friends.value ?? [])
            }else {
                debug(1234)
            }
        }
    }
    
    /// 取得使用者資料
    func getUserData(cb:@escaping (UserModel) -> Void) {
        CathyApi.getUser { result in
            switch result {
            case .success(let users):
                if let user = users.first {
                    cb(user)
                }
                
            case .failure(let err):
                debug("err: \(err.localizedDescription)")
            }
        }
    }
    
    
    /// 只操作一次好友列表
    func getFriend(
        type: CathyApi.ModelType,
        cb: @escaping () -> Void,
        err: @escaping () -> Void)
    {
        CathyApi.get(modelType: type) { result in
            switch result {
            case .success(let _friends):
                
                DispatchQueue.main.async {
                    
                    if self.friends.value == nil {
//                        self.friends.value?.append(contentsOf: _friends)
                        self.friends.value = _friends
                    }else {
                        guard var currentFriends = self.friends.value else {return}
                        currentFriends.enumerated().forEach { selfIndex, myFriend in
                            _friends.forEach { _fri in
                                if myFriend == _fri {
                                    let dateFormatter = DateFormatter()
                                    dateFormatter.dateFormat = "yyyy/MM/dd"
                                    
                                    if
                                        let myDate = dateFormatter.date(from: myFriend.updateDate),
                                        let newInDate = dateFormatter.date(from: _fri.updateDate)
                                    {
                                        currentFriends[selfIndex] = myDate > newInDate ? myFriend : _fri
                                    }
                                    
                                }
                                
                                if currentFriends.contains(_fri) == false {
                                    currentFriends.append(_fri)
                                }
                            }
                        }
                        self.friends.value = currentFriends
                        
                    }
                    
                    debug(self.friends.value)
                    cb()
                }
                
            case .failure(let error):
                debug(error)
                err()
            }

        }
    }
    
    /// 取得好友邀請
    func getFriendInvitation(
        cb: @escaping () -> Void,
        err: @escaping () -> Void
    ) {
        CathyApi.get(modelType: .invitation) { result in
            switch result {
            case .success(let _invitaion):
                self.invitation.value = self.filterOutInvitation(_invitaion)
                cb()
            case .failure(let err):
                debug(err)
            }
        }
    }
    
    func filterOutInvitation(_ friend: [FriendModel]) -> [FriendModel] {
        let result = friend.filter{ $0.status == 0}
        
        return result
    }
}

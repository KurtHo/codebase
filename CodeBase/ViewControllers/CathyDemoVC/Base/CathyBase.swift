//
//  CathyBase.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2023/9/12.
//

import UIKit

class CathyBaseViewModel:NSObject {}

class CathyBaseViewsWithModel<T: CathyBaseViewModel>: CathyBaseViews {
    unowned var vm: T
    
    init(vm: T, parentView: UIView) {
        self.vm = vm
        super.init(parentView: parentView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class CathyBaseViews: UIView {
    var parentView: UIView
    
    init(parentView: UIView) {
        self.parentView = parentView
        super.init(frame: .zero)
        parentView.isUserInteractionEnabled = true
        parentView.addSubview(self)
        parentView.layoutIfNeeded()
        setViews()
        setDelegate()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setViews() {
        
    }
    
    func setDelegate() {
        
    }
}

class BaseImageViews: UIImageView {
    var parentView: UIView
    
    init(parentView: UIView) {
        self.parentView = parentView
        super.init(frame: .zero)
        parentView.addSubview(self)
        parentView.isUserInteractionEnabled = true
        setViews()
        setAnimation()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setViews() {
        
    }
    
    func setAnimation() {
        
    }
}

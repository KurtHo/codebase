//
//  FriendCell.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2023/9/12.
//

import UIKit
import SnapKit

class FriendsCell: UITableViewCell {
    
    var friend: FriendModel?
    var inviteWidth:ConstraintMakerEditable!
    var transferOffset:ConstraintMakerEditable!
    
    let favorite:UIImageView = {
        let iv = UIImageView(image: UIImage(named: "icFriendsStar"))
            
        return iv
    }()
    
    let avatar:ImageView = {
        let iv = ImageView()

        return iv
    }()
    
    let nameLb:UILabel = {
        let lb = UILabel()
        lb.setFont(fontName: .PingFangTCRegular, size: 16, color: .greyishBrown)
        return lb
    }()
    
    let transferBtn:UIButton = {
        let btn = UIButton()
        btn.setTitle("轉帳", for: .normal)
        btn.setFont(fontName: .PingFangTCMedium, size: 14, color: .hotPink)
        btn.layer.borderWidth = 1
        btn.layer.borderColor = UIColor.hotPink.cgColor
        return btn
    }()
    
    let dotsImgView:UIImageView = {
        let iv = UIImageView(image: UIImage(named: "icFriendsMore"))
        return iv
    }()
    
    let inviteBtn:UIButton = {
        let btn = UIButton()
        btn.setTitle("邀請中", for: .normal)
        btn.setFont(fontName: .PingFangTCMedium, size: 14, color: .brownGrey)
        btn.layer.borderWidth = 1
        btn.layer.borderColor = UIColor.brownGrey.cgColor
        return btn
    }()
    
    
    let seperator:UIView = {
        let v = UIView()
        v.backgroundColor = .setRGB(r: 228, g: 228, b: 228)
        return v
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        separatorInset = .zero
        backgroundColor = .white
        setViews()
    }

    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setContent(friend: FriendModel) {
        self.friend = friend
        avatar.setImage(imgString: friend.avatar, defaultImg: UIImage(named: "imgFriendsFemaleDefault"))
        nameLb.text = friend.name
        favorite.isHidden = friend.isFavorite
        
        switch friend.status {
        case 1:
            inviteWidth.constraint.update(offset: 60)
            dotsImgView.isHidden = true
            transferOffset.constraint.update(offset: -90)
            
        case 2: //顯示邀請中
            inviteWidth.constraint.update(offset: 0)
            dotsImgView.isHidden = false
            self.transferOffset.constraint.update(offset: -73)
            
            
        default:
            inviteWidth.constraint.update(offset: 60)
            dotsImgView.isHidden = true
            transferOffset.constraint.update(offset: -90)
            debug("default: \(friend.status)")
            
        }
        
    }
    
    private func setViews() {
        [favorite, avatar, nameLb, transferBtn, dotsImgView, inviteBtn, seperator].forEach {
            addSubview($0)
        }
        
        favorite.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(30)
            make.centerY.equalToSuperview()
            make.width.height.equalTo(14)
        }
        
        avatar.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(50)
            make.centerY.equalToSuperview()
            make.width.height.equalTo(40)
        }
        avatar.layer.cornerRadius = 20
        avatar.clipsToBounds = true
        
        nameLb.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.leading.equalTo(avatar.snp.trailing).offset(15)
            make.height.equalTo(22)
        }
        
        inviteBtn.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-20)
            make.centerY.equalToSuperview()
            make.height.equalTo(24)
            self.inviteWidth = make.width.equalTo(60)
        }
        
        transferBtn.snp.makeConstraints { make in
            self.transferOffset = make.trailing.equalToSuperview().offset(-90)
            make.centerY.equalTo(inviteBtn)
            make.height.equalTo(24)
            make.width.equalTo(60)
        }
        
        dotsImgView.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.width.equalTo(18)
            make.height.equalTo(4)
            make.trailing.equalToSuperview().offset(-30)
        }
        dotsImgView.isHidden = true
        
        seperator.snp.makeConstraints { make in
            make.leading.equalTo(105)
            make.trailing.equalToSuperview().inset(30)
            make.bottom.equalToSuperview()
            make.height.equalTo(1)
        }
    }
}

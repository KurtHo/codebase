//
//  CathyUserSeetIDView.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2023/9/12.
//

import UIKit

class CathyUserSetIDView: CathyBaseViews {
    var userModel:UserModel?
    
    let nameLb: UILabel = {
        let lb = UILabel()
        lb.text = "紫晽"
        lb.setFont(fontName: .PingFangTCMedium, size: 17, color: .greyishBrown)
        return lb
    }()
    
    let userIdLb:UILabel = {
        let lb = UILabel()
        lb.text = "設定 KOKO ID"
        lb.setFont(fontName: .PingFangTCRegular, size: 13, color: .greyishBrown)
        return lb
    }()
    
    let rightArrowImgView: UIImageView = {
        let imgView = UIImageView(image: UIImage(named: "icInfoBackDeepGray"))
        return imgView
    }()
    
    let dotView:UIView = {
        let view = UIView()
        view.backgroundColor = .hotPink
        return view
    }()
    
    let avatarBtn: UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "imgFriendsFemaleDefault"), for: .normal)
        return btn
    }()
    
    init(parentView: UIView, userModel:UserModel?) {
        super.init(parentView: parentView)
        self.userModel = userModel
        isUserInteractionEnabled = true
    }
    
    func setUserModel(_ model: UserModel) {
        self.userModel = model
        self.nameLb.text = model.name
        self.userIdLb.text = "KOKO ID：" + model.kokoid
        self.dotView.isHidden = true
        Api.downloadImage(urlStr: model.avatar) { [weak self] img, _ in
            self?.avatarBtn.setImage(img, for: .normal)
        }
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setViews() {
        [nameLb, userIdLb, rightArrowImgView, dotView, avatarBtn].forEach {
            addSubview($0)
        }
        
        nameLb.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(12)
            make.leading.equalToSuperview().offset(30)
            make.height.equalTo(18)
        }
        
        userIdLb.snp.makeConstraints { make in
            make.height.leading.equalTo(nameLb)
            make.top.equalTo(nameLb.snp.bottom).offset(8)
        }
        
        rightArrowImgView.snp.makeConstraints { make in
            make.leading.equalTo(userIdLb.snp.trailing)
            make.centerY.height.equalTo(userIdLb)
        }
        
        dotView.snp.makeConstraints { make in
            make.leading.equalTo(rightArrowImgView.snp.trailing).offset(15)
            make.height.width.equalTo(10)
            make.centerY.equalTo(rightArrowImgView)
        }
        dotView.layer.cornerRadius = 5
        dotView.clipsToBounds = true
        
        avatarBtn.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-30)
            make.height.equalTo(52)
            make.width.equalTo(52)
            make.top.equalToSuperview().offset(8)
        }
        
        avatarBtn.layoutIfNeeded()
        avatarBtn.layer.cornerRadius = avatarBtn.bounds.height / 2
        avatarBtn.clipsToBounds = true
        
    }
    
    func setUserId(_ text: String) {
        userIdLb.text = text
    }
}

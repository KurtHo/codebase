//
//  CustomSegmentController.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2023/9/12.
//

import UIKit
import SnapKit

protocol CustomSegmentControllerDelegate: NSObject {
    func didMove(_ selectedIndex: Int)
}

class CustomSegmentController: CathyBaseViews {
    
    let themeColor: UIColor
    let btnColor: UIColor
//    var segBtns: [UIButton] = []
    var labels = [UILabel]()
    var selectedIndex: Int = 0 {
        didSet {
            moveSelectorView()
        }
    }
    
    private let btnTitles: [String]
    private let leadingGap: CGFloat
    private let btnWidth: CGFloat
    private var btns: [UIButton] = []
    private let selectorWidth: CGFloat
    private var btnTappedFlag = false
    
    private var selectorView:UIView = {
        let v = UIView()
        return v
    }()
    
    
    
    weak var delegate: CustomSegmentControllerDelegate?
    
    init(
        themeColor: UIColor,
        index: Int,
        parentView: UIView,
        btnTitles: [String],
        btnColor: UIColor,
        leadingGap: CGFloat = 0,
        btnWidth: CGFloat = 80,
        selectorWidth: CGFloat = 40
    ) {
        self.themeColor = themeColor
        self.selectedIndex = index
        self.btnTitles = btnTitles
        self.btnColor = btnColor
        self.leadingGap = leadingGap
        self.btnWidth = btnWidth
        self.selectorWidth = selectorWidth
        super.init(parentView: parentView)
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setViews() {
        
        [selectorView].forEach{
            self.addSubview($0)
        }
        
        var lastBtn:UIButton!
        btnTitles.enumerated().forEach { index, btnTitle in
            let btn = UIButton()
            btn.setTitle(btnTitle, for: .normal)
            btn.setTitleColor(btnColor, for: .normal)
            btn.tag = index
            btn.addTarget(self, action: #selector(segmentTapped(_:)), for: .touchUpInside)

            self.addSubview(btn)
            self.btns.append(btn)
            btn.snp.makeConstraints { make in
                if index == 0 {
                    make.leading.equalToSuperview().inset(30)
                    lastBtn = btn
                }else {
                    make.leading.equalTo(lastBtn.snp.trailing).offset(30)
                }
                make.top.equalTo(self.safeAreaLayoutGuide.snp.top)
                make.bottom.equalTo(selectorView)
                make.width.equalTo(self.btnWidth)
                
            }
            
            let label = InsetsLabel()
            label.text = "99+"
            label.setFont(fontName: .PingFangTCMedium, size: 12, color: .white)
            label.insets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
            label.backgroundColor = .setRGB(r: 249, g: 178, b: 220)
            label.textAlignment = .center
            btn.addSubview(label)
            label.snp.makeConstraints { make in
                make.leading.equalTo(btn.titleLabel!.snp.trailing)
                make.bottom.equalTo(btn.snp.centerY)
                make.height.equalTo(18)
                make.width.greaterThanOrEqualTo(18)
            }
            label.layer.cornerRadius = 9
            label.clipsToBounds = true
            self.labels.append(label)
        }
        layoutSubviews()
        
        selectorView.backgroundColor = themeColor
        
        selectorView.snp.makeConstraints { make in
            make.width.equalTo(selectorWidth)
            make.centerX.equalTo(self.btns.first!)
            make.height.equalTo(2)
            make.bottom.equalToSuperview()
        }

        selectorView.layer.cornerRadius = 1
        selectorView.clipsToBounds = true
        
        selectorView.layoutIfNeeded()
        layoutSubviews()
        
    }
    
    @objc private func segmentTapped(_ sender: UIButton) {
        selectedIndex = sender.tag
    }
    
    func moveSelectorView() {
        let gap = (btns[selectedIndex].bounds.width - selectorWidth) / 2
        btnTappedFlag = true
        delegate?.didMove(self.selectedIndex)
        UIView.animate(withDuration: 0.3) {
            self.selectorView.frame.origin.x = self.btns[self.selectedIndex].frame.origin.x + gap
        }completion: { _ in
            self.btnTappedFlag = false
        }
    }
    
    
    func moveSelector(_ ratio: CGFloat) {
        guard !btnTappedFlag else {return}
        let gap = (btns[selectedIndex].bounds.width - selectorWidth) / 2
        let offset = self.btns[0].frame.origin.x + gap
        self.selectorView.frame.origin.x = ratio + offset
    }
}


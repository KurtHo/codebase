//
//  CathyCardView.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2023/9/12.
//

import UIKit

class CathyCardView: CathyBaseViews {
    
    var invitaionModel: FriendModel!
    
    let imgView: ImageView = {
        let iv = ImageView()
        iv.backgroundColor = .gray
        return iv
    }()
    
    let nameLb: UILabel = {
        let lb = UILabel()
        lb.setFont(fontName: .PingFangTCRegular, size: 16, color: .greyishBrown)
        lb.text = "彭安亭"
        return lb
    }()
    
    let describeLb: UILabel = {
        let lb = UILabel()
        lb.setFont(fontName: .PingFangTCRegular, size: 13, color: .brownGrey)
        lb.text = "邀請你成為好友：）"
        return lb
    }()
    
    let acceptBtn: UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "btnFriendsAgree"), for: .normal)
        return btn
    }()
    
    let declineBtn: UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "btnFriendsDelet"), for: .normal)
        return btn
    }()
    
    override init(parentView: UIView) {
        super.init(parentView: parentView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setViews() {
        
        self.backgroundColor = .white
        DispatchQueue.main.async {
            self.snp.makeConstraints { make in
                make.leading.trailing.equalToSuperview().inset(30)
                make.height.equalTo(70)
            }
        }
        setShadow(view: self)
        
        [imgView, nameLb, describeLb, acceptBtn, declineBtn].forEach {
            self.addSubview($0)
        }
        
        imgView.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(15)
            make.centerY.equalToSuperview()
            make.height.width.equalTo(40)
        }
        imgView.layer.cornerRadius = 20
        imgView.clipsToBounds = true
        
        nameLb.snp.makeConstraints { make in
            make.leading.equalTo(imgView.snp.trailing).offset(15)
            make.height.equalTo(22)
            make.top.equalTo(imgView)
        }
        
        describeLb.snp.makeConstraints { make in
            make.leading.equalTo(nameLb)
            make.height.equalTo(18)
            make.bottom.equalTo(imgView.snp.bottom)
        }
        
        declineBtn.snp.makeConstraints { make in
            make.centerY.equalTo(imgView)
            make.height.width.equalTo(30)
            make.trailing.equalTo(self).offset(-15)
        }
        
        acceptBtn.snp.makeConstraints { make in
            make.trailing.equalTo(declineBtn.snp.leading).offset(-15)
            make.centerY.height.width.equalTo(declineBtn)
        }
        
    }
    
    func setModel(_ invitaion: FriendModel) {
        self.invitaionModel = invitaion
        self.nameLb.text = invitaion.name
        self.imgView.setImage(imgString: invitaion.avatar, defaultImg: UIImage(named: "imgFriendsFemaleDefault"))
    }
    
    
    private func setShadow(view: UIView) {
        view.layoutIfNeeded()
        view.layer.cornerRadius = 6
         
         // Apply shadow
        view.layer.shadowColor = UIColor.gray.withAlphaComponent(0.1).cgColor
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize(width: 4, height: 4)
        view.layer.shadowRadius = 4
     }
   
}

//
//  EmptyFriendView.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2023/9/12.
//

import UIKit

class EmptyFriendView: CathyBaseViews {
    
    let imgView: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "imgFriendsEmpty"))
        return imageView
    }()
    
    let label: UILabel = {
        let lb = UILabel()
        lb.text = "就從加好友開始吧：）"
        lb.textAlignment = .center
        lb.setFont(fontName: .PingFangTCMedium, size: 21, color: .greyishBrown)
        return lb
    }()
    
    let describLb: UILabel = {
        let lb = UILabel()
        lb.numberOfLines = 2
        lb.textAlignment = .center
        lb.text = "與好友們一起用 KOKO 聊起來！\n還能互相收付款、發紅包喔：）"
        lb.setFont(fontName: .PingFangTCRegular, size: 14, color: .brownGrey)
        return lb
    }()
    
    
    let addFriendBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("加好友", for: .normal)
        return btn
    }()
    
    let setIdView: UIView = {
        let view = UIView()
 
        return view
    }()

    lazy var setUserId = createButton(with: "幫助好友更快找到你？設定 KOKO ID", front: "幫助好友更快找到你？", back: "設定 KOKO ID")
    
    
    override init(parentView: UIView) {
        super.init(parentView: parentView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setViews() {
        self.backgroundColor = .white
                
        [imgView, label, describLb, addFriendBtn, addFriendBtn, setUserId].forEach {
            self.addSubview($0)
        }

        imgView.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(30)
            make.height.equalTo(172)
            make.width.equalTo(245)
        }

        label.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(imgView.snp.bottom).offset(41)
            make.height.equalTo(29)
        }
        
        describLb.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalTo(label.snp.bottom).offset(8)
            make.height.equalTo(40)
        }
        
        addFriendBtn.snp.makeConstraints { make in
            make.top.equalTo(describLb.snp.bottom).offset(25)
            make.centerX.equalToSuperview()
            make.width.equalTo(192)
            make.height.equalTo(40)
        }
        
        setUserId.snp.makeConstraints { make in
            make.top.equalTo(addFriendBtn.snp.bottom).offset(40)
            make.centerX.equalToSuperview()
            make.height.equalTo(18)
        }
        
        addFriendBtn.layoutIfNeeded()
        applyRoundedButtonStyle(button: addFriendBtn)
    }
    
    
    private func createButton(with text: String, front: String, back:String) -> UIButton {
        let button = UIButton(type: .system)
        button.titleLabel?.numberOfLines = 0
        button.titleLabel?.textAlignment = .center
        
        let attributedText = NSMutableAttributedString(string: text)
        
        let firstRange = (text as NSString).range(of: front)
        attributedText.addAttribute(.foregroundColor, value: UIColor.brownGrey, range: firstRange)
        
        let secondRange = (text as NSString).range(of: back)
        attributedText.addAttribute(.foregroundColor, value: UIColor.hotPink, range: secondRange)
        attributedText.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: secondRange)
        
        button.setAttributedTitle(attributedText, for: .normal)
        return button
    }
    
    private func applyRoundedButtonStyle(button: UIButton) {
                  // Apply rounded corners
        button.layer.cornerRadius = button.frame.height / 2
         
         // Apply shadow
        button.layer.shadowColor = UIColor.setRGB(r: 121, g: 196, b: 27, alpha: 0.4).cgColor
        button.layer.shadowOpacity = 1
        button.layer.shadowOffset = CGSize(width: 0, height: 4)
        button.layer.shadowRadius = 4
        
        let imgView = UIImageView(image: UIImage(named: "icAddFriendWhite"))
        button.addSubview(imgView)
        imgView.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview().offset(-8)
        }
        
        // gradient
        let gradientLayer = CAGradientLayer()
        
        gradientLayer.frame = button.bounds
        gradientLayer.colors = [UIColor.setRGB(r: 86, g: 179, b: 11).cgColor,
                                UIColor.setRGB(r: 166, g: 204, b: 66).cgColor]
        
        gradientLayer.startPoint = CGPoint(x: 0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0.5)
        button.layer.insertSublayer(gradientLayer, at: 0)
        gradientLayer.cornerRadius = button.frame.height / 2
     }
    
}


//
//  SearchFriendBarView.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2023/9/12.
//

import UIKit

class SearchFriendBarView: CathyBaseViews {
    
    var searchTextField:UITextField = {
        let tf = UITextField()
        tf.textColor = .black
        tf.placeholder = "想轉一筆給誰呢？"
        tf.setPlaceholder(color: .gray)
        tf.leftViewMode = .always
        tf.layer.cornerRadius = 12
        tf.backgroundColor = .setRGB(r: 142, g: 142, b: 147, alpha: 0.12)
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 38, height: 20))
        let imageView = UIImageView(frame: CGRect(x: 10, y: 0, width: 14, height: 14))
        imageView.image = UIImage(named: "icSearchBarSearchGray")
        imageView.center.y = view.center.y
        view.addSubview(imageView)
        
        imageView.contentMode = .scaleAspectFit
        
    
//        imageView.tintColor = color
        tf.leftView = view
        
        return tf
    }()
    
    let searchBtn:UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "icBtnAddFriends"), for: .normal)
        
        return btn
    }()
    
    override init(parentView: UIView) {
        super.init(parentView: parentView)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func setViews() {
        
        [searchTextField, searchBtn].forEach {
            addSubview($0)
        }
        
        searchTextField.snp.makeConstraints { make in
            make.leading.equalTo(self).offset(30)
            make.trailing.equalTo(parentView).inset(69)
            make.height.equalTo(36)
            make.centerY.equalTo(self)
        }
        
        
        searchBtn.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-30)
            make.centerY.equalTo(searchTextField)
//            make.top.equalToSuperview().offset(15)
            make.height.width.equalTo(24)
        }
        searchBtn.addTarget(self, action: #selector(test), for: .touchUpInside)
    }
 
    @objc func test(){
        debug("1234")
    }
    
}


//
//  CathyFriendInvitationView.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2023/9/12.
//

import UIKit

protocol InvitationViewExpandDelegate: AnyObject {
    func didExpand(_ expand: Bool)
    func handleInvitation(accepted: Bool, invitationModel: FriendModel)
    func closeTheView()
}

class CathyFriendInvitationView: CathyBaseViews {
    
    var delegate: InvitationViewExpandDelegate?
    
    var isExpand: Bool = false
    
    var invitationModel: [FriendModel] = []
    
    lazy var cardView = CathyCardView(parentView: self)
    lazy var cardView2 = CathyCardView(parentView: self)
    
    let underCard = UIView()
    
    override init(parentView: UIView) {
        super.init(parentView: parentView)
        setBtn()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    override func setViews() {
        self.backgroundColor = .setRGB(r: 252, g: 252, b: 252)
        [cardView, underCard, cardView2].forEach {
            self.addSubview($0)
        }
        
        underCard.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.width.equalTo(cardView).multipliedBy(0.93)
            make.height.equalTo(70)
            make.top.equalTo(cardView.snp.bottom).offset(-60)
        }
        
        DispatchQueue.main.async {
            self.layoutSubviews()
            self.underCard.roundCorners(corners: [.bottomLeft, .bottomRight], radius: 5)
            self.bringSubviewToFront(self.cardView)
            self.bringSubviewToFront(self.cardView2)
        }
        
        underCard.backgroundColor = .black.withAlphaComponent(0.035)
//        nextCard.backgroundColor = .white
        
        let tapGest = UITapGestureRecognizer(target: self, action: #selector(topCardTapped))
        cardView.addGestureRecognizer(tapGest)
        
        
        cardView2.snp.makeConstraints { make in
            make.top.equalTo(cardView.snp.bottom).offset(10)
            make.centerX.width.height.equalTo(cardView)
        }
        cardView2.isHidden = true
        cardView2.alpha = 0
        
        
    }
    
    func setBtn() {
        cardView.acceptBtn.addTarget(self, action: #selector(confirmBtn(_:)), for: .touchUpInside)
        cardView.declineBtn.addTarget(self, action: #selector(declineBtn(_:)), for: .touchUpInside)
        cardView2.acceptBtn.addTarget(self, action: #selector(confirmBtn(_:)), for: .touchUpInside)
        cardView2.declineBtn.addTarget(self, action: #selector(declineBtn(_:)), for: .touchUpInside)
    }

    func setInvitationModel(_ invitationModel: [FriendModel] ) {
        self.invitationModel = invitationModel
        
        if let first = invitationModel.first {
            self.cardView.setModel(first)
        }
        
        if invitationModel.count >= 2 {
            self.cardView2.setModel(invitationModel[1])
        }else {
            
        }
        
    }
    
    @objc func topCardTapped() {
        guard invitationModel.count > 1 else {
            delegate?.didExpand(false)
            return
        }
        cardView2.isHidden = false
        underCard.isHidden = false
        self.isExpand.toggle()
        delegate?.didExpand(isExpand)
        UIView.animate(withDuration: 0.3) {
            self.cardView2.alpha = self.isExpand ? 1 : 0
            self.underCard.alpha = self.isExpand ? 0 : 1
        } completion: { _ in
            self.underCard.isHidden = self.isExpand
            self.cardView2.isHidden = !self.isExpand
            
            self.cardView2.subviews.forEach {
                $0.isUserInteractionEnabled = true
            }
            
        }
    }
    
    @objc func confirmBtn(_ sender: UIButton) {
        if cardView.acceptBtn == sender {
            handleInvitation(isAccept: true, cardView.invitaionModel)
            guard !self.invitationModel.isEmpty else {
                self.delegate?.closeTheView()
                
                return
            }
            
            if invitationModel.count < 2 {
                UIView.animate(withDuration: 0.3) {
                    self.cardView2.alpha = 0
                    self.underCard.alpha = 0
                } completion: { _ in
                    self.cardView2.isHidden = true
                    if self.isExpand {
                        self.topCardTapped()
                    }
                }
            }
            
            
        }else {
            handleInvitation(isAccept: true, cardView2.invitaionModel)
            
            if invitationModel.count < 2 {
                UIView.animate(withDuration: 0.3) {
                    sender.superview?.alpha = 0
                } completion: { _ in
                    sender.superview?.isHidden = true
                    self.topCardTapped()
                }
            }
        }
    
    }
    
    @objc func declineBtn(_ sender: UIButton) {
        if cardView.declineBtn == sender {
            handleInvitation(isAccept: false, cardView.invitaionModel)
            guard !self.invitationModel.isEmpty else {
                self.delegate?.closeTheView()
                
                return
            }
            
            if invitationModel.count < 2 {
                UIView.animate(withDuration: 0.3) {
                    self.cardView2.alpha = 06
                    self.underCard.alpha = 0
                } completion: { _ in
                    self.cardView2.isHidden = true
                    if self.isExpand {
                        self.topCardTapped()
                    }
                }
            }
            
            
        }else {
            handleInvitation(isAccept: false, cardView2.invitaionModel)
            
            if invitationModel.count < 2 {
                UIView.animate(withDuration: 0.3) {
                    sender.superview?.alpha = 0
                } completion: { _ in
                    sender.superview?.isHidden = true
                    self.topCardTapped()
                }
            }
        }
    }
 
    
    func handleInvitation(isAccept: Bool ,_ model: FriendModel) {
        invitationModel.removeAll{$0.fid == model.fid}
        delegate?.handleInvitation(accepted: isAccept, invitationModel: model)
        setInvitationModel(invitationModel)
    }
}







//
//  CathyViews.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2023/9/12.
//


import UIKit
import SnapKit

//class SecondViews: BaseViews {
class CathyViews: CathyBaseViewsWithModel<CathyVM> {
    
    var searchFilterText = ""
    
    var hideSearchView:Bool = false
    
    var segmentControlHeight:ConstraintMakerEditable!
    var searchBarHeight:ConstraintMakerEditable!
    
    
    var refreshControl = UIRefreshControl()
    
    let bgColor = UIColor.setRGB(r: 252, g: 252, b: 252)
    
    
    /// navigation bar
    let customNavigationBar: UIView = {
        let view = UIView()
        return view
    }()
    
//    private let navigationBar: UINavigationBar?
    
    let firstBtn: UIButton = {
        let img = UIImage(named: "secNaviItem1")
        let btn = UIButton()
        btn.setImage(img, for: .normal)
        return btn
    }()
    
    let secondBtn: UIButton = {
        let img = UIImage(named: "secNaviItem2")
        let btn = UIButton()
        btn.setImage(img, for: .normal)
        return btn
    }()
    
    let thirdBtn: UIButton = {
        let img = UIImage(named: "secNaviItem3")
        let btn = UIButton()
        btn.setImage(img, for: .normal)
        return btn
    }()
    
    /// user id view
    lazy var userIdView = CathyUserSetIDView(parentView: self, userModel: nil)
    
    lazy var friendInvitationView = CathyFriendInvitationView(parentView: self)
    
    lazy var segmentControl = CustomSegmentController(themeColor: .hotPink, index: 0, parentView: self, btnTitles: ["好友", "聊天"], btnColor: .black)
    
    
    let seperatorLine: UIView = {
        let view = UIView()
        view.backgroundColor = .setRGB(r: 239, g: 239, b: 239)
        return view
    }()
    
    lazy var searchFriendBar = SearchFriendBarView(parentView: self)
    
    var scrollView: PageScrollView = {
        var sv = PageScrollView()
        sv.showsVerticalScrollIndicator = false
        sv.showsHorizontalScrollIndicator = false
        sv.isDirectionalLockEnabled = true
        sv.backgroundColor = .orange
        sv.isPagingEnabled = true
        return sv
    }()
    
    
    var emptyFriendView: EmptyFriendView?
    
    let button2: UIButton = {
        var btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("button2", for: .normal)
        btn.backgroundColor = .brown
        return btn
    }()
    
    let tableView:UITableView = {
        let tv = UITableView()
        tv.register(FriendsCell.self, forCellReuseIdentifier: FriendsCell.identifier)
        tv.separatorStyle = .none
        tv.backgroundColor = .white
        return tv
    }()
    
    
    override init(vm: CathyVM, parentView: UIView) {
//        self.navigationBar = nil
        super.init(vm: vm, parentView: parentView)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setViews() {
        self.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        [userIdView, friendInvitationView, segmentControl, seperatorLine, searchFriendBar, scrollView].forEach {
            addSubview($0)
        }
        
        setNavigationBar()
        
        userIdView.backgroundColor = bgColor
        parentView.layoutSubviews()
        userIdView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(customNavigationBar.snp.bottom)
            make.height.equalTo(80)
        }
        
        friendInvitationView.snp.makeConstraints { make in
            make.horizontalEdges.equalToSuperview()
            make.top.equalTo(userIdView.snp.bottom)
            make.height.equalTo(0)
        }
        
        friendInvitationView.isHidden = true
        
        
        segmentControl.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(friendInvitationView.snp.bottom)
            //            self.segmentControlHeight = make.height.equalTo(40)
            self.segmentControlHeight = make.height.equalTo(0)
        }
        segmentControl.alpha = 0
        segmentControl.isHidden = true
        
        
        seperatorLine.snp.makeConstraints { make in
            make.top.equalTo(segmentControl.snp.bottom)
            make.height.equalTo(1)
            make.leading.trailing.equalToSuperview()
        }
        
        searchFriendBar.snp.makeConstraints { make in
            make.top.equalTo(seperatorLine.snp.bottom)
            make.leading.trailing.equalToSuperview()
//            self.searchBarHeight = make.height.equalTo(66)
            self.searchBarHeight = make.height.equalTo(0)
        }
        searchFriendBar.alpha = 0
        searchFriendBar.isHidden = true
        
        scrollView.snp.makeConstraints { make in
            make.top.equalTo(searchFriendBar.snp.bottom)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalTo(safeAreaLayoutGuide.snp.bottom)
        }
        
        parentView.layoutSubviews()
        
        scrollView.setContentViews([tableView, button2], superview: self)
        setTableView()
        
        let tapGest = UITapGestureRecognizer(target: self, action: #selector(tapAnyWayToCloseSearchView))
        addGestureRecognizer(tapGest)
    }
    
    override func setDelegate() {
        
        segmentControl.delegate = self
        friendInvitationView.delegate = self
        searchFriendBar.searchTextField.delegate = self
        scrollView.delegate = self
    }
    
    private func setNavigationBar() {
        
        [firstBtn, secondBtn, thirdBtn].forEach {
            customNavigationBar.addSubview($0)
        }
        
        customNavigationBar.backgroundColor = .white
//        customNavigationBar.backgroundColor = .red
//        navigationBar?.isHidden = true
        addSubview(customNavigationBar)
        
        customNavigationBar.snp.makeConstraints { make in
//            make.top.equalToSuperview().offset(64)
            make.top.equalTo(self.safeAreaLayoutGuide.snp.top)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(44)
        }
        
        firstBtn.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(20)
            make.width.height.equalTo(24)
            make.centerY.equalToSuperview()
        }
        
        secondBtn.snp.makeConstraints { make in
            make.leading.equalTo(firstBtn.snp.trailing).offset(24)
            make.width.height.centerY.equalTo(firstBtn)
        }
        
        thirdBtn.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-20)
            make.width.height.centerY.equalTo(firstBtn)
        }
    }
    
}


// MARK: UIScrollViewDelegate
extension CathyViews: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let offset = scrollView.contentOffset.x
        let ratio:CGFloat = offset / 3.54
        debug("ratio: \(ratio)")
        segmentControl.moveSelector(ratio)
    }
}

// MARK: CustomSegmentControllerDelegate
extension CathyViews: CustomSegmentControllerDelegate {
    func didMove(_ selectedIndex: Int) {
        scrollView.setScrollToPage(selectedIndex)
    }
}




// MARK: PageSegmentDelegate
extension CathyViews: PageSegmentDelegate {
    func buttonTap(toIndex: Int) {
        scrollView.setScrollToPage(toIndex)
    }
}

extension CathyViews {
    func setEmptyView() {
        emptyFriendView = EmptyFriendView(parentView: scrollView)
        scrollView.setContentViews([emptyFriendView!, button2], superview: self)
        
    }
    
    func setFriendsView() {
        scrollView.setContentViews([tableView, button2], superview: self)
    }
    
    func setUserSetIDView() {
        
    }
    
    func setInvitation(friends: [FriendModel]) {
        vm.invitatingModel = friends
        DispatchQueue.main.async { [weak self] in
            guard let self = self else {return}
            
            friendInvitationView.setInvitationModel(friends)
            friendInvitationView.isHidden = false
            self.friendInvitationView.alpha = 0
            UIView.animate(withDuration: 0.3) {
                self.friendInvitationView.snp.updateConstraints { make in
                    make.height.equalTo(70 + 25)
                }
                self.friendInvitationView.alpha = 1
                self.layoutIfNeeded()
                
            } completion: { _ in
                self.friendInvitationView.layoutIfNeeded()
            }
        }
    }
    
    func setSegmentControlData(friends: [FriendModel]) {
        let _friends = friends.filter { $0.status == 2}
        guard segmentControl.labels.count != 0 else {return}
        segmentControl.labels[0].text = "\(_friends.count)"
    }
    
    func setSegmentControl(hide: Bool) {
        guard emptyFriendView == nil else {return}
            
        segmentControl.isHidden = false
        self.segmentControl.frame.size.height = 40
        UIView.animate(withDuration: 0.3) {
            self.segmentControlHeight.constraint.update(offset: hide ? 0 : 40)
            
            self.segmentControl.alpha = hide ? 0 : 1
            self.layoutIfNeeded()

        } completion: { _ in
            self.segmentControl.isHidden = hide
        }
    }
    
    func setSearchBar(hide: Bool) {
        
        searchFriendBar.isHidden = false
        
        UIView.animate(withDuration: 0.3) {
            self.searchBarHeight.constraint.update(offset: hide ? 0 : 66)
            self.searchFriendBar.alpha = hide ? 0 : 1
            self.layoutIfNeeded()
            
        } completion: { _ in
            self.searchFriendBar.isHidden = hide
            
        }

    }
    
    func hideSegmentInvitationUser(_ hide: Bool) {
        setSegmentControl(hide: hide)
        let views = [userIdView, segmentControl, friendInvitationView]
        views.forEach {
            $0.isHidden = false
            $0.alpha = 1
        }
        
        if vm.invitatingModel == nil {
            friendInvitationView.isHidden = true
        }
        
        UIView.animate(withDuration: 0.15) {
            views.forEach {
                $0.alpha = hide ? 0 : 1
            }
        } completion: { _ in
            UIView.animate(withDuration: 0.15) {
                self.userIdView.snp.updateConstraints { make in
                    make.height.equalTo(hide ? 0 : 80)
                }
                views.forEach {
                    $0.alpha = hide ? 0 : 1
                }
                
                self.friendInvitationView.snp.updateConstraints { make in
                    if self.vm.invitatingModel == nil {
                        make.height.equalTo(0)
                    }else {
                        make.height.equalTo(hide ? 0 : 70 + 25)
                    }
                }
                
                self.scrollView.snp.updateConstraints { make in
                    make.top.equalTo(self.searchFriendBar.snp.bottom)
                }
                                
                self.layoutIfNeeded()
                
            } completion: { _ in
                
                views.forEach {
                    $0.isHidden = hide
                }
                
                if self.vm.invitatingModel == nil {
                    self.friendInvitationView.isHidden = true
                }else {
                    self.friendInvitationView.isHidden = hide
                }
                
            }
        }

    }

}

// MARK: Alert
extension CathyViews {
    func showAlert(
        action1: @escaping () -> Void,
        action2: @escaping () -> Void,
        action3: @escaping () -> Void
    ) -> UIAlertController {
        let alertController = UIAlertController(title: "", message: "摸擬好友列表", preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "無好友畫面", style: .default) { _ in
            action1()
        })
        
        alertController.addAction(UIAlertAction(title: "只有好友列表", style: .default) { _ in
            action2()
        })
        
        alertController.addAction(UIAlertAction(title: "好友列表含邀請", style: .default) { _ in
            action3()
        })
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        return alertController
    }
}

//


// MARK: UITableViewDelegate
extension CathyViews: UITableViewDelegate {
    private func setTableView() {
        tableView.delegate = self
        tableView.contentInset.bottom = 100
        tableView.refreshControl = refreshControl
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        60
    }
}

//MARK: InvitationViewExpandDelegate
extension CathyViews: InvitationViewExpandDelegate {
    func closeTheView() {
        UIView.animate(withDuration: 0.3) {
            self.friendInvitationView.alpha = 0
            self.friendInvitationView.snp.updateConstraints { make in
                make.height.equalTo(0)
            }
            self.layoutIfNeeded()
        } completion: { _ in
            self.friendInvitationView.isHidden = true
            self.vm.invitatingModel = nil
        }

    }
    
    func handleInvitation(accepted: Bool, invitationModel: FriendModel) {
        if accepted {
            self.vm.acceptedInvitationModel.append(invitationModel)
            debug("accpedted collection: \(self.vm.acceptedInvitationModel)")
        }else {
            self.vm.declineInvitationModel.append(invitationModel)
            debug("declined collection: \(self.vm.declineInvitationModel)")
        }
    }
    
    func didExpand(_ expand: Bool) {
        self.friendInvitationView.snp.updateConstraints { make in
            make.height.equalTo(expand ? 70 * 2 + 25 : 70 + 25)
        }
        
    }
}

//MARK: UITextFieldDelegate
extension CathyViews: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        debug(textField.text ?? "")
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        debug("")
        hideSegmentInvitationUser(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let updatedText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        self.searchFilterText = updatedText ?? ""
        debug(updatedText ?? "")
        tableView.reloadData()
        self.searchFilterText = searchFilterText.replacingOccurrences(of: "/n", with: "")
        
        return true
    }
    
    @objc func tapAnyWayToCloseSearchView() {
        hideSegmentInvitationUser(false)
        self.endEditing(true)
    }
    
    
}

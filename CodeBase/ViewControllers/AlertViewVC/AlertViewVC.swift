//
//  AlertViewVC.swift
//  CodeBase
//
//  Created by KurtHo on 2021/4/20.
//

import UIKit

class AlertViewVC: UIViewController {
    
    var textView:UITextView = UITextView()
    private var alertView:UIView = UIView()
    private var titleLabel:UILabel = UILabel()
    private var cancelBtn:UIButton = UIButton(type: .custom)
    private var confirmBtn:UIButton = UIButton(type: .custom)
    private let horizonLine = UIView()
    private let verticleLine = UIView()
    private let black = UIColor.black.withAlphaComponent(0.3)
    
    private var titleText:String!
    private var placeHolder:String!
    private var confimAct: (() -> Void)?
    
    init() {
        super.init(nibName: nil, bundle: nil)
        self.modalPresentationStyle = .overCurrentContext
        self.modalTransitionStyle = .crossDissolve
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .clear
        setViews()
        showAnimate()
    }
    
    
    func setModel(titleText:String, placeHolder:String, confirmAct: (() ->Void)? ){
        self.titleText = titleText
        self.placeHolder = placeHolder
    }
    
    private func setViews(){
        view.addSubview(alertView)
        let white = UIColor.white.withAlphaComponent(0.9)
        alertView.backgroundColor = white
        let height = UIScreen.main.bounds.height / 3
        alertView.setAnchor(top: view.topAnchor, leading: view.leadingAnchor, trailing: view.trailingAnchor, bottom: nil, height: height, width: nil, padding: UIEdgeInsets(top: 160, left: 50, bottom: 0, right: -50))
        alertView.layer.cornerRadius = 15
        alertView.clipsToBounds = true
        
        
        horizonLine.backgroundColor = .lightGray
        verticleLine.backgroundColor = .lightGray
        view.addSubview(horizonLine)
        view.addSubview(verticleLine)
        
        let width:CGFloat = 1
        let btnHeight:CGFloat = 60
        let fontSize:CGFloat = 16
        
        horizonLine.setAnchor(top: nil, leading: alertView.leadingAnchor, trailing: alertView.trailingAnchor, bottom: alertView.bottomAnchor, height: width, width: nil, padding: UIEdgeInsets(top: 0, left: 0, bottom: -btnHeight, right: 0))
        
        verticleLine.setAnchor(top: horizonLine.topAnchor, leading: nil, trailing: nil, bottom: alertView.bottomAnchor, height: btnHeight, width: width)
        verticleLine.setCenterAnchor(centerX: alertView.centerXAnchor, centerY: nil)
        
        
        alertView.addSubview(titleLabel)
        titleLabel.setAnchor(top: alertView.topAnchor, leading: alertView.leadingAnchor, trailing: alertView.trailingAnchor, bottom: nil, height: 60, width: nil)
        titleLabel.text = "Type Something"
        titleLabel.textAlignment = .center
        titleLabel.setFont(fontName: .PingFangTCRegular, size: fontSize, color: .black)
        
        
        alertView.addSubview(textView)
        textView.setAnchor(top: titleLabel.bottomAnchor, leading: alertView.leadingAnchor, trailing: alertView.trailingAnchor, bottom: horizonLine.topAnchor, height: nil, width: nil, padding: UIEdgeInsets(top: 2, left: 20, bottom: -15, right: -20))
        textView.layer.borderWidth = 1
        textView.layer.borderColor = UIColor.lightGray.cgColor
        textView.setFont(fontName: .PingFangTCRegular, size: 16, color: .black)

        
        alertView.addSubview(confirmBtn)
        alertView.addSubview(cancelBtn)
        confirmBtn.setFont(fontName: .PingFangTCSemibold, size: fontSize, color: .systemBlue)
        cancelBtn.setFont(fontName: .PingFangTCSemibold, size: fontSize, color: .systemBlue)
        confirmBtn.setTitle("確定", for: .normal)
        cancelBtn.setTitle("取消", for: .normal)
        confirmBtn.setAnchor(top: horizonLine.bottomAnchor, leading: verticleLine.leadingAnchor, trailing: alertView.trailingAnchor, bottom: alertView.bottomAnchor, height: height, width: nil)
        cancelBtn.setAnchor(top: horizonLine.bottomAnchor, leading: alertView.leadingAnchor, trailing: verticleLine.leadingAnchor, bottom: alertView.bottomAnchor, height: height, width: nil)
        
        cancelBtn.addTarget(self, action: #selector(self.dismissAnimate), for: .touchUpInside)
        
    }
    
    
    private func showAnimate(){
        UIViewPropertyAnimator.runningPropertyAnimator(withDuration: 0.3, delay: .zero, options: .curveLinear) {
            self.view.backgroundColor = self.black
        } completion: { (_) in
            self.textView.becomeFirstResponder()
        }
    }

    
    @objc func dismissAnimate(){
        self.view.endEditing(true)
        UIViewPropertyAnimator.runningPropertyAnimator(withDuration: 0.3, delay: .zero, options: .curveLinear) {
            self.view.backgroundColor = .clear
            [self.alertView, self.titleLabel, self.verticleLine, self.horizonLine, self.confirmBtn, self.cancelBtn, self.textView].forEach {
                $0.alpha = 0
            }
            
        } completion: { (done) in
            self.dismiss(animated: false, completion: nil)

        }

    }
}

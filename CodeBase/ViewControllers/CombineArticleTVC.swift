//
//  CombineArticleTVC.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/1/7.
//

import UIKit
import Combine

@available(iOS 13.0, *)
class CombineArticleTVC: ArticleTVC {
   
    let gesture = UITapGestureRecognizer()
    let action = PassthroughSubject<ArticleModel, Never>()
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.isUserInteractionEnabled = true
        contentView.addGestureRecognizer(gesture)
        gesture.addTarget(self, action: #selector(tapAct))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func tapAct() {
//        debug(article ?? "")
        action.send(self.article)
    }
    
}

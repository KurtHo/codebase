//
//  BoxBindingViews.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/5/29.
//

import UIKit


class BoxBindingViews {
    var superView: UIView
    
    let collectionView:UICollectionView = {
        let flowlayout = UICollectionViewFlowLayout()
        flowlayout.minimumInteritemSpacing = 1
        flowlayout.minimumLineSpacing = 1
        flowlayout.scrollDirection = .vertical
        
        let width = UIScreen.main.bounds.width
        flowlayout.itemSize = CGSize(width: (width/3)-1, height: (width/3)-1)
        flowlayout.sectionInset = UIEdgeInsets(top: 1, left: 0, bottom: 11, right: 0)

        let cv = UICollectionView(frame: .zero, collectionViewLayout: flowlayout)
        cv.backgroundColor = .darkGray
        
        
        cv.register(BoxCollectionCell.self, forCellWithReuseIdentifier: BoxCollectionCell.identifier)
        
        return cv
    }()
    
    
    init(superView:UIView) {
        self.superView = superView
        
    }
    
    func setViews() {
        superView.addSubview(collectionView)
        collectionView.snp.makeConstraints { make in
            make.edges.equalTo(superView.safeAreaLayoutGuide)
        }
        
    }
}

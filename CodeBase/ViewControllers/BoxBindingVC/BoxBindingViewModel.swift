//
//  BoxBindingVM.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/5/29.
//

import Foundation

class BoxBindingViewModel {
    
    var news:Bind<NewsList?> = Bind(value: nil)
    
    func getNewsList(finish: (() -> Void)? = nil ) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            api.get(.news(source: .us), decode: NewsList.self) { [weak self] model, result in
                guard let self = self, let model = model else {return}
                self.news.value = model
                finish?()
            }
        }
    }
}

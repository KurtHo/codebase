//
//  BoxCollectionCell.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/5/31.
//

import UIKit

class BoxCollectionCell: UICollectionViewCell {
    
    var imgView: UIImageView = {
        let iv = UIImageView()
        iv.backgroundColor = .red
        return iv
    }()
    let titleLabel: UILabel = {
        let lb = UILabel()
        lb.backgroundColor = .white
        return lb
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUI()
    }
    
    
    func setUI(){
        addSubview(imgView)
        imgView.addSubview(titleLabel)
        
        imgView.snp.makeConstraints { make in
            make.top.leading.trailing.equalToSuperview()
            make.height.equalTo(imgView.snp.width)
        }
        
        titleLabel.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.height.equalTo(20)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

//
//  BoxBindingVC.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/5/29.
//

import UIKit

class BoxBindingVC: UIViewController {
    lazy var views = BoxBindingViews(superView: self.view)
    let vm = BoxBindingViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        views.setViews()
        bind()
        
    }
 
    
    func bind() {
        views.collectionView.dataSource = self
        
        vm.getNewsList()
        
        vm.news.bindUp{ _ in
            self.views.collectionView.reloadData()
        }
    }
    
}


extension BoxBindingVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return vm.news.value?.articles.count ?? 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BoxCollectionCell.identifier, for: indexPath) as! BoxCollectionCell
             
        return cell
    }
}

struct BoxCollectionViewCompo {
    var count:Int {
        return isEmpty ? 1 : (newsList?.articles.count ?? 1)
    }
    var itemSize:CGSize {
        return isEmpty ?
        CGSize(width: kScreenW, height: kScreenH) :
        CGSize(width: (kScreenW/3)-1, height: (kScreenW/3)-1)
    }
    
    var isEmpty:Bool {
        return self.newsList == nil
    }
    
    var newsList: NewsList?
    
    
    init(newsList: NewsList?){
        self.newsList = newsList
    }
}

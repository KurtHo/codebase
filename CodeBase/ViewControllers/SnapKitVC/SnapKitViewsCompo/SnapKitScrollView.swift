//
//  SnapKitViewsCompo.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/2/9.
//

import UIKit

class SnapKitScrollView: UIScrollView {
    
    init(showIndicator:Bool, words:String) {
        super.init(frame: .zero)
        self.showsVerticalScrollIndicator = showIndicator
        label.text = NSLocalizedString(words, comment: "")
        
        setAnchor()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setAnchor() {
        addSubview(contentView)
        contentView.addSubview(imgBtn)
        contentView.addSubview(label)
        
        contentView.snp.makeConstraints { make in
            make.width.edges.equalTo(self)
        }
        
        imgBtn.snp.makeConstraints { make in
            make.top.leading.trailing.equalTo(contentView)
            make.height.equalTo(self.snp.width).multipliedBy(0.75)
        }
        let edge: CGFloat = 5
        imgBtn.contentEdgeInsets = UIEdgeInsets(top: edge, left: edge, bottom: edge, right: edge)
        
        label.snp.makeConstraints { make in
            make.leading.trailing.equalTo(imgBtn).inset(edge)
            make.top.equalTo(imgBtn.snp.bottom).inset(-edge)
            make.bottom.equalTo(contentView.snp.bottom).inset(edge)
        }
        
    }
    
    
    private lazy var contentView: UIView = {
        let v = UIView()
        return v
    }()
    
    private lazy var imgBtn: UIButton = {
        let btn = UIButton()
        btn.setImage(.aBei, for: .normal)
        return btn
    }()
    
    private let label: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.lineBreakMode = .byClipping
        return label
    }()
}

//
//  SnapKitViews.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/2/9.
//

import UIKit

class SnapKitViews: BaseView<SnapKitViewModel> {
    private let topViewHeight: CGFloat = 40
    private let edgeValue:CGFloat = 50
    private var previousOffset: CGFloat = 0
    
    
    override func addViews() {
        [shrinkContent, scrollView].forEach {
            self.addSubview($0)
        }
        shrinkContent.addSubview(shrinkBtn)
    }
    
    override func setConstraints() {
        shrinkContent.snp.makeConstraints { make in
            make.top.equalTo(self.safeAreaLayoutGuide.snp.top)
            make.height.equalTo(topViewHeight)
            make.left.right.equalTo(self)
        }
   
        shrinkBtn.snp.makeConstraints { make in
            make.bottom.left.right.equalTo(shrinkContent)
            make.height.equalTo(topViewHeight)
        }
        
        scrollView.snp.makeConstraints { make in
            make.leading.trailing.equalTo(self)
            make.top.equalTo(shrinkContent.snp.bottom)
            make.bottom.equalTo(self.safeAreaLayoutGuide.snp.bottom)
        }
        
    }

    
    private let shrinkContent: UIView = {
        let v = UIView()
        v.backgroundColor = .red
        return v
    }()
    
    let shrinkBtn: UIButton = {
        let btn = UIButton()
        btn.setTitle("btn", for: .normal)
        btn.backgroundColor = .blue
        return btn
    }()
    
    lazy var scrollView: SnapKitScrollView = {
        let sv = SnapKitScrollView(showIndicator: false, words: words)
//        sv.bounces = false
        sv.delegate = self
        return sv
    }()
    
    let words = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
}


extension SnapKitViews: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetDiff = previousOffset - scrollView.contentOffset.y
        
        let updateAfterTop:Bool = scrollView.contentOffset.y > (edgeValue + shrinkContent.frame.height)
        
        let botEdge:CGFloat = scrollView.contentSize.height - scrollView.frame.height - edgeValue
        let updateBeforeBot:Bool = scrollView.contentOffset.y < botEdge
        
        if updateAfterTop && updateBeforeBot {
            
            shrinkContent.snp.updateConstraints { make in
                var offset = shrinkContent.frame.height + offsetDiff < 0 ? 0 : shrinkContent.frame.height + offsetDiff
                offset = offset > topViewHeight ? topViewHeight : offset
                make.height.equalTo(offset)
            }
            
        }
        
        previousOffset = scrollView.contentOffset.y
        
        debug("content offset: \(scrollView.contentOffset.y), offsetDiff: \(offsetDiff), updateEdge: \(updateBeforeBot)")
    }
}

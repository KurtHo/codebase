//
//  SnapKitVC.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/2/9.
//

import UIKit

class SnapKitVC: BaseVC {
 
    var views:SnapKitViews!
    var vm = SnapKitViewModel()
    
    
    override func loadView() {
        super.loadView()
        self.views = SnapKitViews.init(vm: vm, owner: self)
        title = "snap kit vc"
        binding()
    }
    
 
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func binding() {
        views.shrinkBtn.rx.tap.subscribe { event in
            print(1234)
        }.disposed(by: disposeBag)
    }
    
    deinit {
        print("snapKitVC")
    }
}

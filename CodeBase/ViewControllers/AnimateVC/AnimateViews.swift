//
//  AnimateViews.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/2/15.
//

import UIKit

class AnimateViews: BaseView<BaseViewModel> {
    
    private let height:CGFloat = 440
    private var currentState: State = .closed
    private var bottomConstraint = NSLayoutConstraint()

    private var popupView: UIView = {
        let view = UIView()
        view.backgroundColor = .gray
        return view
    }()
    
    private lazy var tap: UITapGestureRecognizer = {
        let t = UITapGestureRecognizer()
        t.addTarget(self, action: #selector(popupViewTapped(recognizer:)))
        return t
    }()
    
    
    override func addViews() {
        [popupView].forEach {
            addSubview($0)
        }
    }
    
    override func setConstraints() {
        popupView.snp.makeConstraints { make in
            make.leading.trailing.equalTo(self)
            make.height.equalTo(height + 40)
        }
        bottomConstraint = popupView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: height)
        bottomConstraint.isActive = true
        
    }
    
    override func setAnimation() {
        popupView.addGestureRecognizer(tap)
    }
 
}

extension AnimateViews {
     
    @objc private func popupViewTapped(recognizer: UITapGestureRecognizer) {
        let state = currentState.opposite
        let transitionAnimator = UIViewPropertyAnimator(duration: 0.3, dampingRatio: 1, animations: {
            switch state {
            case .open:
                self.bottomConstraint.constant = 0
            case .closed:
                self.bottomConstraint.constant = self.height
            }
            
            self.layoutIfNeeded()
        })
        transitionAnimator.addCompletion { position in
            switch position {
            case .start:
                self.currentState = state.opposite
            case .end:
                self.currentState = state
            case .current:
                ()
            @unknown default:
                fatalError()
            }
        }
        transitionAnimator.startAnimation()
    }
}

enum State {
    case closed
    case open
    
    var opposite: State {
        switch self {
        case .open: return .closed
        case .closed: return .open
        }
    }
}

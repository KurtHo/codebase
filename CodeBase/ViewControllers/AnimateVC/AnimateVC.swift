//
//  AnimateVC.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/2/15.
//

import UIKit

class AnimateVC: BaseVC {
    
    var views: AnimateViews!
    let vm = AnimateViewModel()
    
    override func loadView() {
        super.loadView()
        views = AnimateViews.init(vm: vm, owner: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        binding()
    }
    
    
    func binding() {
        
    }
}

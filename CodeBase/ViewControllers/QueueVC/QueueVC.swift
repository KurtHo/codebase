//
//  QueueVC.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/1/8.
//

import UIKit
@available(iOS 13.0, *)
class QueueVC: ToastVC {
    var views: QueueViews!
    var vm = QueueViewModel()
    
    override func loadView() {
        super.loadView()
        views = QueueViews.init(vm: vm, owner: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        binding()
        title = "concurrency call api"
        

//        vm.callAPIOneByAnother { [weak self] in
//            guard let _ = self else {return}
//            print("return to main thread")
//        }
            
        vm.callApiOneByOne {[weak self] _ in
            self?.views.tableView.reloadData()
            print("return to main thread")
        }

        debug("start call api one by another")
      
        vm.runSourceTimer()
        vm.checkMemoryPressure()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
            self?.showToast(message: "若沒東西或圖沒出現，要稍等一下")
        }
    }
    
    func binding() {
        views.tableView.dataSource = self
    }
}
@available(iOS 13.0, *)
extension QueueVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vm.articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ArticleTVC.identifier, for: indexPath) as! ArticleTVC
        cell.setContent(article: vm.articles[indexPath.row])
        
        return cell
    }
}

//
//  QueueViewModel.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/1/8.
//

import Foundation
import Combine
@available(iOS 13.0, *)
class QueueViewModel: BaseViewModel {
    
    var observers:[AnyCancellable] = []
    var employeeCount = 0
    var allEmployee:[EmployeeModel] = []
    var articles = [ArticleModel]()
    
    let employees = URL(string: "http://dummy.restapiexample.com/api/v1/employees")!
    let employee1 = URL(string: "http://dummy.restapiexample.com/api/v1/employee/1")!
    let employee2 = URL(string: "http://dummy.restapiexample.com/api/v1/employee/2")!
    let employee3 = URL(string: "http://dummy.restapiexample.com/api/v1/employee/3")!
    let employee4 = URL(string: "http://dummy.restapiexample.com/api/v1/employee/4")!
    
    let teslaNews = URL(string: "https://newsapi.org/v2/everything?q=tesla&apiKey=API_KEY")
    
    let techNews = URL(string: "https://newsapi.org/v2/top-headlines?sources=techcrunch&apiKey=API_KEY")
    
    let appleNews = URL(string: "https://newsapi.org/v2/everything?q=apple&apiKey=API_KEY")
    
    let usNews = URL(string: "")
    
    let twNews = URL(string: "")
    
    
    var sourceTimer: DispatchSourceTimer?
    
    func tryCombineGetData<T: Decodable>(url:URL, decodeType:T.Type, finish: ( (T?)->Void)?) -> Future<T, Error>? {
        return Future { promise in
            let session = URLSession.shared
            session.dataTask(with: url) { data, resp, err in
                var model:T? = nil
                if let data = data, err == nil {
                    do {
                        let _model = try JSONDecoder().decode(T.self, from: data)
                        model = _model
                        promise(.success(_model))
                        
                    }catch(let err) {
                        promise(.failure(err))
                    }
                    
                }else {
                    promise(.failure(err!))
                }
                finish?(model)
            }.resume()
        }
        
    }
    
    //should only observe when value is changed
    func tryOperateByQueueIsDoneWithCombine<T: Decodable>(urlsDeco:[(URL, T.Type)], completion: @escaping () -> Void) {
        urlsDeco.forEach{url, deco in
            tryCombineGetData(url: url, decodeType: deco.self) { model in
                
            }?.sink(receiveCompletion: { [weak self] finish in
                guard self != nil else {return}
                switch finish {
                case .finished:
                    completion()
                    
                case .failure(let err):
                    print("failure: \(err)")
                }
            }, receiveValue: { [weak self] someModel in
                guard let self = self else {return}
                if let employees = someModel as? EmployeesModel {
                    self.employeeCount = employees.data?.count ?? 0
                }
                
                if let employee = someModel as? EmployeeModel {
                    self.allEmployee.append(employee)
                }
                
                
            }).store(in: &observers)
        }

    }
    
    
    func getData<T: Decodable>(url:URL, decodeType:T.Type, finish:( (T?)->Void)?) {
        let session = URLSession.shared
        session.dataTask(with: url) { data, resp, err in
            if let err = err {
                print("err...: \(err)")
                finish?(nil)
            }else {
                print("urlStr: \(url.path), data: \(String(describing: data))")
                if let data = data {
                    let model = try? JSONDecoder().decode(T.self, from: data)
                    finish?(model)
                }
            }
            
        }.resume()
    }
    
    func operateByQueueIsDone(finish: @escaping () -> Void) {
        let operationE = BlockOperation { [weak self] in
            guard let self = self else {return}
            self.getData(url: self.employee4, decodeType: EmployeeModel.self) { model in
                if let model = model {
                    self.allEmployee.append(model)
                }
                
                finish()
            }
        }
        
        let operationD = BlockOperation { [weak self] in
            guard let self = self else {return}
            self.getData(url: self.employee3, decodeType: EmployeeModel.self) { model in
                if let model = model {
                    self.allEmployee.append(model)
                }
                finish()
                operationE.start()
            }
        }

        let operationC = BlockOperation { [weak self] in
            guard let self = self else {return}
            self.getData(url: self.employee2, decodeType: EmployeeModel.self) { model in
                if let model = model {
                    self.allEmployee.append(model)
                }
                finish()
                operationD.start()
            }
        }
        let operationB = BlockOperation { [weak self] in
            guard let self = self else {return}
            self.getData(url: self.employee1, decodeType: EmployeeModel.self) { model in
                if let model = model {
                    self.allEmployee.append(model)
                }
                finish()
                operationC.start()
            }
        }

        let operationA = BlockOperation { [weak self] in
            guard let self = self else {return}
            self.getData(url: self.employees, decodeType: EmployeesModel.self) { model in
                finish()
                operationB.start()
            }
        }
        operationA.start()
    }
    
    func callAPIOneByAnother(finish: @escaping ()->Void) {
        let semaphore = DispatchSemaphore(value: 0)
        let queue = DispatchQueue.global(qos: .background)
        
        queue.async {
            self.getData(url: self.employees, decodeType: EmployeesModel.self) { model in
                print(111)
                semaphore.signal()
            }
            semaphore.wait()
            
            self.getData(url: self.employee1, decodeType: EmployeeModel.self) { model in
                print(222)
                semaphore.signal()
            }
            semaphore.wait()
            
            self.getData(url: self.employee2, decodeType: EmployeeModel.self) { model in
                print(333)
                semaphore.signal()
            }
            semaphore.wait()
            
            self.getData(url: self.employee3, decodeType: EmployeeModel.self) { model in
                print(444)
                finish()
            }
        }
        
    }
    
    
    /// use Dispatch Semaphore
    func callApiOneByOne(finish: @escaping ([ArticleModel]) -> Void) {
        let semaphore = DispatchSemaphore(value: 0)
        let queue = DispatchQueue.global(qos: .background)
        
        queue.async {
            func getNewApi(_ source: Topic, lastOne: Bool = false) {
                api.get(.news(source: source), decode: NewsList.self) { [weak self] news, result in
                    if let first = news?.articles.first {
                        self?.articles.append(first)
                    }
                    finish(self?.articles ?? [])
                    if !lastOne {
                        semaphore.signal()
                    }
                    
                }
                if !lastOne {
                    semaphore.wait()
                }
            }
            
            getNewApi(.appleNews)
            getNewApi(.techNews)
            getNewApi(.teslaNews)
            getNewApi(.tw, lastOne: true)
            
//          
        }
    }
    
}


// https://www.jianshu.com/p/48521d5a8bb6
extension QueueViewModel {
    /// - Parameters:
    ///   - deadline: delay
    ///   - leeway: 予許誤差，設為零誤差還是一樣會有，設高點有助效能
    func runSourceTimer(
        deadline: Double = 3,
        leeway: DispatchTimeInterval = .nanoseconds(500)) {
        debug("source timer get start:\(Date.init().timeIntervalSince1970)")
        sourceTimer = DispatchSource.makeTimerSource(flags: .strict, queue: .main)
        sourceTimer?.schedule(deadline: .now() + deadline, repeating: 2, leeway: leeway)
        sourceTimer?.setEventHandler {
            debug("\(Date.init().timeIntervalSince1970)")
        }
        sourceTimer?.resume()
    }
    
    /// 目前這段code不會debug 出內存狀態
    /// MemoryPressureEvent是內存變換訊息,有四種選項:
    /// .all是所有狀態都監聽
    /// .normal是監聽回復正常的消息
    /// .warning是監聽內存警告的消息
    /// .critical是監聽記憶體進入緊急狀態的訊息 
    func checkMemoryPressure() {
        let memPresure = DispatchSource.makeMemoryPressureSource(eventMask: .all, queue: .global())
        memPresure.setEventHandler {
            debug("內存狀態")
        }
        memPresure.activate()
    }
}

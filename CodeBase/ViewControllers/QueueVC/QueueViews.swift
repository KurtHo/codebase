//
//  QueueViews.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/1/8.
//

import UIKit
@available(iOS 13.0, *)
class QueueViews: BaseView<QueueViewModel> {
    lazy var tableView: UITableView = {
        let tv = UITableView()
        tv.delegate = self
        tv.register(ArticleTVC.self, forCellReuseIdentifier: ArticleTVC.identifier)
        return tv
    }()
    
    override func addViews() {
        addSubview(tableView)
    }
    
    override func setConstraints() {
        tableView.setAnchor(top: safeAreaLayoutGuide.topAnchor, leading: leadingAnchor, trailing: trailingAnchor, bottom: safeAreaLayoutGuide.bottomAnchor, height: nil, width: nil)
    }
    
}

extension QueueViews: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}

//
//  File.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/2/14.
//

import UIKit

class ComplicatedVC: BaseVC {
    
    var views: ComplicatedViews!
    var vm = ComplicatedViewModel()
    
    
    override func loadView() {
        super.loadView()
        views = ComplicatedViews.init(vm: vm, owner: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "complicated vc"
        binding()
    }
    
    func binding() {
        views.collectionView.dataSource = self
    }
    
}

extension ComplicatedVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return vm.data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ComplicatedCollectionCell.identifier, for: indexPath) as? ComplicatedCollectionCell else {
            return UICollectionViewCell()
        }
        
        
        let str = String(vm.data[indexPath.row])
        cell.label.text = str
        
        return cell
    }
    
    
}

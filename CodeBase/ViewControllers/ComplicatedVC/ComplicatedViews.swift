//
//  ComplicatedViews.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/2/14.
//

import UIKit


class ComplicatedViews: BaseView<ComplicatedViewModel> {
 
    private lazy var maxHeaderHeight: CGFloat = frame.width / 4 * 2.8
    private let minHeaderHeight: CGFloat = 40
    private var previousScrollOffset: CGFloat = 0
    
    private var shrinkContentConstraint: NSLayoutConstraint {
        return pageContentView.constraints.filter{$0.firstAttribute == .height}.first!
    }
    
    
    override func addViews() {
        [pageContentView, segmentView, collectionView].forEach {
            addSubview($0)
        }
        
    }
    
    override func setConstraints() {
        pageContentView.snp.makeConstraints { make in
            make.top.equalTo(self.safeAreaLayoutGuide.snp.top)
            make.leading.trailing.equalTo(self)
            make.height.equalTo(maxHeaderHeight)
        }
        
        segmentView.snp.makeConstraints { make in
            make.leading.trailing.equalTo(pageContentView)
            make.bottom.equalTo(pageContentView.snp.bottom)
            make.height.equalTo(40)
        }
        
        collectionView.snp.makeConstraints { make in
            make.leading.trailing.equalTo(segmentView)
            make.top.equalTo(segmentView.snp.bottom)
            make.bottom.equalTo(self.snp.bottom)
        }
    }
    
    let pageContentView = ComplicatedViewsPageContentView()

    let segmentView: UIView = {
        let sv = UIView()
        sv.backgroundColor = .brown
        return sv
    }()
    
    lazy var collectionView: UICollectionView = {
        let flowlayout = UICollectionViewFlowLayout()
        flowlayout.minimumInteritemSpacing = 1
        flowlayout.minimumLineSpacing = 1
        flowlayout.scrollDirection = .vertical
        
        let width = UIScreen.main.bounds.width
        flowlayout.itemSize = CGSize(width: (width/3)-1, height: (width/3)-1)
        flowlayout.sectionInset = UIEdgeInsets(top: 1, left: 0, bottom: 11, right: 0)

        let cv = UICollectionView(frame: .zero, collectionViewLayout: flowlayout)
        cv.backgroundColor = .darkGray
        
        cv.register(ComplicatedCollectionCell.self, forCellWithReuseIdentifier: ComplicatedCollectionCell.identifier)
        cv.delegate = self
        return cv
    }()
    
}

extension ComplicatedViews {
    func canAnimateHeader (_ scrollView: UIScrollView) -> Bool {
        let scrollViewMaxHeight = scrollView.frame.height + self.shrinkContentConstraint.constant - minHeaderHeight
        return scrollView.contentSize.height > scrollViewMaxHeight
    }
    func setScrollPosition() {
        self.collectionView.contentOffset = CGPoint(x:0, y: 0)
    }
}
extension ComplicatedViews: UICollectionViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let scrollDiff = (scrollView.contentOffset.y - previousScrollOffset)
        let isScrollingDown = scrollDiff > 0
        let isScrollingUp = scrollDiff < 0
        if canAnimateHeader(scrollView) {
            var newHeight = shrinkContentConstraint.constant
            if isScrollingDown {
                newHeight = max(minHeaderHeight, shrinkContentConstraint.constant - abs(scrollDiff))
            } else if isScrollingUp {
                newHeight = min(maxHeaderHeight, shrinkContentConstraint.constant + abs(scrollDiff))
            }
            if newHeight != shrinkContentConstraint.constant {
                shrinkContentConstraint.constant = newHeight
                setScrollPosition()
                previousScrollOffset = scrollView.contentOffset.y
            }
        }
    }
}


//
//  ComplicatedViewsPageContentView.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/2/14.
//

import UIKit

class ComplicatedViewsPageContentView: UIView {
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addViews()
        setConstraints()
    }
    
    func addViews() {
        addSubview(scrollView)
        scrollView.setContentViews([red, orange, yellow, green, blue], superview: self)
        
        red.addSubview(redBtn)
    }
    
    func setConstraints() {
        scrollView.snp.makeConstraints { make in
            make.top.leading.trailing.bottom.equalTo(self)
        }
        
        DispatchQueue.main.async {
            self.redBtn.snp.makeConstraints { make in
                make.leading.trailing.bottom.equalTo(self.red)
                make.height.equalTo(self.red.frame.height)
            }
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let scrollView:PageScrollView = {
        let sv = PageScrollView()
        sv.isPagingEnabled = true
        sv.backgroundColor = .systemBlue
        return sv
    }()
    
    let red:UIView = {
        let v = UIView()
        v.backgroundColor = .red
        return v
    }()
    
    let orange:UIButton = {
        let v = UIButton()
        v.setTitle("orange btn", for: .normal)
        v.backgroundColor = .orange
        return v
    }()
    
    let yellow:UIView = {
        let v = UIView()
        v.backgroundColor = .yellow
        return v
    }()
    
    let green:UIView = {
        let v = UIView()
        v.backgroundColor = .green
        return v
    }()
    let blue:UIView = {
        let v = UIView()
        v.backgroundColor = .blue
        return v
    }()
    
    let redBtn:UIButton = {
        let btn = UIButton()
        btn.setTitle("red button", for: .normal)
        return btn
    }()
}

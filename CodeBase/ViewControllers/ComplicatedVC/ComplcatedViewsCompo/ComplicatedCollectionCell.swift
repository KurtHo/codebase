//
//  ComplicatedCollectionCell.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/2/14.
//

import UIKit

class ComplicatedCollectionCell: UICollectionViewCell {
    let label = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        label.textAlignment = .center
        addSubview(label)
        label.snp.makeConstraints { make in
            make.top.leading.trailing.bottom.equalTo(self)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

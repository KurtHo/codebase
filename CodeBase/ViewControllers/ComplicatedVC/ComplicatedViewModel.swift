//
//  ComplicatedViewModel.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/2/14.
//

import Foundation

class ComplicatedViewModel: BaseViewModel {
    let data:[Int] = {
        var d = [Int]()
        for i in 1...30 {
            d.append(i)
        }
        return d
    }()
}

//
//  NavigationItemVC.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/1/7.
//

import UIKit

@available(iOS 14.0, *)
class NavigationItemVC: BaseVC {
    var views: NavigationItemViews!
    let vm = NavigationItemViewModel()
    
    override func loadView() {
        super.loadView()
        title = "navigaiton item only for ios 14"
        views = NavigationItemViews.init(vm: vm, owner: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    
}

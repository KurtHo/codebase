//
//  NavigationItemViews.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/1/7.
//

import UIKit

@available(iOS 14.0, *)
class NavigationItemViews: BaseView<NavigationItemViewModel> {
    enum ItemsMenu: CaseIterable {
        case copy, rename, duplicate, move
        var title:String {
            switch self {
            case .copy:         return "Copy"
            case .rename:       return "Rename"
            case .duplicate:    return "Duplicate"
            case .move:         return "Move"
            }
        }
        
        var comment:String {
            switch self {
            case .copy:         return "Copy"
            case .rename:       return "Rename"
            case .duplicate:    return "Duplicate"
            case .move:         return "Move"
            }
        }
        
        var img:UIImage? {
            switch self {
            case .copy:         return UIImage(systemName: "doc.on.doc")
            case .rename:       return UIImage(systemName: "pencil")
            case .duplicate:    return UIImage(systemName: "plus.square.on.square")
            case .move:         return UIImage(systemName: "folder")
            }
        }
        
        static func from(str: String) -> Self {
            return Self.allCases.first { str == "\($0.title)" }!
        }
    }
    
    lazy var barItem: UIBarButtonItem = {
        let btn = UIButton()
        btn.setImage(.refresh, for: .normal)
        let bi = UIBarButtonItem()
        bi.image = .question
        let menuElem: [UIMenuElement] = ItemsMenu.allCases.map({item in
            return UIAction(title: NSLocalizedString(item.title, comment: item.comment), image: item.img, handler: menuHandler)
        })
        let menu = UIMenu(title: "", children: menuElem)
        bi.menu = menu
        return bi
    }()
    
    
    func menuHandler(action: UIAction) {
        let item = ItemsMenu.from(str: action.title)
        switch item {
        case .copy:
            print("copy...")
        case .rename:
            print("rename...")
        case .duplicate:
            print("duplicate...")
        case .move:
            print("move...")
        }
    }
    
    override func setupReferencingOutlets(owner: AnyObject?) {
        if let naviVC = owner as? NavigationItemVC {
            naviVC.navigationItem.rightBarButtonItem = barItem
        }
    }
}

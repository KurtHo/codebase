//
//  NavigationPopoverVC.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/1/8.
//

import UIKit

class NavigationPopoverVC: BaseVC {
    var views: NavigationPopoverViews!
    let vm = NavigationPopoverViewModel()
    let popoverVC = PopoverVC.init(type: .home)
    
    override func loadView() {
        super.loadView()
        self.views = NavigationPopoverViews.init(vm: vm, owner: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        binding()
        title = "Popover vc"
    }
    
    func binding() {
        popoverVC.subView.delegate = self
        views.barItem.addTarget(self, action: #selector(naviItemAct), for: .touchUpInside)
    }
    
    @objc func naviItemAct() {
        popoverVC.show()
    }
    
}

extension NavigationPopoverVC: DismissBackDelegate {
    
    func btnClick(sender: UIButton) {
        popoverVC.hide()
        if sender.tag == 0 {
            debug(sender.titleLabel?.text ?? "")
        }else if sender.tag == 1 {
            debug(sender.titleLabel?.text ?? "")
        }else if sender.tag == 2 {
            debug(sender.titleLabel?.text ?? "")
        }else if sender.tag == 3 {
            debug(sender.titleLabel?.text ?? "")
        }
    }
}

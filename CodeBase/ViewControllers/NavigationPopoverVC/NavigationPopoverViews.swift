//
//  NavigationPopoverViews.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/1/8.
//

import UIKit

class NavigationPopoverViews: BaseView<NavigationPopoverViewModel> {
    
     let barItem: UIButton = {
        let btn = UIButton()
        btn.setImage(.question, for: .normal)
        return btn
    }()
    
    override func setupReferencingOutlets(owner: AnyObject?) {
        if let vc = owner as? NavigationPopoverVC {
            vc.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: barItem)
        }
    }
    
}

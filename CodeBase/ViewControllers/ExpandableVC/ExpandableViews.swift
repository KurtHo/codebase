//
//  ExpandableViews.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2023/9/19.
//

struct Section {
    var genre: String
    var items: [String]
    var isExpanded: Bool
}

import UIKit

class ExpandableViews: BaseView<BaseViewModel> {
    var sections: [Section] = [
        Section(genre: "iPhone", items: ["iPhone X", "iPhone XR", "iPhone 11", "iPhone 12"], isExpanded: false),
        Section(genre: "iPad", items: ["iPad Air", "iPad Pro"], isExpanded: false),
        Section(genre: "Apple Watch", items: ["Apple Watch 1", "Apple Watch 2"], isExpanded: false)
    ]
    
    let tableView: UITableView = {
        let tableView = UITableView()
        return tableView
    }()
    
    
    override func setConstraints() {
        tableView.register(ExpandableTableViewCell.self, forCellReuseIdentifier: ExpandableTableViewCell.identifier)
        tableView.register(MyHeaderView.self, forHeaderFooterViewReuseIdentifier: MyHeaderView.reuseIdentifier)
        tableView.delegate = self
//        tableView.dataSource = self
        
        addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        tableView.backgroundColor = .gray
//        tableView.contentInsetAdjustmentBehavior = .never
        
        if #available(iOS 15.0, *) {
          tableView.sectionHeaderTopPadding = 0
        }
    }
}

extension ExpandableViews: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        UITableView.automaticDimension
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = MyHeaderView(seciton: self.sections[section])

        headerView.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(sectionTapped(_:)))
        headerView.addGestureRecognizer(tapGesture)
        headerView.tag = section
        
        return headerView
    }
    
    @objc func sectionTapped(_ sender: UITapGestureRecognizer) {
        guard let section = sender.view?.tag else { return }
        sections[section].isExpanded.toggle()
        tableView.reloadSections([section], with: .fade)
    }
}

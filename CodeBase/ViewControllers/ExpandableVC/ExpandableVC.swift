//
//  ExpandableVC.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/8/23.
//

import UIKit

class ExpandableVC: UIViewController {

    lazy var views = ExpandableViews(vm: BaseViewModel(), owner: self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        views = ExpandableViews(vm: BaseViewModel(), owner: self)
        views.tableView.dataSource = self
    }
    
}

//MARK: table view data source
extension ExpandableVC: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return views.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sections = views.sections
        return sections[section].isExpanded ? sections[section].items.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ExpandableTableViewCell.identifier, for: indexPath) as? ExpandableTableViewCell else { return UITableViewCell() }
        cell.titleLabel.text = views.sections[indexPath.section].items[indexPath.row]
        return cell
    }

}





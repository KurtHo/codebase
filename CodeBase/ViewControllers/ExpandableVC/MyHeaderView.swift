//
//  MyHeaderView.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2023/9/15.
//

import UIKit

class MyHeaderView: UITableViewHeaderFooterView {
//    var section: Section!
    var section: Bind<Section>!
    
    let label: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textColor = .black
        return label
    }()
    
    let line: UIView = {
        let v = UIView()
        v.backgroundColor = .gray
        return v
    }()
    
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(systemName: "chevron.down")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = #colorLiteral(red: 0.3382260101, green: 0.3382260101, blue: 0.3382260101, alpha: 1)
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    static let reuseIdentifier:String = "myHeaderView"
    
    init(seciton: Section) {
//        super.init(frame: .zero)
        super.init(reuseIdentifier: MyHeaderView.reuseIdentifier)
        [label, line, imageView].forEach {
            self.addSubview($0)
        }
        isUserInteractionEnabled = true
        self.section = Bind(value: seciton)
        label.text = section.value.genre
        
        
        label.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(8)
            make.height.centerY.equalToSuperview()
        }
        
        imageView.snp.makeConstraints { make in
            make.trailing.equalToSuperview().inset(14)
            make.centerY.equalToSuperview()
            make.width.height.equalTo(20)
        }
        
        line.snp.makeConstraints { make in
            make.top.equalTo(label.snp.bottom)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(1)
        }
        
        bindUp()

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func bindUp() {
        section.bindUp { _ in
            DispatchQueue.main.async {
                self.rotateImgView()
            }
        }
    }
    
    private func rotateImgView() {
        let expanded = self.section.value.isExpanded
        self.imageView.transform =  CGAffineTransform(rotationAngle: expanded ? 0 : CGFloat.pi)
        UIView.animate(withDuration: 0.3) {
            self.imageView.transform =  CGAffineTransform(rotationAngle: expanded ? CGFloat.pi : 0)
        }
    }
    
    func setContent(str: String) {
        
    }
}

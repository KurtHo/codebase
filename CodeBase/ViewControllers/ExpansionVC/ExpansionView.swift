import UIKit
import SnapKit

class ExpansionView: UIView {
    private var maxHeaderHeight: CGFloat!
    private var minHeaderHeight: CGFloat!
    private var previousScrollOffset: CGFloat = 0
    private var targetViewHeight:CGFloat!
    /// 需設定目標的bottom anchor
    var targetBottomConstraint: Constraint!
    
    private let topSpace = UIView()
    private let targetView: UIView
    private let scrollView: UIScrollView
    
    /// targetView的高一定要有
    /// bottomAnchor或topAnchor 設置其中一個即可
    init(targetView: UIView, scrollView: UIScrollView) {
        self.targetView = targetView
        self.scrollView = scrollView
        super.init(frame: .zero)
        scrollView.delegate = self
        setUI()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        setCollapseView(scrollView: scrollView, targetView: targetView)
        topSpace.snp.makeConstraints { make in
            targetBottomConstraint = make.bottom.equalTo(self.snp.top).offset(targetView.bounds.height).constraint
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    private func setUI() {
        [topSpace, scrollView].forEach{
            addSubview($0)
            $0.translatesAutoresizingMaskIntoConstraints = false
        }
        topSpace.addSubview(targetView)
        
        topSpace.snp.makeConstraints { make in
            make.top.leading.trailing.equalToSuperview()
        }
        
        scrollView.snp.makeConstraints { make in
            make.top.equalTo(topSpace.snp.bottom)
            make.leading.trailing.bottom.equalToSuperview()
        }

    }
}

extension ExpansionView: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let scrollDiff = (scrollView.contentOffset.y - previousScrollOffset)
        let isScrollingDown = scrollDiff > 0
        let isScrollingUp = scrollDiff < 0
        
        if canAnimateHeader(scrollView) {
            var newHeight = targetViewHeight
            if isScrollingDown {
                newHeight = max(minHeaderHeight, targetViewHeight - abs(scrollDiff))
                
            } else if isScrollingUp {
                newHeight = min(maxHeaderHeight, targetViewHeight + abs(scrollDiff))
                
            }
            if newHeight != targetViewHeight {
                targetViewHeight = newHeight
                self.targetBottomConstraint.update(offset: newHeight ?? 0)
                
                setScrollPosition(scrollView)
                previousScrollOffset = scrollView.contentOffset.y
            }
        }
    }
    
    func setCollapseView(
        scrollView: UIScrollView,
        targetView: UIView,
        minHeaderHeight: CGFloat = 0
    ) {
        scrollView.delegate = self
        targetView.superview?.layoutIfNeeded()
        
        self.targetViewHeight = targetView.bounds.height
        self.maxHeaderHeight = targetView.bounds.height
        self.minHeaderHeight = minHeaderHeight
    }
    
    func canAnimateHeader (_ scrollView: UIScrollView) -> Bool {
        let scrollViewMaxHeight = scrollView.frame.height + self.targetViewHeight - minHeaderHeight
        return scrollView.contentSize.height > scrollViewMaxHeight
    }
    
    func setScrollPosition(_ sv: UIScrollView) {
        sv.contentOffset = CGPoint(x:0, y: 0)
    }
}

import UIKit

class ExpansionVC: UIViewController {
    
    lazy var expansionView =
    ExpansionView(targetView: topView, scrollView: tableView)
    
    let tableView = UITableView()
    let topView = UIView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        setTableView()
        view.backgroundColor = .white
    }
        
    private func setUI() {
        view.addSubview(expansionView)
        topView.backgroundColor = .red
        expansionView.backgroundColor = .orange
        tableView.backgroundColor = .blue
        
        expansionView.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
//            make.top.equalTo(view.safeAreaLayoutGuide.snp.top).offset(BaseNavigationController.topBarHeight)
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top)
        }
        
        topView.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.top.equalToSuperview().offset(20)
            make.width.equalTo(200)
            make.height.equalTo(44)
        }
    }
    
    private func setTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
}

extension ExpansionVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        55
    }
}

extension ExpansionVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 25
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = "cell: \(indexPath.row)"
        return cell
    }
    
    
}

//
//  SecondVC.swift
//  CodeBase
//
//  Created by KurtHo on 2020/12/1.
//

import UIKit

class SecondVC: BaseVC {
    let list = [ #colorLiteral(red: 0.5843137255, green: 0.8823529412, blue: 0.8274509804, alpha: 1) ,#colorLiteral(red: 0.9529411765, green: 0.5058823529, blue: 0.5058823529, alpha: 1), #colorLiteral(red: 0.9882352941, green: 0.8901960784, blue: 0.5411764706, alpha: 1)]
    lazy var views = SecondViews(superview: view)
    
    convenience init(str:String){
        self.init(nibName: nil, bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        views.setPosition()
        views.collectionView.delegate = self
        views.collectionView.dataSource = self
        views.collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "MyCell")
    }
    
    override func viewDidLayoutSubviews() {
      super.viewDidLayoutSubviews()
      views.collectionView.scrollToItem(at: IndexPath(item: 1, section: 0), at: .left, animated: false)
    }
}

extension SecondVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return list.count
        }
        
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyCell", for: indexPath)

        cell.contentView.backgroundColor = list[indexPath.row]
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 414, height: 700)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageFloat = (scrollView.contentOffset.x / scrollView.frame.size.width)
        let pageInt = Int(round(pageFloat))
        
        switch pageInt {
        case 0:
            views.collectionView.scrollToItem(at: [0, 3], at: .left, animated: false)
        case list.count - 1:
            views.collectionView.scrollToItem(at: [0, 1], at: .left, animated: false)
        default:
            break
        }
    }
    
    
}

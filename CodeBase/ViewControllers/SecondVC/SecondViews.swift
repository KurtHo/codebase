//
//  SecondViews.swift
//  CodeBase
//
//  Created by CI-Kurt on 2021/6/3.
//

import UIKit

class SecondViews: NSObject {
    var superview: UIView
    
    let segmentControl: PageSegmentControl = {
        var sc = PageSegmentControl()
        sc.commaSeparatedButtonTitles = "TAB 1, TAB2, TAB3"
        sc.textColor = .gray
        sc.backgroundColor = .yellow
        sc.isFill = false
        return sc
    }()
    
    var collectionView: UICollectionView = {
        let flowlayout = UICollectionViewFlowLayout()
        flowlayout.minimumInteritemSpacing = 0
        flowlayout.minimumLineSpacing = 0
        flowlayout.scrollDirection = .horizontal
        
        let cv = UICollectionView(frame: .zero, collectionViewLayout: flowlayout)
        cv.backgroundColor = .systemBlue
        cv.isPagingEnabled = true
        cv.showsHorizontalScrollIndicator = false
        return cv
    }()
    
    
    init(superview: UIView) {
        self.superview = superview
        
        super.init()
    }
    
    
    func setPosition() {
        [segmentControl, collectionView].forEach {
            superview.addSubview($0)
        }
        
        segmentControl.setAnchor(top: superview.safeAreaLayoutGuide.topAnchor, leading: superview.leadingAnchor, trailing: superview.trailingAnchor, bottom: nil, height: 40, width: nil)
        
        collectionView.setAnchor(top: segmentControl.bottomAnchor, leading: superview.leadingAnchor, trailing: superview.trailingAnchor, bottom: superview.bottomAnchor, height: nil, width: nil)
    }
    
}

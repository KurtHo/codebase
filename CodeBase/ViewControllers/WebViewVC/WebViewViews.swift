//
//  WebViewViews.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/2/17.
//

import UIKit
import WebKit

class WebViewViews: BaseView<WebViewViewModel> {
    
    override func addViews() {
        [webView, preBtn, nextBtn].forEach {
            addSubview($0)
        }
        
        loadWebView()
    }
    
    override func setConstraints() {
        webView.snp.makeConstraints { make in
            make.top.leading.trailing.equalTo(self.safeAreaLayoutGuide)
            make.bottom.equalTo(preBtn.snp.top)
        }
        
        preBtn.snp.makeConstraints { make in
            make.leading.equalTo(webView)
            make.width.equalTo(self.snp.width).dividedBy(2)
            make.height.equalTo(40)
            make.bottom.equalTo(self.safeAreaLayoutGuide)
        }
        
        nextBtn.snp.makeConstraints { make in
            make.top.equalTo(webView.snp.bottom)
            make.leading.equalTo(preBtn.snp.trailing)
            make.width.equalTo(self.snp.width).dividedBy(2)
            make.height.equalTo(40)
            make.bottom.equalTo(self.safeAreaLayoutGuide)
        }
     
    }
    
    func loadWebView() {
        let request = URLRequest(url: self.viewModel.url!)
        webView.load(request)
    }
    
    lazy var webView: WKWebView = {
        let wk = WKWebView(frame: .zero, configuration: WKWebViewConfiguration())
        return wk
    }()
    
    let preBtn:UIButton = {
        let btn = UIButton()
        btn.backgroundColor = .black
        btn.setTitle("pre", for: .normal)
        return btn
    }()
    
    let nextBtn:UIButton = {
        let btn = UIButton()
        btn.backgroundColor = .blue
        btn.setTitle("next", for: .normal)
        return btn
    }()
}


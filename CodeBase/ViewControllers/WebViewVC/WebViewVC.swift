//
//  WebViewVC.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/2/17.
//

import UIKit
import WebKit

class WebViewVC: BaseVC {
    
    var views: WebViewViews!
    var vm = WebViewViewModel()
    
    override func loadView() {
        super.loadView()
        views = WebViewViews.init(vm: vm, owner: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        binding()
    }
    
    func binding() {
        views.webView.navigationDelegate = self
        views.preBtn.addTarget(self, action: #selector(goBack), for: .touchUpInside)
        views.nextBtn.addTarget(self, action: #selector(goForward), for: .touchUpInside)
    }
    
    @objc func goBack() {
        views.webView.goBack()
    }
    
    @objc func goForward() {
        views.webView.goForward()
    }
}

extension WebViewVC: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        let targetWord = "iOS and iPadOS usage"
        let script = "var element = document.evaluate(\"//*[contains(text(), '\(targetWord)')]\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue; element.scrollIntoView();"    
        debug("我看過ui了，都是在主線程跑，這蠻有可能是版本的問題 \nhttps://developer.apple.com/forums/thread/713290")
        
        webView.evaluateJavaScript(script) { (_, error) in
            if let error = error {
                debug("Error: \(error)")
            }
        }
    }
}

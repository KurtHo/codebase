//
//  PopoverVC.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/1/8.
//

import UIKit
import SnapKit

public enum PopoverType {
    case home
}

protocol DismissBackDelegate: AnyObject {
    func btnClick(sender: UIButton)
}

class PopoverVC: UIView {
    
    var popoverType:PopoverType = .home
    
    convenience init(type:PopoverType) {
        self.init(frame: CGRect.zero)
        self.popoverType = type
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        setUpGlobalViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy var subView : PopoverView = {
        let view = PopoverView.init(type: popoverType)
        return view
    }()
    
    func setUpGlobalViews() {
        let bg_btn = UIButton(frame:CGRect(x:0,y:0,width:kScreenW,height:kScreenH))
        bg_btn.backgroundColor = UIColor.clear
        self.addSubview(bg_btn)
        bg_btn.addTarget(self, action: #selector(cancleAction), for: .touchDown)
    }

    func show(){

        DispatchQueue.main.async {
            self.alpha = 1
            self.isHidden = false

            UIApplication.shared.keyWindow?.addSubview(self)
            self.snp.makeConstraints({ (make) in
                make.edges.equalToSuperview()
            })

            self.addSubview(self.subView)
            self.subView.snp.makeConstraints({ (make) in
                make.top.equalToSuperview().offset(kSafeAreaTopHeight + 5)
                make.right.equalToSuperview().inset(15)
                make.size.equalTo(CGSize(width: self.subView.viewWidth, height: self.subView.viewHeight))
            })
        }
    }

    func hide(){
        DispatchQueue.main.async {
            UIView.animateKeyframes(withDuration: 0.25, delay: 0, options: [], animations: {
                self.alpha = 0
            }) { (_) in
                self.isHidden = true
                self.subView.removeFromSuperview()
            }
        }
    }
    
    @objc func cancleAction() {
        UIApplication.shared.keyWindow?.addSubview(self)
        UIApplication.shared.keyWindow?.bringSubviewToFront(self)
        
        UIView.animate(withDuration: 0.25, animations: {
            self.alpha = 0
        }){ _ in
            self.removeFromSuperview()
            
        }
    }
}


//
//  PopoverView.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/1/8.
//
import UIKit

class PopoverView: UIView {
    
    weak var delegate: DismissBackDelegate?
    var viewWidth: CGFloat = 150
    var viewHeight: CGFloat = 0
    
    var btnArray: [UIButton] = []
    var lineArray: [UIView] = []
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    convenience init(type:PopoverType) {
        self.init(frame: CGRect.zero)
        backgroundColor = .red
        layer.cornerRadius = 8
        setupView(type)
    }
    
    func setupView(_ type:PopoverType){
        
        addSubview(bannerImage)
        bannerImage.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        switch type {
        case .home:
            btnArray = [addBtn,cameraBtn,searchGroupBtn,creatGroupBtn]
            viewHeight = 215
//        case .chat:
//            btnArray = [businessBtn,stickyChatBtn]
//            viewHeight = 113
//
//        case .contact:
//            btnArray = [addBtn,cameraBtn]
//            viewHeight = 113
//        case .redEnvelop:
//            btnArray = [receivedRedEnvelopeCellBtn,sendRedEnvelopeCellBtn]
//            viewHeight = 113
        }
        
        for _ in 1...btnArray.count-1 {
            let line = UIView()
            line.backgroundColor = .black
            lineArray.append(line)
        }
        
        for index in 0...btnArray.count-1{
            addSubview(btnArray[index])
            
            if index == 0 {
                btnArray[index].snp.makeConstraints { (make) in
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().inset(15)
                    make.top.equalToSuperview().offset(12)
                    make.height.equalTo(50)
                }
                
                addSubview(lineArray[index])
                lineArray[index].snp.makeConstraints { (make) in
                    make.leftMargin.rightMargin.equalTo(btnArray[index])
                    make.top.equalTo(btnArray[index].snp.bottom)
                    make.height.equalTo(1)
                }
            }else{
                
                btnArray[index].snp.makeConstraints { (make) in
                    make.leftMargin.rightMargin.equalTo(btnArray[index-1])
                    make.top.equalTo(lineArray[index-1].snp.bottom)
                    make.height.equalTo(50)
                }

                if index != btnArray.count-1 {
                    addSubview(lineArray[index])
                    lineArray[index].snp.makeConstraints { (make) in
                        make.leftMargin.rightMargin.equalTo(btnArray[index-1])
                        make.top.equalTo(btnArray[index].snp.bottom)
                        make.height.equalTo(1)
                    }
                }
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func btnClick(sender: UIButton) {
        delegate?.btnClick(sender: sender)
    }
    
    lazy var bannerImage: UIView = {
        let iv = UIImageView(image: UIImage(named: "common_addvc"))
        return iv
    }()
    
    lazy var addBtn: UIButton = {
        let btn = UIButton()
        btn.tag = 0
        btn.setTitleColor(.black, for: .normal)
//        btn.setFont(fontName: .PingFangTCRegular, size: 15, color: .black)
        btn.setTitle("添加联络人", for: .normal)
        btn.addTarget(self, action: #selector(btnClick), for: .touchDown)
//        let image = UIImage(named: "maintabbar_dynamic")
//        btn.setImage(image, for: .normal)
        
        if let titleLabel = btn.titleLabel,let imageView = btn.imageView{
            let titleFrame = titleLabel.frame
            let imageFrame = imageView.frame
            let space = titleFrame.origin.x - imageFrame.origin.x - imageFrame.size.width
            
            btn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -titleFrame.width+20)
            btn.titleEdgeInsets = UIEdgeInsets(top: 0, left: -imageFrame.width, bottom: 0, right: 0)
        }
        
        return btn
    }()
    
    lazy var cameraBtn: UIButton = {
        let btn = UIButton()
        btn.tag = 1
        btn.setTitleColor(.black, for: .normal)
//        btn.setFont(fontName: .PingFangTCRegular, size: 15, color: .black)
        btn.setTitle("扫ㄧ扫", for: .normal)
        btn.addTarget(self, action: #selector(btnClick), for: .touchDown)
//        let image = UIImage(named: "maintabbar_dynamic")
//        btn.setImage(image, for: .normal)
        
        if let titleLabel = btn.titleLabel,let imageView = btn.imageView{
            let titleFrame = titleLabel.frame
            let imageFrame = imageView.frame
            let space = titleFrame.origin.x - imageFrame.origin.x - imageFrame.size.width
            
            btn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -titleFrame.width+60)
            btn.titleEdgeInsets = UIEdgeInsets(top: 0, left: -imageFrame.width, bottom: 0, right: 0)
        }
        
        return btn
    }()
    
    lazy var searchGroupBtn: UIButton = {
        let btn = UIButton()
        btn.tag = 2
        btn.setTitleColor(.black, for: .normal)
//        btn.setFont(fontName: .PingFangTCRegular, size: 15, color: .black)
        btn.setTitle("搜寻群组", for: .normal)
        btn.addTarget(self, action: #selector(btnClick), for: .touchDown)
//        let image = UIImage(named: "maintabbar_dynamic")
//        btn.setImage(image, for: .normal)
        
        if let titleLabel = btn.titleLabel,let imageView = btn.imageView{
            let titleFrame = titleLabel.frame
            let imageFrame = imageView.frame
            let space = titleFrame.origin.x - imageFrame.origin.x - imageFrame.size.width
            
            btn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -titleFrame.width+40)
            btn.titleEdgeInsets = UIEdgeInsets(top: 0, left: -imageFrame.width, bottom: 0, right: 0)
        }
        
        return btn
    }()
    
    lazy var creatGroupBtn: UIButton = {
        let btn = UIButton()
        btn.tag = 3
        btn.setTitleColor(.black, for: .normal)
//        btn.setFont(fontName: .PingFangTCRegular, size: 15, color: .black)
        btn.setTitle("创建群组", for: .normal)
        btn.addTarget(self, action: #selector(btnClick), for: .touchDown)
//        let image = UIImage(named: "maintabbar_dynamic")
//        btn.setImage(image, for: .normal)
        
        if let titleLabel = btn.titleLabel,let imageView = btn.imageView{
            let titleFrame = titleLabel.frame
            let imageFrame = imageView.frame
            let space = titleFrame.origin.x - imageFrame.origin.x - imageFrame.size.width
            
            btn.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -titleFrame.width+40)
            btn.titleEdgeInsets = UIEdgeInsets(top: 0, left: -imageFrame.width, bottom: 0, right: 0)
        }
        
        return btn
    }()
    
    
}



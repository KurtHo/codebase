//
//  BaseView.swift
//  CodeBase
//
//  Created by KurtHo on 2021/12/7.
//

import UIKit
import RxSwift
import RxCocoa
import SnapKit

class BaseViewModel: NSObject {
    let disposeBag = DisposeBag()
}

class BaseView<T: BaseViewModel>: UIView {
    
    unowned var viewModel: T!
    weak var owner: AnyObject?
    let disposeBag = DisposeBag()
    
    init() {
        super.init(frame: .zero)
    }
    
    init(vm: T, owner: AnyObject?, frame: CGRect? = nil) {
        if let f = frame {
            super.init(frame: f)
        } else {
            var rect = UIScreen.main.bounds
            rect.size.height -= (kSafeAreaTopHeight+kSafeTabBarHeight)
            super.init(frame: rect)
        }
        backgroundColor = .white
        viewModel = vm
        self.owner = owner
        addViews()
        setConstraints()
        setAnimation()
        setupOutlets(owner: owner)
        setupReferencingOutlets(owner: owner)
        setupReceivedActions(owner: owner)
        
        for subview in subviews {
            subview.layoutSubviews()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /// 初始化視圖參數
    func addViews() -> Void{
        
    }
    
    /// 設置子視圖約束
    func setConstraints() -> Void{
        
    }
    
    func setAnimation() -> Void {
        
    }
    
    /// 設置視圖關聯關係
    func setupOutlets(owner: AnyObject?){
        if owner != nil && owner!.isKind(of: UIViewController.classForCoder()){
            (owner as! UIViewController).view = self
        }
    }
    
    /// 設置協議引用關聯關係
    func setupReferencingOutlets(owner: AnyObject?){
        
    }
    
    /// 設置事件關聯關係
    func setupReceivedActions(owner: AnyObject?){
        
    }
    
    /// 重畫頁面資料
    func reloadView() {
        
    }
    
    /// TableView Selected change color
    func setCellSelected(tableView: UITableView?, indexPath: IndexPath) {
        guard let _tableView = tableView else {
            return
        }
        for section in 0..<_tableView.numberOfSections {
            for row in 0..<_tableView.numberOfRows(inSection: section) {
                var selected = false
                if section == indexPath.section && row == indexPath .row {
                    selected = true
                }
                _tableView.cellForRow(at: IndexPath(row: row, section: section))?.isSelected = selected
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
}




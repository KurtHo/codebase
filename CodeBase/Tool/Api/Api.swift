//
//  Api.swift
//  CodeBase
//
//  Created by CI-Kurt on 2021/6/14.
//

import UIKit
import Moya

class Api {
    enum ErrorResponse {
        case Timeout, UrlErorr
    }
    
    enum ApiError {
        case sessionError(err:Error)
        case decodeError(string:String)
        case urlError(string:String)
    }

    typealias ErrorCallBack = ((ApiError)->Void)?
    
    typealias ResponseData<T:Decodable> = (([T.R]?, ErrorResponse?) -> Void) where T: HoldingResults
    
    static func get<T: Decodable>(
        urlStr:String,
        model: T.Type,
        completion: @escaping Api.ResponseData<T>
    ) {
        
        guard let url = URL(string: urlStr) else {
            print("Invalid URL")
            DispatchQueue.main.async {
                completion(nil, .UrlErorr)
            }
            return
        }
        let request = URLRequest(url: url)
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            if let data = data {
                do {
                    let decodedResponse = try JSONDecoder().decode(model, from: data)
                    DispatchQueue.main.async {
                        completion(decodedResponse.results, nil)
                    }
                    
                } catch {
                    print(error)
                }
            }else {
                print("Fetch failed: \(error?.localizedDescription ?? "Unknown error")")
            }
            
        }.resume()
    }
    
    /// GeneralResponseModel 用於api會有固定data
    static func get<T>(
        _ apiContent: ApiContent,
        decode: T.Type,
        completion: @escaping (GeneralResponseModel<T>?, Result<Response, MoyaError>) -> Void) where T: Decodable
    {
        let moya = MoyaProvider<ApiContent>()
        let decoder = JSONDecoder()
//        var loginRespModel:LoginRespModel?
        
        moya.request(apiContent) { result in
            switch result {
            case let .success(resp):
                do {
                    let model = try decoder.decode(GeneralResponseModel<T>.self, from: resp.data)
//                    debug("model: \(model)")
                    
//                    if let loginResp = model.data as? LoginRespModel {
//                        loginRespModel = loginResp
//                    }
                    
                    completion(model, result)
                    
                } catch let err {
                    debug("decode fail: \(err)")
                    completion(nil, result)
                }
                
            case let .failure(err):
                debug(err)
                completion(nil, result)
            }
        }
    }
    
    
/// Download image and save it to Caches
/// - Parameters:
///   - urlStr: image url string
///   - defaultImage: defaultImage
///   - finish: call back
///   - errCB: error call back
    static func downloadImage(urlStr: String?, defaultImage: UIImage? = nil, finish: @escaping (UIImage?, String?)->Void, errCB: ErrorCallBack = nil){
        guard let urlString = urlStr, let url = URL(string: urlString)  else {
            finish(defaultImage, urlStr)
            return
        }
        
        if let image = Caches.shared.image.object(forKey: urlString as NSString) {
            finish(image, urlString)
        }else{
            URLSession.shared.dataTask(with: url){ data, resp, err in
                if let err = err {
                    errCB?(.sessionError(err: err))
                    finish(defaultImage, urlStr)
                }
                guard let data = data, let image = UIImage(data: data) else {
                    finish(defaultImage, urlStr)
                    return
                }
                DispatchQueue.main.async {
                    Caches.shared.image.setObject(image, forKey: urlString as NSString)
                    finish(image, urlString)
                }
  
            }.resume()
        }
    }
}




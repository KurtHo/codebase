//
//  CombineApi.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/1/7.
//

import Foundation
import Combine

@available(iOS 13.0, *)
let combineApi = CombineApi.shared
@available(iOS 13.0, *)
class CombineApi {
    private init(){}
    static let shared = CombineApi()
    
    func getData() -> Future<NewsList?, Error>? {
        return Future { promise in
            api.get(.news(source: .us), decode: NewsList.self) { newList, result in
                switch result {
                case .success(_):
                    promise(.success(newList))
                case .failure(let err):
                    promise(.failure(err))
                }
            }
        }
    }

}

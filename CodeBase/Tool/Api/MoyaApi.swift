//
//  MoyaApi.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/1/6.
//

import Foundation
import Moya

enum Topic {
    case tw, us, teslaNews, techNews, appleNews
    
    var value:String {
        switch self {
        case .tw:           return "tw"
        case .us:           return "us"
        case .teslaNews:    return "tesla"
        case .techNews:     return "techcrunch"
        case .appleNews:    return "apple"
        }
    }
}

enum ApiContent {
    /// news的api
    case news(source:Topic)
}


//https://newsapi.org/v2/top-headlines?country=us&apiKey=3983486410c94b188a7840b7e4fd5eb8
extension ApiContent: TargetType {
    var baseURL: URL { URL(string: "https://newsapi.org/v2")! }
    var apiKey: String {"3983486410c94b188a7840b7e4fd5eb8"}
    
    var method: Moya.Method {
        switch self {
        case .news:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .news(let topic):
            switch topic {
            case .tw, .us, .techNews:
                return  "/top-headlines"
            case .appleNews:
                return "everything"
            case .teslaNews:
                return "/everything"
            }
            
        }
    }
    
    var task: Task {
        switch self {
        case .news(let topic):
            
            switch topic {
            case .us, .tw:
                return .requestParameters(
                    parameters: ["apiKey": apiKey, "country": topic],
                    encoding: URLEncoding.default
                )
            case .teslaNews:
                return .requestParameters(
                    parameters: ["apiKey": apiKey, "q": topic.value],
                    encoding: URLEncoding.default
                )
                
            case .techNews:
                return .requestParameters(
                    parameters: ["sources": topic.value, "apiKey": apiKey],
                    encoding: URLEncoding.default
                )
                
            case .appleNews:
                return .requestParameters(
                    parameters: ["q": topic.value, "apiKey": apiKey],
                    encoding: URLEncoding.default
                )
            }
            
        }
    }
    
    var headers: [String : String]? {
        return [
            "Content-type": "application/json"
        ]
    }
    
}

public let api = MoyaApi.shared
public class MoyaApi {
    private init(){}
    static fileprivate let shared = MoyaApi()
    private var imageCache = NSCache<NSString, UIImage>()
    
    let authPlugin = AccessTokenPlugin { _ in "access_token" }
//    lazy var moya = MoyaProvider<ApiContent>(plugins: [authPlugin])
    let moya = MoyaProvider<ApiContent>()
    let decoder = JSONDecoder()
    
    func get<T>(_ apiContent: ApiContent, decode: T.Type ,completion: @escaping (T?, Result<Response, MoyaError>) -> Void) where T: Decodable {
        moya.request(apiContent) { result in
            
            switch result {
            case let .success(resp):
                debug(resp.request?.url ?? "")
                do {
                    let model = try self.decoder.decode(T.self, from: resp.data)
                    completion(model, result)
                } catch(let err) {
                    print("decode fail: \(err)")
                }
                
            case let .failure(err):
                completion(nil, result)
                print(err)
            }
        }
    }
    
    func downloadImage(
        urlStr: String?,
        defaultImage: UIImage? = nil,
        finish: @escaping (UIImage?, String?)->())
    {
        guard
            let urlString = urlStr,
            let url = URL(string: urlString)
        else {
            debug("err convert url fail: \(urlStr ?? "")")
            finish(defaultImage, urlStr)
            return
        }
        
        if let image = imageCache.object(forKey: urlString as NSString) {
            finish(image, urlString)
        } else {
            URLSession.shared.dataTask(with: url) { (data, response, err) in
                guard
                    let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200,
                    let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                    let data = data, err == nil,
                    let image = UIImage(data: data)
                else {
                    finish(defaultImage, urlString)
                    return
                }

                self.imageCache.setObject(image, forKey: urlString as NSString)
                finish(image, urlString)
                
            }.resume()
            
        }
    
    }
    
}

//
//  InstiateVC.swift
//  CodeBase
//
//  Created by CI-Kurt on 2021/6/14.
//

import UIKit

protocol VcMapDelegate {
    var storyBoardId: String {get}
    var identifier: String {get}
}


//let vc = InstantiaVC.viewController(.secondVc) as! SecondVC

/// <#Description#>
class InstantiaVC {
    static func viewController(_ vc: InstantiaVC.RequestApi) -> UIViewController {
        let sb = UIStoryboard(name: vc.storyBoardId, bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: vc.identifier)
        return vc
    }
    
    enum RequestApi: VcMapDelegate {
        case firstVc, secondVc
        
        var storyBoardId: String{
            switch self {
            case .firstVc, .secondVc:
                return "Main"
            }
        }
        
        var identifier: String {
            switch self {
            case .firstVc:      return "FirstVC"
            case .secondVc:     return "SecondVC"
            }
        }
    }
}


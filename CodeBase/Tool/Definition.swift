//
//  Definition.swift
//  CodeBase
//
//  Created by KurtHo on 2021/12/7.
//

import UIKit
// MARK: - Screen Size
let kScreenH: CGFloat = UIScreen.main.bounds.size.height
let kScreenW: CGFloat = UIScreen.main.bounds.size.width

// MARK: - Screen Adaption
let kNavigationbarHeight:  CGFloat = 44.0
let kSafeAreaStatusHeight: CGFloat = kScreenH >= 812.0 ? 44.0: 20.0
let kSafeAreaBottomHeight: CGFloat = kScreenH >= 812.0 ? 34.0 : 0.0
let kSafeAreaTopHeight:    CGFloat = kSafeAreaStatusHeight + kNavigationbarHeight
let kSafeTabBarHeight:     CGFloat = kScreenH >= 812.0 ? 83 : 49

// MARK: - Constant
let kButtonHeight: CGFloat = 39
let kAssetHeaderHeight: CGFloat = 116
let kPortraitSize: CGSize = CGSize(width: 73, height: 73)
let kTableFooterHeight: CGFloat = 35
let kCornerRadius: CGFloat = 10
let edgeOffset: CGFloat = 10
let kBankPickerH: CGFloat = 270
let kHorizonInset: CGFloat = 20

// MARK: - Ratio
let kScreenWidthRatio = kScreenW / 375.0
let kScreenHeightRatio = kScreenH / 667.0
let itemHeaderHeight: CGFloat = 72.0

// MARK: - APP
var appDelegate: AppDelegate? {
    return UIApplication.shared.delegate as? AppDelegate
}

var iPhoneXAbove: Bool {
    return kScreenH >= 812.0
}

// MARK: - 屏幕判断
// 4inch
var isIphone5s: Bool {
    if (kScreenH == 568.0 && kScreenW == 320.0) || (kScreenH == 320.0 && kScreenW == 568.0) {
        return true
    } else {
        return false
    }
}
// 4.7inch
var isIphone6s: Bool {
    if (kScreenH == 667.0 && kScreenW == 375.0) || (kScreenH == 375.0 && kScreenW == 667.0) {
        return true
    } else {
        return false
    }
}

// 5.5inch
var isIphone6Plus: Bool{
    
    if (kScreenH == 736.0 && kScreenW == 414.0) || (kScreenH == 414.0 && kScreenW == 736.0) {
        return true
    }else{
        return false
    }
}

// 5.8inch
var isIphoneX: Bool{
    
    if (kScreenH == 812.0 && kScreenW == 375.0) || (kScreenH == 375.0 && kScreenW == 812.0) {
        return true
    }else{
        return false
    }
    
}

/// 6.1inch (xr / xs max)
var isIphoneXR: Bool{
    if (kScreenH == 896.0 && kScreenW == 414.0) || (kScreenH == 414.0 && kScreenW == 896.0){
        return true
    }else {
        return false
    }
}

/// 当前版本号
var versionNumber: String {
    get {
        return Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String ?? ""
    }
}

/// build号
var buildNumber: String {
    get {
        return Bundle.main.infoDictionary!["CFBundleVersion"] as? String ?? ""
    }
}


//
//  Bind.swift
//  CodeBase
//
//  Created by KurtHo on 2020/12/1.
//

// way without rx
class Bind<T> {
    typealias Listener = (T) -> Void
    var listener: Listener?
    
    var value:T  {
        didSet{
            listener?(self.value)
        }
    }
    
    init(value:T) {
        self.value = value
    }
    
    func bindUp(listener:Listener?){
        self.listener = listener
        self.listener?(value)
    }
    
    func renew(){
        listener?(self.value)
    }
}



//
//  VideoManager.swift
//  SliderVideo
//
//  Created by CI-Kurt on 2020/12/23.
//

//MARK:- Check File Manager In Simulator
//Use print(NSHomeDirectory()) and copy the path.
//Open Finder app and press Shift+Cmd+G
//Paste the copied path and press GO

import Foundation
import AVKit

//more technique ↓↓↓
//https://www.freecodecamp.org/news/video-stories-and-caching-ios/
class VideoManager: AsynchronousOperation {
    
    enum ExportStatus {
        case sucess, fail, isExist
    }
    
    static let shared = VideoManager()
    let manager = FileManager.default
    private var exportSession: AVAssetExportSession?
    
    func createVideoFolder(){
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        let docURL = URL(string: documentsDirectory)!
        
        for folderCase in VideoFolder.allCases {
            let dataPath = docURL.appendingPathComponent(folderCase.subFolder)
            if manager.fileExists(atPath: dataPath.absoluteString) == false {
                do {
                    try manager.createDirectory(atPath: dataPath.absoluteString, withIntermediateDirectories: true, attributes: nil)
                    debug("create music folder ...")
                } catch {
                    debug(error.localizedDescription);
                }
            }
        }

    }
    
    //Summary
    /** 比對FileManager裡的VideoFolder，以url的後段判斷
        例如  http://techslides.com/demos/sample-videos/small.mp4，則名稱為small.mp4
        if user lost connection, the export will be failed !!
        Data = loca data
        URL = local URL
    */
//    func getDataFor(_ item: PlayerItem, folder:VideoFolder, completion: ( (Data?, URL?, ExportStatus) -> Void)? = nil ) {
//        print("is exportable: \(item.asset.isExportable)")
//        guard item.asset.isExportable, let itemUrl:URL = item.url, let urlName:URL = itemUrl.getVideoName(contain: "du_15"), let fragFile = itemUrl.getVideoName(contain: "du_15", withWord:"downloading") else {
//            completion?(nil, item.url, .fail)
//            return
//        }
//
//
//        let isFileExist = folder.isExist(url: urlName)
//        if isFileExist.exist {
//            completion?(nil, nil, .isExist)
//
//        }else {
//            var exportFile:URL!
//            exportFile = folder.appendPath(url: urlName)
//            let downloading = folder.appendPath(url: fragFile)
//
//            let composition = AVMutableComposition()
//            let compositionVideoTrack = composition.addMutableTrack(withMediaType: AVMediaType.video, preferredTrackID: CMPersistentTrackID(kCMPersistentTrackID_Invalid))
////            let compositionAudioTrack = composition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: CMPersistentTrackID(kCMPersistentTrackID_Invalid))
//
//            guard let sourceVideoTrack = item.asset.tracks(withMediaType: AVMediaType.video).first else {return}
////            let sourceAudioTrack = item.asset.tracks(withMediaType: AVMediaType.audio).first!
//            do {
//                try compositionVideoTrack?.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: item.duration), of: sourceVideoTrack, at: CMTime.zero)
////                try compositionAudioTrack?.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: item.duration), of: sourceAudioTrack, at: CMTime.zero)
//            } catch {
//                completion?(nil, nil, .fail)
//                return
//            }
//
//            let preset: String = AVAssetExportPresetPassthrough
////            let compatiblePresets = AVAssetExportSession.exportPresets(compatibleWith: composition)
////            if compatiblePresets.contains(AVAssetExportPreset1920x1080) {
////                preset = AVAssetExportPreset1920x1080
////            }
//            self.exportSession = AVAssetExportSession(asset: composition, presetName: preset)
//            guard
//                let exportSession = self.exportSession,
//                exportSession.supportedFileTypes.contains(AVFileType.mp4) else {
//                completion?(nil, nil, .fail)
//                return
//            }
//
//            exportSession.outputURL = downloading
//            exportSession.outputFileType = AVFileType.mp4
//            let startTime = CMTimeMake(value: 0, timescale: 1)
//            let timeRange = CMTimeRangeMake(start: startTime, duration: item.duration)
//            exportSession.timeRange = timeRange
//            exportSession.shouldOptimizeForNetworkUse = false
//
//            exportSession.exportAsynchronously {
//                if let error = exportSession.error {
//                    print("error....: \(error)")
//                    completion?(nil, nil, .fail)
//                }else {
//                    folder.removeFirstIfExceed(folder: folder)
//
////                    let data = try? Data(contentsOf: downloading)
//                    try? folder.manager.copyItem(at: downloading, to: exportFile)
//                    try? folder.manager.removeItem(at: downloading)
//
//                    let data = try? Data(contentsOf: exportFile)
////                    print("download done..... \(urlName.lastPathComponent)")
////                    data?.size()
//
//                    self.exportSession = nil
//                    completion?(data, exportFile, .sucess)
//                }
//            }
//
//        }
//
//    }
    
//    func cancelExport() {
//        MediaPreloader.shared.allowPreloadFlag = false
//        exportSession?.asset.cancelLoading()
//        exportSession?.cancelExport()
//        exportSession = nil
//        
//        print("cancel export....")
//    }
    
}

extension VideoManager {
    
}

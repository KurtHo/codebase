//
//  VideoFolder.swift
//  SliderVideo
//
//  Created by CI-Kurt on 2021/1/18.
//

import Foundation
import AVKit

enum VideoFolder: CaseIterable {
    case lowRes, highRes
    var manager: FileManager {
        return FileManager.default
    }
    
    var subFolder: String {
        let supfolder = "VideoFolder"
        switch self {
        case .lowRes: return "\(supfolder)/lowRes"
        case .highRes: return "\(supfolder)/highRes"
        }
    }
    
    private var max: Int {
        switch self {
        case .lowRes:   return 25
        case .highRes:  return 20
        }
    }
    
    var files: [String]? {
        let foldersPath = manager.urls(for: .documentDirectory, in: .userDomainMask)[0].path + "/"
        switch self {
        case .lowRes:
            return try? manager.contentsOfDirectory(atPath: foldersPath + VideoFolder.lowRes.subFolder)
        case .highRes:
            return try? manager.contentsOfDirectory(atPath: foldersPath + VideoFolder.highRes.subFolder)
        }
    }
    
    var videoFolder: URL {
        let docURL =  manager.urls(for: .documentDirectory, in: .userDomainMask).first! as URL
        switch self {
        case .lowRes:
            return docURL.appendingPathComponent(VideoFolder.lowRes.subFolder)
        case .highRes:
            return docURL.appendingPathComponent(VideoFolder.highRes.subFolder)
        }
    }
    
//    func appendPath(url: URL, byPath: Bool = false) -> URL {
//        if byPath {
//            var urlStr = URLComponents(string: url.absoluteString)!.path
//
//            for _ in urlStr {
//                if urlStr.first == "/" {
//                    urlStr.removeFirst()
//                }else {break}
//            }
//
//            let newString = urlStr.replacingOccurrences(of: "/", with: "", options: .literal, range: nil)
//            let compoStr = newString.seperateBy(["."])
//            var encryptStr = CryptoUtil.sha512Hex(string: newString)
//            if let last = compoStr.last {
//                encryptStr.append(".\(last)")
//            }
//
//            return videoFolder.appendingPathComponent(encryptStr)
//        }
//        return videoFolder.appendingPathComponent(url.lastPathComponent)
//    }
    
//    func isExist(url: URL) -> (exist: Bool, urlPath: URL, playerItem: PlayerItem?) {
//        var videoPath: URL!
//        var item: PlayerItem?
//        var exist: Bool!
//
//        videoPath = appendPath(url: url, byPath: true)
//        exist = manager.fileExists(atPath: videoPath.path)
//
//        if exist == false {
//            videoPath = appendPath(url: url)
//            exist = manager.fileExists(atPath: videoPath.path)
//        }
//
//        let assetKeys = ["playable", "hasProtectedContent", "tracks", "duration"]
//
//        let asset = exist ? AVAsset(url: videoPath) : AVAsset(url: url)
//        item = PlayerItem(asset: asset, automaticallyLoadedAssetKeys: assetKeys)
//
//        return (exist, videoPath, item)
//    }
    
    func removeFirstIfExceed(folder:VideoFolder) {
        let files = self.sortUrlsByDate(folder: folder.subFolder)
        let max = folder.max

        if files.count > max {
            do {
                let removeFile = videoFolder.path + "/" + (files.last ?? "")
                try manager.removeItem(atPath: removeFile)
            }catch {
                print("remove failed: \(files.last ?? "")")
            }
        }
    }
    
    func sortUrlsByDate(folder:String) -> [String]{
        var urlsPath = [String]()
        
        do {
            let folders = manager.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let urlPath = URL(fileURLWithPath: folders.path + "/" + folder)
            var urls = [URL]()
            
            let directoryContent = try manager.contentsOfDirectory(at: urlPath, includingPropertiesForKeys:
                [.creationDateKey], options: [.skipsSubdirectoryDescendants, .skipsHiddenFiles])
            
            urls = directoryContent.sorted(by: { (l, r) -> Bool in
                guard let lResource = try? l.resourceValues(forKeys: [.creationDateKey]).creationDate,
                    let rResource = try? r.resourceValues(forKeys: [.creationDateKey]).creationDate else {return false}
                return (lResource.timeIntervalSince1970 ) > (rResource.timeIntervalSince1970 )
            })
            
            urls.forEach{ url in
                let strPath = url.path.replacingOccurrences(of: self.videoFolder.path + "/", with: "")
                urlsPath.append(strPath)
            }
            
        }
        catch {
            print(error)
        }
        return urlsPath
    }
    
    func removeNotCompletedDownlaods() {
        if let doownloadings = files {
            doownloadings.forEach { dl in
                if dl.contains("(downloading)_") {
                    do {
                        let removeFile = videoFolder.path + "/" + dl
                        try manager.removeItem(atPath: removeFile)
                    } catch {
                        debug("remove dl failed...")
                    }
                }
            }
            
        }
    }
    
    /// Remove local url is exist
    /// - Parameter url: full url
    func remove(url:URL?){
        guard let url = url else {return}
        if let locaFiles = files {
            let urlStr = url.lastPathComponent
            locaFiles.forEach { localFile in
                if localFile.contains(urlStr) {
                    do {
                        let removeFiles = videoFolder.path + "/" + urlStr
                        try manager.removeItem(atPath: removeFiles)
                    }catch{
                        debug("remove fail: \(urlStr)")
                    }
                }
            }
        }
    }
    
}

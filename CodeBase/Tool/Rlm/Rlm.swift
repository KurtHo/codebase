//
//  Rlm.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/1/6.
//

import Foundation
import RealmSwift

let rlm = Rlm.shared
class Rlm {
    private init(){}
    static let shared = Rlm()
    var realm = try! Realm()
    
    func add<S: Sequence>(_ objs: S, update: Realm.UpdatePolicy) where S.Iterator.Element: Object {
        do {
            try realm.write({
                realm.add(objs, update: update)
            })
        } catch(let err) {
            print("decode fail: \(err)")
        }
    }
}

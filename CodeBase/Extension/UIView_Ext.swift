//
//  UIView_Ext.swift
//  CodeBase
//
//  Created by KurtHo on 2020/12/1.
//

import UIKit

extension UIView {
    
    /// set anchor
    /// - Parameters:
    ///   - top: top anchor
    ///   - leading: leading anchor
    ///   - trailing: trailing anchor
    ///   - bottom: bottom anchor
    ///   - height: height constraint
    ///   - width: widhth constraint
    ///   - padding: edgeInsets
    func setAnchor(top: NSLayoutYAxisAnchor?, leading: NSLayoutXAxisAnchor?, trailing: NSLayoutXAxisAnchor?, bottom: NSLayoutYAxisAnchor?, height: CGFloat?, width: CGFloat?, padding: UIEdgeInsets = UIEdgeInsets.zero) {
        self.translatesAutoresizingMaskIntoConstraints = false
        if let top = top {
            topAnchor.constraint(equalTo: top, constant: padding.top).isActive = true
        }
        
        if let leading = leading {
            leadingAnchor.constraint(equalTo: leading, constant: padding.left).isActive = true
        }
        
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: padding.bottom).isActive = true
        }
        
        if let trailing = trailing {
            trailingAnchor.constraint(equalTo: trailing, constant: padding.right).isActive = true
        }
        
        if let height = height {
            heightAnchor.constraint(equalToConstant: height).isActive = true
        }
        
        if let width = width {
            widthAnchor.constraint(equalToConstant: width).isActive = true
        }
        
    }
    
    /// set anchor
    /// - Parameters:
    ///   - centerX: center x anchor
    ///   - centerY: center y anchor
    ///   - offsetX: offset x
    ///   - offsetY: offset y
    func setCenterAnchor(centerX: NSLayoutXAxisAnchor?, centerY: NSLayoutYAxisAnchor?, offsetX: CGFloat = .zero, offsetY: CGFloat = .zero) {
        if let centerX = centerX {
            centerXAnchor.constraint(equalTo: centerX, constant: offsetX).isActive = true
        }
        
        if let centerY = centerY {
            centerYAnchor.constraint(equalTo: centerY, constant: offsetY).isActive = true
        }
    }
    
    /// addsubview before use this function
    func setRoundedShape(){
        layoutIfNeeded()
        clipsToBounds = true
        layer.cornerRadius = frame.height / 2
    }
    
    func removeAllConstraints() {
        if let _superview = self.superview {
            self.removeFromSuperview()
            _superview.addSubview(self)
        }
    }
    
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
         let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
         let mask = CAShapeLayer()
         mask.path = path.cgPath
         layer.mask = mask
     }
}

//
//  UITableViewCell_Ext.swift
//  CodeBase
//
//  Created by KurtHo on 2021/12/10.
//

import UIKit

extension UITableViewCell {
    
    static var identifier: String {
        return String(describing: self)
    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
}

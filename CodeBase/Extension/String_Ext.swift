//
//  String_Ext.swift
//  CodeBase
//
//  Created by CI-Kurt on 2021/6/15.
//

import Foundation

extension String {
    func noChinese() -> String?{
        return self.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
    }
    
    var floatValue: Float {
        return (self as NSString).floatValue
    }
 
    
    func cutStr(from: Int, to: Int)-> String{
        if self.count < from || self.count < abs(to) { return "" }
        let start = self.index(self.startIndex, offsetBy: from)
        let end = self.index(self.endIndex, offsetBy: to)
        let result = self[start...end]
        return String(result)
        
    }
    
    /// Last string after dot
    func ext() -> String {
        let arr = self.lowercased().split(separator: ".")
        let cnt = arr.count
        
        if cnt > 1 {
            return String(arr[cnt-1])
        }
        return ""
    }
    
    func getStringAfter(_ place:String = "Documents") -> String{
        guard let urlStr = URL(string: self) else {return self}
        var result:String = ""
        var flag:Bool = false
        let component = urlStr.pathComponents
        
        component.enumerated().forEach{
            if $1 == place {
                flag = true
            }
            if flag {
                result.append("/\($1)")
            }
        }
        
        return result
    }
    
    func getLastComponent() -> String {
        guard let urlStr = URL(string: self) else {return self}
        return urlStr.lastPathComponent
    }
    
    func seperateBy(_ strs:[String]) -> [String] {
        var result = [String]()
        strs.forEach{
            result += self.components(separatedBy: $0)
        }
        
        return result
    }
    
    func toSec() -> Double {
        
        guard self.isEmpty == false else { return 0 }
        
        let arr = self.split(separator: ":")
        if arr.count == 3 {
            // 01:11:22
            let h: Double = Double(arr[0]) ?? 0.0
            let m: Double = Double(arr[1]) ?? 0.0
            let s: Double = Double(arr[2]) ?? 0.0
            return (h * 60.0 * 60.0) + (m * 60.0) + s
        } else if arr.count == 2 {
            // 11:22
            let m: Double = Double(arr[0]) ?? 0.0
            let s: Double = Double(arr[1]) ?? 0.0
            return (m * 60.0) + s
        }
        return 0
    }
    
    func toURL() -> URL? {
        return URL(string: self)
    }
    
}

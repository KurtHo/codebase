//
//  UIColor_Ext.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2021/12/23.
//

import UIKit

extension UIColor {
    static let themeOrnage = UIColor(red: 235/255, green: 97/255, blue: 0/255, alpha: 1.0)
    
    static let tabSelected = UIColor(red: 34/255, green: 34/255, blue: 34/255, alpha: 1.0)
    
    static let tabUnselected = UIColor(red: 136/255, green: 136/255, blue: 136/255, alpha: 1.0)
    
    static let naviTitle = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1.0)
    
    static let operatorSepLine = UIColor(red: 204/255, green: 204/255, blue: 204/255, alpha: 1.0)
}


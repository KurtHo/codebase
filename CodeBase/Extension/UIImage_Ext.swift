//
//  Image_Ext.swift
//  CodeBase
//
//  Created by KurtHo on 2021/12/6.
//

import UIKit

extension UIImage {
    static var question: UIImage? {
        return UIImage(named: "question")
    }
    
    static var naviBack: UIImage? {
        UIImage(named: "naviBack")
    }
    
    static var search: UIImage? {
        UIImage(named: "search")
    }
    
    static var aBei: UIImage? {
        return UIImage(named: "aBei")
    }
    
    static var refresh: UIImage? {
        return UIImage(named: "icon_refresh")
    }
    
    static var tSquare: UIImage? {
        return UIImage(named: "tsquare")
    }
}

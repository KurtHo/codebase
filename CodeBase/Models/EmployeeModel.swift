//
//  EmployeeModel.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/1/8.
//

import Foundation

class EmployeesModel:Decodable {
    var status:String?
    var data:[EmployeeModel]?
    var message:String?
}

class EmployeeModel:Decodable {
    var id:Int?
    var employeeName:String?
    var employeeSalary:Int?
    var employeeAge:Int?
    var profileImage:String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case employeeName = "employee_name"
        case employeeSalary = "employee_salary"
        case employeeAge = "employee_age"
        case profileImage = "profile_image"
    }
}

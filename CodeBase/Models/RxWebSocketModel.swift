//
//  RxWebSocketModel.swift
//  CodeBase
//
//  Created by KurtHo on 2021/12/13.
//

import Foundation
import RxSwift

class RxWebSocketModel {
    var name:String? = nil
    var members:[String] = []
    
    var action:String = ""
    var systemMessage:String = ""
    var message:String = ""
}

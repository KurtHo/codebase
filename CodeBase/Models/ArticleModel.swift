//
//  ArticleModel.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2022/1/6.
//

import Foundation
import RealmSwift

class NewsList: Decodable {
    var status: String
    var totalResults: Int
    var articles: [ArticleModel] = [ArticleModel]()

    var rlmArticles: List<ArticleModel> {
        let objs = List<ArticleModel>()
        articles.forEach{
            objs.append($0)
        }
        return objs
    }
}

class ArticleModel:Object, Decodable {
    
    override static func primaryKey() -> String? {
        return "title"
    }

    @objc dynamic var source: Source?
    @objc dynamic var author: String?
    @objc dynamic var title: String?
//    var description: String?
    @objc dynamic var desc: String?
    @objc dynamic var url: String?
    @objc dynamic var urlToImage: String?
    @objc dynamic var publishedAt: String?
    @objc dynamic var content: String?
    
    enum CodingKeys: String, CodingKey {
        case source, author, title, url, urlToImage, publishedAt, content
        case desc = "description"
    }

}

class Source: Object, Decodable {
    @objc dynamic var id: String?
    @objc dynamic var name: String?
}




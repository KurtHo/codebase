//
//  ResumeModel.swift
//  CodeBase
//
//  Created by KurtHo on 2020/12/1.
//

import UIKit

class ResumeModel {
    var contents = [TextImgModel]()
}

class TextImgModel {
    let titleText:String
    let describ:String
    let img:UIImage
//    let vc:UIViewController?
    
    init(text:String, describ:String, img:UIImage) {
        self.titleText = text
        self.describ = describ
        self.img = img
    }
}

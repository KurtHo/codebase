//
//  GeneralResponseMOdel.swift
//  CodeBase
//
//  Created by Tai-Yi Ho on 2023/8/16.
//

import Foundation

struct GeneralResponseModel<T: Decodable>: Decodable {
    let code: Int?
    let message: String?
    let data: T?
}

struct ResponseData<T: Decodable>: Decodable {
    let data: T?
}


struct LoginRespModel: Decodable {
    let accessToken: String
    let refreshToken: String
}

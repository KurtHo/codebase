//
//  BaseScrollView.swift
//  CodeBase
//
//  Created by KurtHo on 2020/12/7.
//

import UIKit

protocol PageScrollViewDelegate: AnyObject {
    func currentPage(_ page:Int)
}

class PageScrollView: UIScrollView {
    let contentView = UIView()
    private var views = [UIView]()
    var page:Int = 0
    
    weak var pageDelegate:PageScrollViewDelegate?
    
    override func draw(_ rect: CGRect) {
        AppManager.shared.orientDelegate = self
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.contentSize = CGSize(width: self.frame.width * CGFloat(views.count), height: self.frame.height)
//        contentOffset.x = frame.width * CGFloat(page)
        
    }

    
    func setContentViews(_ views:[UIView], superview: UIView) {
//        delegate = self
        contentView.backgroundColor = .lightGray
        addSubview(contentView)
        
        superview.layoutSubviews()
        contentView.setAnchor(top: superview.safeAreaLayoutGuide.topAnchor, leading: superview.leadingAnchor, trailing: nil, bottom: superview.safeAreaLayoutGuide.bottomAnchor, height: nil, width: superview.bounds.width * CGFloat(views.count))
        
        var previousAnchor = self.leadingAnchor
        for view in views {
            contentView.addSubview(view)
            
            view.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
            view.setAnchor(top: topAnchor, leading: previousAnchor, trailing: nil, bottom: superview.safeAreaLayoutGuide.bottomAnchor, height: nil, width: nil)

            previousAnchor = view.trailingAnchor
        }
        
        self.views = views
        setNeedsLayout()
        self.contentSize = CGSize(width: self.frame.width * CGFloat(views.count), height: self.frame.height)
        
    }
    
    func setScrollToPage(_ page:Int){
        self.page = page
        let offsetX:CGFloat = bounds.width * CGFloat(page)
        setContentOffset(CGPoint(x: offsetX, y: contentOffset.y), animated: true)
        
        debug("page: \(page), contentOffset: \(self.contentOffset)")
    }
    
}

extension PageScrollView: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        page = Int(scrollView.contentOffset.x / bounds.width)
        pageDelegate?.currentPage(page)
        debug("page: \(page)")
    }

}

extension PageScrollView: OrientDelegate {
    func didChange(_ orientation: Orientation) {
        guard let superview = superview else {return}
        let x = Int(superview.bounds.height) * page
        
        DispatchQueue.main.async {
            self.setContentOffset(CGPoint(x: x, y: 0), animated: true)
        }
    }

}

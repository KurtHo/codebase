//
//  BaseTableView.swift
//  CodeBase
//
//  Created by KurtHo on 2020/12/1.
//

import UIKit

class BaseTableView: UITableView {
    
    init() {
        super.init(frame: .zero, style: .plain)
//        separatorStyle = .none
        separatorColor = .clear
        backgroundColor = .lightGray
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

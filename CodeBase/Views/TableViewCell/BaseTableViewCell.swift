//
//  BaseTableViewCell.swift
//  CodeBase
//
//  Created by KurtHo on 2020/12/1.
//

import UIKit

class BaseTableViewCell: UITableViewCell {
    
    init() {
        super.init(style: .default, reuseIdentifier: nil)
        selectionStyle = .none
        separatorInset = .zero
//        backgroundColor = .clear
        backgroundColor = .white
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

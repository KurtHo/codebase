//
//  FirstCell.swift
//  CodeBase
//
//  Created by KurtHo on 2020/12/1.
//

import UIKit

class FirstTableViewCell: BaseTableViewCell {
    private var data: Bind<TextImgModel>!
    
    private var titleLabel: UILabel = {
        var lb = UILabel()
        lb.setFont(fontName: .PingFangTCSemibold, size: 18, color: .black)
        return lb
    }()
    
    private var descLabel: UILabel = {
        let lb = UILabel()
        lb.setFont(fontName: .PingFangTCRegular, size: 16, color: .black)
        lb.numberOfLines = 2
        return lb
    }()
    
    
    func setContent(data: TextImgModel) {
        self.data = Bind(value:data)
        
        [titleLabel, descLabel].forEach {
            self.addSubview($0)
        }
        titleLabel.text = self.data.value.titleText
        descLabel.text = self.data.value.describ
        setPosition()
    }
    
    private func setPosition() {
        titleLabel.setAnchor(top: topAnchor, leading: leadingAnchor, trailing: trailingAnchor, bottom: nil, height: nil, width: nil, padding: UIEdgeInsets(top: 2, left: 40, bottom: 0, right: 25))
        descLabel.setAnchor(top: titleLabel.bottomAnchor, leading: leadingAnchor, trailing: trailingAnchor, bottom: nil, height: nil, width: nil, padding: UIEdgeInsets(top: 0, left: 40, bottom: 0, right: 10))
    }
    
}

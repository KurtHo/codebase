//
//  SegmentController.swift
//  CodeBase
//
//  Created by KurtHo on 2020/11/30.
//

import UIKit

protocol PageSegmentDelegate: AnyObject {
    func buttonTap(toIndex:Int)
}

class PageSegmentControl: UIControl {
    weak var delegate:PageSegmentDelegate?
    
    private var buttons = [UIButton]()
    private var scrollView = UIScrollView()
    
    private var selector = UIView()
    private var toIndex: Int?
    var selectorSegmentIndex = 0 {
        didSet {
            if isAniamte {
                didSelected()
            }
        }
    }
    
    private var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    var commaSeparatedButtonTitles: String = ""
    var borderColor: UIColor = .clear
    
    var textColor: UIColor = .gray
    var selectorColor: UIColor = .black
    var isFill: Bool = true
    
    private var selectorTextColor: UIColor = .black
    private var fontSize: CGFloat = 17
    private var isAniamte: Bool = true
    private let leftPadding: CGFloat = 16
    private let rightPadding: CGFloat = 16

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.scrollView.contentSize.height = 1
        self.scrollView.showsVerticalScrollIndicator = false
        self.scrollView.showsHorizontalScrollIndicator = false
        self.scrollView.delegate = self
        
        DispatchQueue.main.async {
            self.updateView()
        }
    }
    
    
    override func layoutSubviews() {
        didSelected()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func updateView() {
        debug("update view")
        
        let buttonTitles = commaSeparatedButtonTitles.components(separatedBy: ",")
        var scrollViewWidth: CGFloat = 0
        var buttonPositionX: CGFloat = 0 + leftPadding
        
        for buttonTitle in buttonTitles {
            let button = UIButton(type: .custom)
            button.setTitle(buttonTitle, for: .normal)
            button.setFont(fontName: .PingFangTCRegular, size: fontSize, color: textColor)
            button.titleLabel?.sizeToFit()
            button.setTitleColor(textColor, for: .normal)
            button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
            let right:CGFloat = isFill ? 0 : 25
            button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 3, right: right)
            button.sizeToFit()
            button.contentHorizontalAlignment = .center
            button.titleLabel?.textAlignment = .center
            button.frame.origin.x = buttonPositionX
            
//            if isFill {
//                button.frame.size.width = (frame.width - leftPadding - rightPadding) / CGFloat(buttonTitles.count)
//            } else {
            scrollViewWidth += button.frame.width
//            }
            
            button.frame.size.height = self.frame.height
            button.frame.origin.y = 0
            buttonPositionX += button.frame.width
            
            scrollView.addSubview(button)
            buttons.append(button)
        }
        addSubview(scrollView)
        
        if isFill {
            scrollView.setAnchor(top: topAnchor, leading: leadingAnchor, trailing: trailingAnchor, bottom: bottomAnchor, height: nil, width: nil)
            
            var previous = scrollView.leadingAnchor
            
            buttons.forEach {
                $0.setAnchor(top: topAnchor, leading: previous, trailing: nil, bottom: bottomAnchor, height: nil, width: nil)
                $0.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 1/3).isActive = true
                previous = $0.trailingAnchor
            }
        }
        
        buttons[0].setTitleColor(selectorTextColor, for: .normal)
        
        let selectorWidth = buttons[selectorSegmentIndex].titleLabel?.frame.width ?? 0
        let selectorStartPosition = (buttons[selectorSegmentIndex].titleLabel?.frame.origin.x ?? 0) + buttons[selectorSegmentIndex].frame.origin.x
        
        selector = UIView(frame: CGRect(x: selectorStartPosition,
                                        y: self.frame.height - 3,
                                        width: selectorWidth,
                                        height: 3))
        
        selector.layer.cornerRadius = 1
        selector.clipsToBounds = true
        selector.backgroundColor = selectorColor
        
        addSubview(selector)
        
        if isFill {
//            selector.setAnchor(top: nil, leading: buttons[0].leadingAnchor, trailing: buttons[0].trailingAnchor, bottom: scrollView.bottomAnchor, height: nil, width: nil)
//            selector.heightAnchor.constraint(equalToConstant: 3).isActive = true
            
            layoutIfNeeded()
            self.selector.frame.origin.x = self.buttons[0].titleLabel?.frame.origin.x ?? 0
 
        }
    }
    
    override func draw(_ rect: CGRect) {
        
//        layoutIfNeeded()
//        updateView()
        
        scrollView.frame = rect
        
        if let toIndex = toIndex {
            UIView.animate(withDuration: 0.2, animations: {
                self.buttons[self.selectorSegmentIndex].setTitleColor(self.textColor, for: .normal)
                self.selector.frame.size.width = self.buttons[toIndex].titleLabel?.frame.width ?? 0
                self.selector.frame.origin.x = (self.buttons[toIndex].titleLabel?.frame.origin.x ?? 0) + self.buttons[toIndex].frame.origin.x
                self.buttons[toIndex].setTitleColor(self.selectorTextColor, for: .normal)
            }) { _ in
                self.selectorSegmentIndex = toIndex
                self.toIndex = nil
            }
            self.buttons[toIndex].isUserInteractionEnabled = false
        }

    }
    
    @objc func buttonTapped(sender: UIButton) {
        for (buttonIndex, btn) in buttons.enumerated() {
            if btn == sender {
                selectorSegmentIndex = buttonIndex
            }
        }
        delegate?.buttonTap(toIndex: selectorSegmentIndex)
        sendActions(for: .valueChanged)
    }
    
    func didSelected() {
        scrollView.layoutIfNeeded()
        for (buttonIndex, btn) in buttons.enumerated() {
            
            btn.isUserInteractionEnabled = false
            btn.setTitleColor(textColor, for: .normal)

            if buttonIndex == selectorSegmentIndex {
                
                let selectorStartPosition = (btn.titleLabel?.frame.origin.x ?? 0) + btn.frame.origin.x
                let selectorWidth = btn.titleLabel?.frame.width ?? 0
                
                UIView.animate(withDuration: 0.2, animations: {
                    self.selector.frame.size.width = selectorWidth
                    self.selector.frame.origin.x = selectorStartPosition
                }, completion: { _ in
                    for button in self.buttons {
                        button.isUserInteractionEnabled = true
                    }
                })
                btn.setTitleColor(selectorTextColor, for: .normal)
            }
        }
    }
}

extension PageSegmentControl: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollView.contentOffset.y = 0
    }
}


